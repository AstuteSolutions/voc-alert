﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Iper.Common.EntityFramework;
using Iper.Common.Models;
using Microsoft.EntityFrameworkCore;
using VoC.Alerts.Domain;
using Question = VoC.Alerts.Domain.Question;

namespace VoC.Alerts.Logic
{
    public interface IVocResponseServices
    {
        IQueryable<Response> GetResponses(int companyId, int surveyId, int questionId, DateTime cutoffDateTime);
        IQueryable<Response> GetResponses(int companyId, int surveyId, int questionId, List<long> respondents);
        ValueTask<int> GetTotalNumberOfResponses(int companyId, int surveyId, int durationInMinutes);
    }

    public class VocResponseServices : IVocResponseServices
    {
        private readonly WebValidatorContext _context;

        public VocResponseServices(WebValidatorContext context)
        {
            _context = context;
        }

        public IQueryable<Response> GetResponses(int companyId, int surveyId, int questionId, DateTime cutoffDateTime)
        {
            _context.Database.SetCommandTimeout(TimeSpan.FromMinutes(5));
            var languageId = 1;
            var localCurrentTime = cutoffDateTime.ToLocalTime();

            return from masterData in _context.MasterDatas.Include(x => x.DetailDataCollection).AsNoTracking()
                   join respondentData in _context.RespondentDatas.AsNoTracking().Where(x => x.LangId == languageId)
                       on masterData.RespondentId equals respondentData.RespondentId
                   join surveyAccount in _context.SurveyAccounts.AsNoTracking().Where(c => c.CompanyId == companyId && c.SurveyId == surveyId)
                       on new { masterData.ProjectId } equals new { ProjectId = surveyAccount.SurveyId }
                   join surveyQuestionnare in _context.SurveyQuestionnaireSetups.AsNoTracking().Where(x => x.LangId == languageId)
                       on new { masterData.ProjectId, masterData.QuestionId, QuestionTypeId = (int)masterData.QuestionTypeId }
                       equals new { ProjectId = surveyQuestionnare.SurveyId, surveyQuestionnare.QuestionId, surveyQuestionnare.QuestionTypeId }
                   join questionVersion in _context.MasterQuestionVersions.AsNoTracking() on new { surveyQuestionnare.VersionId, surveyQuestionnare.QuestionId }
                       equals new { questionVersion.VersionId, questionVersion.QuestionId }
                   join questionDesc in _context.QuestionLongDescs.AsNoTracking().Where(x => x.LangId == languageId)
                       on questionVersion.LongDescId equals questionDesc.LongDescId
                   where respondentData.SubmitDateTime >= localCurrentTime &&
                         surveyQuestionnare.QuestionId == questionId 
                   select new Response(masterData.RespondentId)
                   {
                       Survey = new Survey(surveyAccount.SurveyId) { Title = surveyAccount.SurveyTitle },
                       Question = new Question(questionVersion.QuestionId)
                       {
                           TypeId = surveyQuestionnare.QuestionTypeId,
                           Text = questionDesc.LongText
                       },
                       Answer = masterData.Answer,
                       TagId = GetTags(masterData)
                   };
        }

        public IQueryable<Response> GetResponses(int companyId, int surveyId, int questionId,  List<long> respondents)
        {
            _context.Database.SetCommandTimeout(TimeSpan.FromMinutes(5));
            var languageId = 1;

            return from masterData in _context.MasterDatas.Include(x => x.DetailDataCollection).AsNoTracking()
                   join respondentData in _context.RespondentDatas.AsNoTracking().Where(x => x.LangId == languageId)
                       on masterData.RespondentId equals respondentData.RespondentId
                   join surveyAccount in _context.SurveyAccounts.AsNoTracking().Where(c => c.CompanyId == companyId && c.SurveyId == surveyId)
                       on new { masterData.ProjectId } equals new { ProjectId = surveyAccount.SurveyId }
                   join surveyQuestionnare in _context.SurveyQuestionnaireSetups.AsNoTracking().Where(x => x.LangId == languageId)
                       on new { masterData.ProjectId, masterData.QuestionId, QuestionTypeId = (int)masterData.QuestionTypeId }
                       equals new { ProjectId = surveyQuestionnare.SurveyId, surveyQuestionnare.QuestionId, surveyQuestionnare.QuestionTypeId }
                   join questionVersion in _context.MasterQuestionVersions.AsNoTracking() on new { surveyQuestionnare.VersionId, surveyQuestionnare.QuestionId }
                       equals new { questionVersion.VersionId, questionVersion.QuestionId }
                   join questionDesc in _context.QuestionLongDescs.AsNoTracking().Where(x => x.LangId == languageId)
                       on questionVersion.LongDescId equals questionDesc.LongDescId
                   where respondents.Contains(respondentData.RespondentId) &&
                         surveyQuestionnare.QuestionId == questionId
                   select new Response(masterData.RespondentId)
                   {
                       Survey = new Survey(surveyAccount.SurveyId) { Title = surveyAccount.SurveyTitle },
                       Question = new Question(questionVersion.QuestionId)
                       {
                           TypeId = surveyQuestionnare.QuestionTypeId,
                           Text = questionDesc.LongText
                       },
                       Answer = masterData.Answer,
                       TagId = GetTags(masterData)
                   };
        }

        private static int[] GetTags(MasterData data)
        {
            if (data.TagId != null)
            {
                return new[] {data.TagId.Value};
            }

            else if (data.DetailDataCollection.Any())
            {
                return data.DetailDataCollection.Where(x => x.TagId != null).Select(x => x.TagId.Value).ToArray();
            }

            return new []{-1};
        }
        public async ValueTask<int> GetTotalNumberOfResponses(int companyId, int surveyId, int durationInMinutes)
        {
            var languageId = 1;
            var result = (from masterData in _context.MasterDatas.AsNoTracking().Where(x => x.ProjectId == surveyId)
                          join respondentData in _context.RespondentDatas.AsNoTracking().Where(x => x.LangId == languageId && (x.SubmitDateTime >= DateTime.Now.AddMinutes(-durationInMinutes) &&
                                  x.SubmitDateTime <= DateTime.Now))
                              on masterData.RespondentId equals respondentData.RespondentId
                          join surveyAccount in _context.SurveyAccounts.AsNoTracking()
                                  .Where(c => c.CompanyId == companyId && c.SurveyId == surveyId)
                              on new { masterData.ProjectId } equals new { ProjectId = surveyAccount.SurveyId }
                          select masterData.RespondentId).Distinct().CountAsync();
            return await result.ConfigureAwait(false);
        }
    }
}