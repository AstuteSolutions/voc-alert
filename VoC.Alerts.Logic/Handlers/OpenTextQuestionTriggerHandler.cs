﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VoC.Alerts.Domain;
using VoC.Alerts.Logic.Triggers;
using Voc.Alerts.TriggerEventResolvers.TriggerEventCreators;

namespace VoC.Alerts.Logic.Handlers
{
    public class OpenTextQuestionTriggerHandler : IRequestHandler<OpenTextQuestionTrigger, TriggerEvents>
    {
        private readonly IVocResponseServices _vocServices;
        private readonly ITriggerEventResolver _eventResolver;
        public OpenTextQuestionTriggerHandler(IVocResponseServices vocServices, IEnumerable<ITriggerEventResolver> triggerEventResolvers)
        {
            _vocServices = vocServices;
            _eventResolver = triggerEventResolvers.FirstOrDefault(x => x.Type.Equals(nameof(OpenTextQuestionTrigger)));
        }

        public async Task<TriggerEvents> Handle(OpenTextQuestionTrigger request, CancellationToken cancellationToken)
        {
            var events = new List<TriggerEvent>();
            var records = request.RespondentId.Any() ? _vocServices.GetResponses(request.CompanyId, request.SurveyId, request.QuestionId, request.RespondentId) :
                _vocServices.GetResponses(request.CompanyId, request.SurveyId, request.QuestionId, request.CutOffTime);
            foreach (var record in records)
            {
                var triggerEvent = _eventResolver.ProcessData(record, request);
                if (triggerEvent != null)
                {
                    events.Add(triggerEvent);
                }
                else
                    events.Add(new TriggerEvent(Guid.NewGuid(), request.Id, request.SetId, DateTime.UtcNow,
               request.CompanyId)
                    {
                        TargetMatchId = record.Id.ToString()
                    });
            }
            await Task.Yield();
            return new TriggerEvents(events);
        }
    }

}
