﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using VoC.Alerts.Domain;
using VoC.Alerts.Logic.Triggers;
using Voc.Alerts.TriggerEventResolvers.TriggerEventCreators;

namespace VoC.Alerts.Logic.Handlers
{
    public class SurveyVolumeTriggerHandler : IRequestHandler<SurveyVolumeTrigger, TriggerEvents>
    {
        private readonly IVocResponseServices _vocServices;
        private readonly ITriggerEventResolver _eventResolver;

        public SurveyVolumeTriggerHandler(IVocResponseServices vocServices, IEnumerable<ITriggerEventResolver> triggerEventResolvers)
        {
            _vocServices = vocServices;
            _eventResolver = triggerEventResolvers.FirstOrDefault(x => x.Type.Equals(nameof(SurveyVolumeTrigger)));
        }

        public async Task<TriggerEvents> Handle(SurveyVolumeTrigger request, CancellationToken cancellationToken)
        {
            var events = new List<TriggerEvent>();
            var totalResponseCount = await _vocServices.GetTotalNumberOfResponses(request.CompanyId, request.SurveyId, request.DurationInMinutes);

            var triggerEvent = _eventResolver.ProcessData(totalResponseCount, request);
            if (triggerEvent != null)
            {
                events.Add(triggerEvent);
            }
            await Task.Yield();
            if (!events.Any())
                events.Add(new TriggerEvent(Guid.NewGuid(), request.Id, request.SetId, DateTime.UtcNow,
                    request.CompanyId)
                { TargetMatchId=totalResponseCount.ToString()});

            return new TriggerEvents(events);
        }
    }
}
