﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Iperloop.Core.Events.Abstraction;
using Iperloop.Core.Events.Models;
using MediatR;
using Microsoft.Extensions.Logging;
using VoC.Alerts.Abstraction.Events;
using VoC.Alerts.Abstraction.Sources;
using VoC.Alerts.Domain;
using VoC.Alerts.Logic.Triggers;
using Voc.Alerts.TriggerEventResolvers;
using Voc.Alerts.TriggerEventResolvers.TriggerDefinitions;

namespace VoC.Alerts.Logic.Sources
{
    public class SurveySourceHandler : ISourceHandler<AlertDefinition>
    {
        private readonly IEventsStore<TriggerEvent> _eventStore;
        private readonly IEventDispatcher _eventDispatcher;
        private readonly ILogger<SurveySourceHandler> _logger;
        private readonly IMediator _mediator;
        private readonly Dictionary<string, Type> _triggerTypesDictionary;
        private readonly ITriggerCreator _triggerCreator;

        public SurveySourceHandler(IEventsStore<TriggerEvent> eventStore, IEventDispatcher eventDispatcher, ILogger<SurveySourceHandler> logger, IMediator mediator, ITriggerCreator triggerCreator)
        {
            _eventStore = eventStore;
            _eventDispatcher = eventDispatcher;
            _logger = logger;
            _mediator = mediator;
            _triggerCreator = triggerCreator;
            _triggerTypesDictionary = TriggerTypes.TriggerTypesDictionary;
        }

        public async Task Execute(AlertDefinition source)
        {
            var events = new List<TriggerEvent>();
            foreach (var triggerData in source.Triggers)
            {
                var trigger = CreateTriggerObject(triggerData, source);
                if (trigger == null)
                {
                    continue;
                }
                var result = (TriggerEvents)await _mediator.Send(trigger, CancellationToken.None).ConfigureAwait(false);
                if (result == null)
                {
                    continue;
                }

                events.AddRange(result.Events);
            }
            if (events.Any(e => e.Outcome))
            {
                _logger.LogInformation($"Event triggered on Alert '{source.Name}' - {source.Company.Id}");
                await _eventStore.Store(events.Where(x=>x.Outcome)).ConfigureAwait(false);
                var firstTriggers = events.First(x => x.Outcome);
                _eventDispatcher.Dispatch(new Message<EventMessage>("events", new EventMessage(firstTriggers.Id)
                {
                    AlertId = source.Id,
                    CompanyId = source.Company.Id,
                    ExecutionTime = firstTriggers.ExecutionTime,
                    TriggerId = firstTriggers.TriggerId,
                    TriggerSetId = firstTriggers.TriggersSetId,
                    Type = "event",
                    CorrelationId = firstTriggers.Id
                }), false);
            }
        }

        private object CreateTriggerObject(TriggerData triggerData, AlertDefinition alertDefinition)
        {
            if (_triggerTypesDictionary.TryGetValue(triggerData.Type, out var triggerType))
            {
                return CreateTrigger(triggerData, alertDefinition.Company.Id, ((SurveySource)alertDefinition.Source).SurveyId, triggerType);
            }

            return default;
        }

        private  object CreateTrigger(TriggerData triggerData, int companyId, int surveyId, Type triggerType)
        {
           return  _triggerCreator.CreateTrigger(triggerData, companyId, surveyId, triggerType);
        }
    }
}
