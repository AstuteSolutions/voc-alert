﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Iperloop.Core.Events.Abstraction;
using Iperloop.Core.Events.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using VoC.Alerts.Constants;
using VoC.Alerts.Context;
using VoC.Alerts.Context.Entities;
using VoC.Alerts.Domain;
using AlertDefinition = VoC.Alerts.Domain.AlertDefinition;
using TriggerEvent = VoC.Alerts.Domain.TriggerEvent;

namespace VoC.Alerts.Logic.Communications
{
    public class EventCommunicationHandler : IEventCommunicationHandler
    {
        private readonly AlertsContext _context;
        private readonly ILogger<EventCommunicationHandler> _logger;
        private readonly IEventDispatcher _eventDispatcher;

        public EventCommunicationHandler(AlertsContext context, ILogger<EventCommunicationHandler> logger, IEventDispatcher eventDispatcher)
        {
            _context = context;
            _logger = logger;
            _eventDispatcher = eventDispatcher;
        }

        public Task HandleAsync(AlertDefinition alertDefinition, IEnumerable<Recipient> recipients)
        {
            throw new NotImplementedException();
        }

        public async Task HandleAsync(Guid id,TriggerEvent @event, CancellationToken cancellationToken)
        {
            try
            {
                var alertInstance = _context.AlertInstances.Include(i => i.CommunicationDefinition)
                    .ThenInclude(ii => ii.Recipients)
                    .FirstOrDefault(x => x.Id.Equals(id));
                if (alertInstance==null)
                    throw new NotSupportedException(
                        $"Cannot create communication instance without alert Instance {id}");
                var messages = new List<Guid>();
                foreach (var communicationRecipient in alertInstance.CommunicationDefinition.Recipients)
                {
                    var messageId = Guid.NewGuid();
                    _context.CommunicationInstances.Add(new AlertCommunicationInstance
                    {
                        Id = messageId,
                        AlertInstanceId = alertInstance.Id,
                        RecipientId = communicationRecipient.Id,
                        State = CommunicationState.Await,
                        CreatedOn = DateTime.UtcNow
                    });
                    messages.Add(messageId);
                }

                alertInstance.State = AlertState.Communicated;
                _context.AlertInstances.Update(alertInstance);
                await _context.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
                messages.ForEach(x => _eventDispatcher.Dispatch(new Message<AlertInstanceMessage>("comm", new AlertInstanceMessage(x)), false));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }
    }

    public class AlertInstanceMessage : EventBase
    {
        public AlertInstanceMessage(Guid instanceId) :base(instanceId, DateTime.UtcNow)
        {
        }
    }
}
