﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using VoC.Alerts.Domain;

namespace VoC.Alerts.Logic.Communications
{
    public interface IEventCommunicationHandler
    {
        Task HandleAsync(AlertDefinition alertDefinition, IEnumerable<Recipient> recipients);
        Task HandleAsync(Guid id,TriggerEvent @event, CancellationToken cancellationToken);
    }
}
