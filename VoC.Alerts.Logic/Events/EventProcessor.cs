﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VoC.Alerts.Constants;
using VoC.Alerts.Context.Entities;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Domain;
using VoC.Alerts.Logic.Communications;
using TriggerEvent = VoC.Alerts.Domain.TriggerEvent;

namespace VoC.Alerts.Logic.Events
{
    public class EventProcessor : IEventProcessor
    {
        private readonly IEventCommunicationHandler _communicationHandler;
        private readonly IEventsRepository _eventsRepository;
        private readonly IAlertsRepository _alertsRepository;

        public EventProcessor(IEventCommunicationHandler communicationHandler, IEventsRepository eventsRepository, IAlertsRepository alertsRepository)
        {
            _communicationHandler = communicationHandler;
            _eventsRepository = eventsRepository;
            _alertsRepository = alertsRepository;
        }

        public async Task ProcessAllTriggers(AlertTriggerSet triggersSet, TriggerEvent @event, CancellationToken cancellationToken)
        {

            //check for logical Operation AND
            //Get the Threshold value for trigger set
            //Get the CompareTimeInterval value for trigger set
            //Get all the triggers from trigger set
            //For Each Trigger, Get the triggerEvent which are greater than last communicated alert instance time  and  (DateTime.UtcNow - x.ExecutionTime).TotalMinutes <=  "CompareTimeInterval" and  compare it with "Threshold"
            // if any of the trigger doesn't  match the condition the alert instance  will be invalid 
            //if all the trigger match the condition, Compare it with notification frequency ,if the last sent time is greater than or equal to DateTime.UtcNow. Create alert Instance to Send out. 

            var compareTimeInterval = triggersSet.TimeIntervalInMinutes;

            var pastEvent = triggersSet.Instances.Where(x => x.State == AlertState.Skipped ||
                                                             x.State == AlertState.Communicated).OrderByDescending(o => o.CreatedOn).FirstOrDefault();

            IEnumerable<VoC.Alerts.Context.Entities.TriggerEvent> triggerEvents = null;
            if (pastEvent == null)
            {
                triggerEvents = triggersSet.TriggerEvents.Where(x =>
                   (DateTime.UtcNow - x.ExecutionTime).TotalMinutes <= compareTimeInterval).ToList();
            }
            else
            {
                triggerEvents = triggersSet.TriggerEvents.Where(x =>
                    (DateTime.UtcNow - x.ExecutionTime).TotalMinutes <= compareTimeInterval && x.ExecutionTime > pastEvent.CreatedOn).ToList();
            }

            if (triggerEvents.Any())
            {
                var groupedTriggerEventsByTrigger = triggerEvents
                    .GroupBy(x => x.TriggerId).Select(x => new
                    {
                        TriggerId = x.Key,
                        TriggerEvents = x.Select(y => y)
                    });
                var triggersDefinitionsCount = triggersSet.Triggers.Count;
                if (triggersDefinitionsCount == groupedTriggerEventsByTrigger.Count())
                {
                    var firstTrigger = groupedTriggerEventsByTrigger.OrderByDescending(x => x.TriggerEvents.Count()).FirstOrDefault();
                    if (firstTrigger != null)
                    {
                        foreach (var triggerEvent in firstTrigger.TriggerEvents)
                        {
                            var respondentId = triggerEvent.TargetMatchId;
                            var responsesByTriggerCriteria = groupedTriggerEventsByTrigger
                                .Where(x => x.TriggerId != firstTrigger.TriggerId).Select(x => x.TriggerEvents)
                                .SelectMany(y => y).Where(z => z.TargetMatchId == respondentId).GroupBy(x => x.TriggerId);
                            var finalState = responsesByTriggerCriteria.Count() == triggersDefinitionsCount - 1 ? AlertState.Skipped : AlertState.Invalid;
                            if (finalState == AlertState.Skipped)
                                await _eventsRepository
                                    .GenerateAlertInstanceAsync(Guid.NewGuid(), @event, finalState, cancellationToken)
                                    .ConfigureAwait(false);
                        }
                    }
                }

            }
        }
        public async Task<AlertState> ProcessAsync(TriggerEvent @event, CancellationToken cancellationToken)
        {
            AlertState? finalState = null;
            var triggersSet = await _alertsRepository.GetTriggerSetAsync(@event.TriggersSetId).ConfigureAwait(false);
            if (triggersSet.Instances?.Count > 0)
            {

                var recentAlertInstance = triggersSet.Instances.Max(x => x.CreatedOn);
                if (recentAlertInstance != default && @event.ExecutionTime.Subtract(recentAlertInstance).TotalSeconds <= 10)
                {
                    // Similar event was just processed, skipping this one.
                    finalState = AlertState.Invalid;
                    return finalState.Value;
                }
            }

            //check for logical Operation AND
            //Get the Threshold value for trigger set
            //Get the CompareTimeInterval value for trigger set
            //Get all the triggers from trigger set
            //For Each Trigger, Get the triggerEvent which are greater than last communicated alert instance time  and  (DateTime.UtcNow - x.ExecutionTime).TotalMinutes <=  "CompareTimeInterval" and  compare it with "Threshold"
            // if any of the trigger doesn't  match the condition the alert instance  will be invalid 
            //if all the trigger match the condition, Compare it with notification frequency ,if the last sent time is greater than or equal to DateTime.UtcNow. Create alert Instance to Send out. 
            if (triggersSet.LogicalOperation == 1)
            {
                await ProcessAllTriggers(triggersSet, @event, cancellationToken);
                triggersSet = await _alertsRepository.GetTriggerSetAsync(@event.TriggersSetId).ConfigureAwait(false);
                if (@event.Outcome && new ThresholdHandler().ShouldProcessTrigger(triggersSet, triggersSet.Instances) && NotificationHandler.IsAboveLastSentAlertCreatedTime(triggersSet, triggersSet.Instances) && ValidateNonMultipleNotificationTrigger(triggersSet))
                {
                    finalState = AlertState.Initiated;
                    var alertInstanceId = Guid.NewGuid();
                    await _eventsRepository.GenerateAlertInstanceAsync(alertInstanceId, @event, finalState.Value, cancellationToken).ConfigureAwait(false);
                    await _communicationHandler.HandleAsync(alertInstanceId, @event, cancellationToken).ConfigureAwait(false);
                    await ResetTriggersSetAfterAlertInitiated(triggersSet);
                }
                else
                {
                    //Reset time for Non MultipleNotification Triggers
                    await UpdateLastResetTimeForNonMultipleNotificationAlert(triggersSet);
                }

            }


            if (triggersSet.LogicalOperation == 2) // Consider 2 is Logical OR. 
            {
                if (@event.Outcome && new ThresholdHandler().ShouldProcessTrigger(triggersSet, triggersSet.Instances) && NotificationHandler.IsAboveLastSentAlertCreatedTime(triggersSet, triggersSet.Instances) && ValidateNonMultipleNotificationTrigger(triggersSet))
                {
                    finalState = AlertState.Initiated;
                    var alertInstanceId = Guid.NewGuid();
                    await _eventsRepository.GenerateAlertInstanceAsync(alertInstanceId, @event, finalState.Value, cancellationToken).ConfigureAwait(false);
                    await _communicationHandler.HandleAsync(alertInstanceId, @event, cancellationToken).ConfigureAwait(false);
                    await ResetTriggersSetAfterAlertInitiated(triggersSet);
                }
                else
                {
                    // Don't notify users.
                    finalState = AlertState.Skipped;
                    await _eventsRepository.GenerateAlertInstanceAsync(Guid.NewGuid(), @event, finalState.Value, cancellationToken).ConfigureAwait(false);
                    await UpdateLastResetTimeForNonMultipleNotificationAlert(triggersSet);
                }
            }

            if (!finalState.HasValue)
            {
                return default;
            }
            await _eventsRepository.ProcessEventRecordAsync(@event.Id, cancellationToken).ConfigureAwait(false);
            return finalState.Value;
        }
        private Task ResetTriggersSetAfterAlertInitiated(AlertTriggerSet triggerSet)
        {
            triggerSet.LastResetDateTime = null;
             return _alertsRepository.UpdateAlertTriggerSetAsync(triggerSet);
        }

        private Task<bool> UpdateLastResetTimeForNonMultipleNotificationAlert(AlertTriggerSet triggerSet)
        {
            if (triggerSet.IsMultipleNotificationTrigger.HasValue && !triggerSet.IsMultipleNotificationTrigger.Value &&(triggerSet.Threshold==1 || !new ThresholdHandler().ShouldProcessTrigger(triggerSet, triggerSet.Instances)))
            {
                triggerSet.LastResetDateTime = DateTime.UtcNow;
                return UpdateOneTimeNotificationAlertResetTime(triggerSet);
            }
            return Task.Run(() => false);
        }
        private Task<bool> UpdateOneTimeNotificationAlertResetTime(AlertTriggerSet triggerSet)
        {
            return _alertsRepository.UpdateAlertTriggerSetAsync(triggerSet);
        }

        private bool ValidateNonMultipleNotificationTrigger(AlertTriggerSet triggerSet)
        {

            if (triggerSet.Instances.Count(x=>x.State==AlertState.Communicated) == 0)

                return true;

            if (triggerSet.IsMultipleNotificationTrigger is null)

                return true;

            if (triggerSet.IsMultipleNotificationTrigger.HasValue &&
                triggerSet.IsMultipleNotificationTrigger.Value)

                return true;

            if (triggerSet.IsMultipleNotificationTrigger.HasValue &&
               !triggerSet.IsMultipleNotificationTrigger.Value && triggerSet.LastResetDateTime.HasValue)

                return true;

            return false;
        }

    }



    public static class NotificationHandler
    {
        public static bool IsAboveLastSentAlertCreatedTime(AlertTriggerSet triggerSet, IEnumerable<AlertInstance> instances)
        {
            var recentCommunicatedAlertInstance = triggerSet.Instances.Where(x => x.State == AlertState.Communicated).ToList();

            //when no NotificationFrequencyInHours specified, Meaning  send the alert only once
            if (triggerSet.NotificationFrequencyInHours.HasValue && recentCommunicatedAlertInstance.Count > 0)
            {
                var duration = triggerSet.NotificationFrequencyInHours.Value;
                var createAlertNotAllowed = (DateTime.UtcNow - recentCommunicatedAlertInstance.Max(x => x.CreatedOn)).TotalHours < duration;
                if (createAlertNotAllowed)
                {
                    return false;
                }
            }
            return true;
        }
    }

    public class ThresholdHandler
    {
        private bool ShouldProcessTrigger(bool triggersBelowThreshold, int pastEventsCount, int threshold)
        {
            var isAboveThreshold = pastEventsCount > threshold;
            return (triggersBelowThreshold ^ isAboveThreshold) || (threshold == pastEventsCount);
        }
        public bool ShouldProcessTrigger(AlertTriggerSet triggerSet, IEnumerable<AlertInstance> instances)
        {

            if (triggerSet.Threshold.HasValue && !triggerSet.Triggers.FirstOrDefault().Type.Equals("SurveyVolumeTrigger"))
            {

                var threshold = (int?)triggerSet.Threshold ?? 0;
                var alertInstances = instances.ToList();
                var pastCommunicatedEvent = alertInstances.OrderByDescending(o => o.CreatedOn)
                    .FirstOrDefault(x => x.State == AlertState.Communicated);

                //when sending the subsequent alerts
                if (pastCommunicatedEvent != null && !(triggerSet.TriggersBelowThreshold ?? false))
                {
                    //when sending the subsequent alerts with compare time interval
                    if (triggerSet.TimeIntervalInMinutes.HasValue)
                    {
                        var compareInMinutes = triggerSet.TimeIntervalInMinutes.Value;
                        var totalEventsInCompareTimeIntervalRange = alertInstances.Where(x => x.CreatedOn > pastCommunicatedEvent.CreatedOn &&
                                                                    x.State == AlertState.Skipped &&
                                                                    (DateTime.UtcNow - x.CreatedOn).TotalMinutes <= compareInMinutes).OrderByDescending(x => x.CreatedOn).Count();
                        return ShouldProcessTrigger(triggerSet.TriggersBelowThreshold ?? false, totalEventsInCompareTimeIntervalRange, threshold );
                    }
                    //when sending the subsequent alerts without  compare time interval
                    var pastEvents = alertInstances.Where(x => x.CreatedOn > pastCommunicatedEvent.CreatedOn &&
                                                               x.State == AlertState.Skipped)
                        .OrderByDescending(x => x.CreatedOn)
                        .Count();
                    return ShouldProcessTrigger(triggerSet.TriggersBelowThreshold ?? false, pastEvents, threshold );
                }

                //when sending first alert with compare timeInterval 
                if (triggerSet.TimeIntervalInMinutes.HasValue)
                {
                    var compareInMinutes = triggerSet.TimeIntervalInMinutes.Value;
                    var totalEvents = alertInstances.Where(x => x.State == AlertState.Skipped &&
                                                               (DateTime.UtcNow - x.CreatedOn).TotalMinutes <= compareInMinutes).OrderByDescending(x => x.CreatedOn).Count();
                    return ShouldProcessTrigger(triggerSet.TriggersBelowThreshold ?? false, totalEvents, threshold );
                }

                //when sending first alert without compare timeInterval 
                var previousEvents = alertInstances.Where(x => x.State == AlertState.Skipped)
                    .OrderByDescending(x => x.CreatedOn)
                    .Count();
                return ShouldProcessTrigger(triggerSet.TriggersBelowThreshold ?? false, previousEvents, threshold );


            }

            return true;
        }
    }


}
