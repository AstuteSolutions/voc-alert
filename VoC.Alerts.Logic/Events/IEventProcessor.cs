﻿using System.Threading;
using System.Threading.Tasks;
using VoC.Alerts.Constants;
using VoC.Alerts.Domain;

namespace VoC.Alerts.Logic.Events
{
    public interface IEventProcessor
    {
        Task<AlertState> ProcessAsync(TriggerEvent @event, CancellationToken cancellationToken);
    }
}
