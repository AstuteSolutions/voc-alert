﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VoC.Alerts.Abstraction.Events;
using VoC.Alerts.Context;
using VoC.Alerts.Domain;

namespace VoC.Alerts.Logic.Events
{
    public class EventsStore : IEventsStore<TriggerEvent>
    {
        private readonly AlertsContext _context;

        public EventsStore(AlertsContext context)
        {
            _context = context;
        }

        public async Task Store(IEnumerable<TriggerEvent> events)
        {
            await _context.TriggerEvents.AddRangeAsync(events.Select(e => new Context.Entities.TriggerEvent
            {
                Id = e.Id,
                CompanyId = e.CompanyId,
                TriggerId = e.TriggerId,
                TriggersSetId = e.TriggersSetId,
                ExecutionTime = e.ExecutionTime,
                ProcessedOn = null,
                Output = e.Outcome,
                MatchLookupId = e.Match.Id,
                TargetMatchId = e.TargetMatchId
            })).ConfigureAwait(true);
            await _context.SaveChangesAsync(CancellationToken.None).ConfigureAwait(false);
        }

        public TriggerEvent GetAwaitingEvent(Guid eventId)
        {
            var eventEntity = _context.TriggerEvents.Include(i => i.TriggersSet)
                .Include(i => i.Trigger)
                .Include(i => i.MatchLookup)
                .FirstOrDefault(e => e.Id.Equals(eventId) && !e.ProcessedOn.HasValue);
            if (eventEntity == null)
            {
                return default;
            }

            return eventEntity.MatchLookupId.HasValue
                ? new TriggerEvent(eventEntity.Id, eventEntity.TriggerId, eventEntity.TriggersSetId, eventEntity.ExecutionTime,
                    eventEntity.CompanyId,
                    new TriggerStringLookup(eventEntity.MatchLookup.Id, eventEntity.TriggerId, eventEntity.MatchLookup.Value))
                : new TriggerEvent(eventEntity.Id, eventEntity.TriggerId, eventEntity.TriggersSetId, eventEntity.ExecutionTime,
                    eventEntity.CompanyId);
        }

        public IEnumerable<TriggerEvent> GetRelatedEvents(Guid eventId)
        {
            throw new NotImplementedException();
        }
    }
}
