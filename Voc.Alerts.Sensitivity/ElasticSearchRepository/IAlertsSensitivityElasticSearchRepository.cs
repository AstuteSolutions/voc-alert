﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VoC.Alerts.Domain;
using VoC.Alerts.Logic.Triggers;
using Voc.Alerts.Sensitivity.Filters;

namespace Voc.Alerts.Sensitivity.ElasticSearchRepository
{
    public interface IAlertsSensitivityElasticSearchRepository
    {
        Task<AlertSensitivityResponse> GetFilterRespondents(int companyId, int surveyId,
            IEnumerable<object> triggers, int timeInterval, int comparisionDays = 7);
    }
}