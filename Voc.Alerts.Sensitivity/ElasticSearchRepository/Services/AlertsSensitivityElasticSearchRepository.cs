﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Emplifi.VoC.Responses.Domain.Records;
using Emplifi.VoC.Responses.Domain.Records.Type;
using Emplifi.VoC.Responses.Repository;
using Emplifi.VoC.Responses.Repository.Utils;
using Microsoft.Extensions.Logging;
using Nest;
using Newtonsoft.Json;
using VoC.Alerts.Logic.Triggers;
using Voc.Alerts.Sensitivity.Filters;
using Voc.Alerts.Sensitivity.Models;

namespace Voc.Alerts.Sensitivity.ElasticSearchRepository.Services
{
    public class AlertsSensitivityElasticSearchRepository : ElasticRepositoryBase, IAlertsSensitivityElasticSearchRepository
    {
        private readonly ILogger<AlertsSensitivityElasticSearchRepository> _logger;
        private int _company;
        private IEnumerable<object> _triggers;
        private int _duration;
        private DateTime _from;
        private DateTime _to;
        private int _survey;
        private IEnumerable<Tuple<string, DateTime>> _dateRanges;
        public AlertsSensitivityElasticSearchRepository(IElasticClient elasticClient, ILogger<AlertsSensitivityElasticSearchRepository> logger) : base(elasticClient)
        {
            _logger = logger;
        }

        public async Task<AlertSensitivityResponse> GetFilterRespondents(int companyId, int surveyId, IEnumerable<object> triggers, int timeInterval, int comparisionDays = 7)
        {
            try
            {
                var previousCompareDaysMinusToday = comparisionDays + 1;
                var daysAgo = DateTime.UtcNow.AddDays(-comparisionDays);
                var fromDateTime = new DateTime(daysAgo.Year, daysAgo.Month, daysAgo.Day, 00, 00, 00, DateTimeKind.Utc);
                var toDateTime = DateTime.UtcNow.Date.AddTicks(-1);
                SetFilters(companyId, surveyId, triggers, timeInterval, toDateTime, fromDateTime);
                var uniqueResponses = await GetUniqueRespondents();
                _dateRanges = uniqueResponses.Select(x => new Tuple<string, DateTime>(x.Key, x.Date));
                var filteredResponses = await GetFilteredResponsesByTrigger();

                return new AlertSensitivityResponse
                (
                    uniqueResponses,
                    filteredResponses
                );
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error getting charts {triggers} {_duration} {numberOfDays}", JsonConvert.SerializeObject(triggers), timeInterval, comparisionDays);
                throw;
            }
        }

        private void SetFilters(int companyId, int surveyId, IEnumerable<object> triggers, int timeInterval, DateTime toDateTime,
            DateTime fromDateTime)
        {
            _company = companyId;
            _triggers = triggers;
            _to = toDateTime;
            _from = fromDateTime;
            _duration = timeInterval;
            _survey = surveyId;
        }

        private async Task<IEnumerable<DocumentsCountByKey>> GetFilteredRespondents()
        {
            const string mentionsAggregationTitle = "filterResponses";
            var filterBuilder = BuildFilters(_triggers);
            filterBuilder.Add(fil => fil.DateRange(dr => dr.Field(drf => drf.EndDatetime)
                .GreaterThanOrEquals(_from)
                .LessThanOrEquals(_to)));
            var query = Context.SearchAsync<VocDocument>(s => s
                .Index(_company.ToString())
                .From(0)
                .Take(DefaultBatchSize)
                .Query(q => q
                    .Bool(f => f.Filter(filterBuilder))).Aggregations(aggs => aggs
                    .DateHistogram(mentionsAggregationTitle, date => date
                        .Field(p => p.EndDatetime)
                        .FixedInterval($"{_duration}h")
                        .ExtendedBounds(_from, _to)
                        .MinimumDocumentCount(0)
                        .Order(HistogramOrder.KeyAscending).Aggregations(a =>
                            a.Cardinality("unique_responses", f =>
                                f.Field(d => d.RespondentId).PrecisionThreshold(DefaultBatchSize))))));
            var isValidQuery = await HandleQuery(query).GetSuccess();
            if (!isValidQuery) return null;
            var searchResponse = await query;
            var filteredResponses = searchResponse.Aggregations.DateHistogram(mentionsAggregationTitle).Buckets
                .Select(x => new DocumentsCountByKey((long)(x.Cardinality("unique_responses").Value ?? 0), x.KeyAsString)
                {
                    Date = x.Date
                });
            return filteredResponses;
        }


        private async Task<IEnumerable<DocumentsCountByKey>> GetFilteredResponsesByTrigger()
        {
            var processedResponseDetails = await ValidateTriggersByLogicalCondition();
            var documentsCountByKey = new List<DocumentsCountByKey>();
            for (var i = 0; i < _dateRanges.Count(); i++)
            {
                var (fromDateKey, from) = _dateRanges.ElementAt(i);
                var to = i == _dateRanges.Count() - 1 ? from.AddHours(_duration) : _dateRanges.ElementAt(i + 1).Item2;
                var responsesCount = processedResponseDetails.Count(x => x.SubmittedDateTime >= from && x.SubmittedDateTime < to);
                documentsCountByKey.Add(new DocumentsCountByKey(responsesCount, fromDateKey)
                {
                    Date = from
                });
            }
            return documentsCountByKey;
        }


        private async Task<IEnumerable<ProcessedResponseDetails>> ValidateTriggersByLogicalCondition()
        {
            var condition = _triggers.Count() > 1 ? "and" : "or";
            var processedResponseDetails = new List<ProcessedResponseDetails>();
            var filteredResponses = await GetAllFilteredResponses();
            var triggerWithHighResponses = filteredResponses.OrderByDescending(x => x.Responses.Count).FirstOrDefault();
            if (triggerWithHighResponses == null) return null;
            if (condition == "and")
            {
                var uniqueResponses = triggerWithHighResponses.Responses.GroupBy(x => x.RespondentId);
                foreach (var response in uniqueResponses)
                {
                    var respondentIdInOtherFilteredResponses = filteredResponses
                        .Where(x => !x.Id.Equals(triggerWithHighResponses.Id))
                        .SelectMany(y => y.Responses).Where(res => res.RespondentId == response.Key);
                    if (respondentIdInOtherFilteredResponses.Count() == _triggers.Count() - 1)
                    {
                        processedResponseDetails.Add(new ProcessedResponseDetails()
                        {
                            RespondentId = response.Key,
                            SubmittedDateTime = response.FirstOrDefault()!.EndDatetime
                        });
                    }
                }
                return processedResponseDetails;
            }

            return ValidateTriggersResponsesByLogicalOr(filteredResponses);

        }


        private static IEnumerable<ProcessedResponseDetails> ValidateTriggersResponsesByLogicalOr(List<TriggersWithResponses> triggersWithResponses)
        {
            var allResponses = triggersWithResponses.SelectMany(x => x.Responses).GroupBy(x => x.RespondentId)
                .Select(fil => new ProcessedResponseDetails()
                {
                    RespondentId = fil.Key,
                    SubmittedDateTime = fil.FirstOrDefault()!.EndDatetime
                });
            return allResponses;

        }

        private async Task<List<TriggersWithResponses>> GetAllFilteredResponses()
        {
            var triggerWithFilterResponses = new List<TriggersWithResponses>();
            foreach (var trigger in _triggers)
            {
                var filteredResponses = await GetResponsesByTriggerDefinition(trigger);
                triggerWithFilterResponses.Add(new TriggersWithResponses()
                {
                    Responses = filteredResponses,
                    Id = Guid.NewGuid()
                });
            }
            return triggerWithFilterResponses;
        }



        private async Task<List<VocDocument>> GetResponsesByTriggerDefinition(object trigger)
        {
            var filterBuilder = new List<Func<QueryContainerDescriptor<VocDocument>, QueryContainer>>();
            var filter = GetFilterBasedOnTypeOfAlert(trigger);
            filterBuilder.AddRange(filter);
            filterBuilder.Add(fil => fil.DateRange(dr => dr.Field(drf => drf.EndDatetime)
                .GreaterThanOrEquals(_from)
                .LessThanOrEquals(_to)));
            var query = Context.SearchAsync<VocDocument>(s => s
                .Index(_company.ToString())
                .From(0)
                .Take(DefaultBatchSize)
                .Query(q => q
                    .Bool(f => f.Filter(filterBuilder))));
            var isValidQuery = await HandleQuery(query).GetSuccess();
            if (!isValidQuery) return null;
            return await HandleScrollSearch(query);
        }

        private async Task<IEnumerable<DocumentsCountByKey>> GetUniqueRespondents()
        {
            const string mentionsAggregationTitle = "responses";
            var filterBuilder = new List<Func<QueryContainerDescriptor<VocDocument>, QueryContainer>>
            {
                fil => fil.DateRange(dr => dr.Field(drf => drf.EndDatetime)
                    .GreaterThanOrEquals(_from)
                    .LessThanOrEquals(_to)),
                x => x.Term(t => t.Field(f => f.Survey.SurveyId).Value(_survey))
            };
            var query = Context.SearchAsync<VocDocument>(s => s
                .Index(_company.ToString())
                .From(0)
                .Take(DefaultBatchSize)
                .Query(q => q
                    .Bool(f => f.Filter(filterBuilder))).Aggregations(aggs => aggs
                    .DateHistogram(mentionsAggregationTitle, date => date
                        .Field(p => p.EndDatetime)
                        .FixedInterval($"{_duration}h")
                        .ExtendedBounds(_from, _to)
                        .MinimumDocumentCount(0)
                        .Order(HistogramOrder.KeyAscending).Aggregations(a =>
                            a.Cardinality("unique_responses", f =>
                                f.Field(d => d.RespondentId).PrecisionThreshold(DefaultBatchSize))))));
            var isValidQuery = await HandleQuery(query);
            if (!isValidQuery.IsValid)
            {
                _logger.LogError("Error on Querying Elastic Search {error}, {servererror}", 
                    JsonConvert.SerializeObject(isValidQuery.OriginalException,new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    }), 
                    JsonConvert.SerializeObject(isValidQuery.ServerError, new JsonSerializerSettings()
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    }));
                return null;
            }
            var searchResponse = await query;
            var respondents = searchResponse.Aggregations.DateHistogram(mentionsAggregationTitle).Buckets
                .Select(x => new DocumentsCountByKey((long)(x.Cardinality("unique_responses").Value ?? 0), x.KeyAsString)
                {
                    Date = x.Date
                });
            return respondents;
        }

        public List<Func<QueryContainerDescriptor<VocDocument>, QueryContainer>> BuildFilters(IEnumerable<object> triggers)
        {
            var filterBuilder = new List<Func<QueryContainerDescriptor<VocDocument>, QueryContainer>>();
            foreach (var trigger in triggers)
            {
                filterBuilder.AddRange(GetFilterBasedOnTypeOfAlert(trigger));
            }
            return filterBuilder;
        }

        private Func<QueryContainerDescriptor<VocDocument>, QueryContainer>[] GetFilterBasedOnTypeOfAlert(
            object trigger)
        {
            var baseTrigger = (Trigger<TriggerEvents>)trigger;
            switch (baseTrigger.Type)
            {
                case "OpenTextQuestion":
                    {
                        var request = (OpenTextQuestionTrigger)trigger;
                        var filterText = request.Lookups.Select(x => x.Value).ToArray();
                        var filterQuestion = request.QuestionId;
                        var surveyId = request.SurveyId;
                        if (filterText.Any())
                        {
                            Func<QueryContainerDescriptor<VocDocument>, QueryContainer> filter =
                                s => s.Bool(b => b.Filter(
                                    m => m.Term(t => t.Question.QuestionId, filterQuestion),
                                    m => m.Term(t => t.Survey.SurveyId, surveyId),
                                    x => x.Term(e => e.Type, (int)RecordType.Response),
                                    fil => fil.QueryString(
                                        mm => mm.Fields(fs => fs.Field(f => f.Text)
                                        ).Query(FullTextQueryBuilder(filterText)).DefaultOperator(Operator.Or)
                                    )));
                            return new[] { filter };
                        }
                        break;
                    }

                case "SingleSelectQuestion":
                    {
                        var request = (SingleSelectQuestionTrigger)trigger;
                        var tagIds = request.Lookups.Select(x => x.Value).ToArray();
                        var filterQuestion = request.QuestionId;
                        if (tagIds.Any())
                        {
                            return QuestionTagFilterBuilder(request.SurveyId, new Dictionary<int, int[]>() { { filterQuestion, tagIds } });
                        }
                        break;
                    }
                case "SingleSelectScoreQuestion":
                    {
                        var request = (SingleSelectScoreQuestionTrigger)trigger;
                        var scores = new decimal?[] { request.LeftBound, request.RightBound };
                        var filterQuestion = request.QuestionId;
                        if (scores.Length == 2)
                        {
                            return ScoreQuestionFilter(request.SurveyId, new KeyValuePair<int, decimal?[]>(filterQuestion, scores));
                        }
                        break;
                    }
                case "SurveyVolumeTrigger":
                    {
                        var request = (SurveyVolumeTrigger)trigger;

                        Func<QueryContainerDescriptor<VocDocument>, QueryContainer> filter =
                            s => s.Bool(b => b.Filter(
                                m => m.Term(t => t.Survey.SurveyId, request.SurveyId)));
                        return new[] { filter };
                    }
                default:
                    throw new NotSupportedException($"Doesn't support trigger type {baseTrigger.Type}");
            }

            return null;
        }

        private static string FullTextQueryBuilder(string[] keywords) => string.Join(" OR ", keywords.Select((keyword) => keyword.Trim().Contains(" ") ? "(" + keyword + ")" : "*" + keyword + "*"));

        private static Func<QueryContainerDescriptor<VocDocument>, QueryContainer>[] QuestionTagFilterBuilder(int surveyId, Dictionary<int, int[]> questionsWithTags)
        {
            var result = new List<Func<QueryContainerDescriptor<VocDocument>, QueryContainer>>();
            foreach (var qTags in questionsWithTags)
            {
                Func<QueryContainerDescriptor<VocDocument>, QueryContainer> filter =
                    s => s.Bool(b => b.Filter(
                        m => m.Term(t => t.Question.QuestionId, qTags.Key),
                        x => x.Term(e => e.Type, (int)RecordType.Response),
                        m => m.Term(t => t.Survey.SurveyId, surveyId),
                        n => n.Terms(t => t.Field(f => f.TagId).Terms(qTags.Value))
                    ));
                result.Add(filter);
            }
            return result.ToArray();
        }

        private static Func<QueryContainerDescriptor<VocDocument>, QueryContainer>[] ScoreQuestionFilter(int surveyId, KeyValuePair<int, decimal?[]> scoreQuestionWithTags)
        {
            var result = new List<Func<QueryContainerDescriptor<VocDocument>, QueryContainer>>();

            var qTagsInOrder = scoreQuestionWithTags.Value.OrderBy(x => x).ToArray();
            if (qTagsInOrder.Count() == 2)
            {
                var rightBound = (double?)qTagsInOrder[1];
                var leftBound = (double?)qTagsInOrder[0];
                Func<QueryContainerDescriptor<VocDocument>, QueryContainer> filter =
                    s => s.Bool(b => b.Filter(
                        m => m.Term(t => t.Survey.SurveyId, surveyId),
                        x => x.Term(e => e.Type, (int)RecordType.Response),
                        m => m.Term(t => t.Question.QuestionId, scoreQuestionWithTags.Key),
                        n => n.Range(t =>
                            t.Field(f => f.Score).GreaterThanOrEquals(leftBound).LessThanOrEquals(rightBound))));
                result.Add(filter);
            }
            return result.ToArray();
        }


    }
}