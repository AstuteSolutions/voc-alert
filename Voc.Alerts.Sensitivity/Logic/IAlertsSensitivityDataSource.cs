﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Voc.Alerts.Sensitivity.ElasticSearchRepository;
using Voc.Alerts.Sensitivity.Filters;
using Voc.Alerts.Sensitivity.Models;

namespace Voc.Alerts.Sensitivity.Logic
{
    public interface IAlertsSensitivityDataSource
    {
        Task<AlertSensitivityResponse> GetAlertsSensitivityCharts(AlertSensitivityRequest request);
    }

    public class ElasticSearchData : IAlertsSensitivityDataSource
    {
        private readonly IAlertsSensitivityElasticSearchRepository _alertsSensitivityElasticSearchRepository;

        public ElasticSearchData(IAlertsSensitivityElasticSearchRepository alertsSensitivityElasticSearchRepository)
        {
            _alertsSensitivityElasticSearchRepository = alertsSensitivityElasticSearchRepository;
        }


        public Task<AlertSensitivityResponse> GetAlertsSensitivityCharts(AlertSensitivityRequest request)
        {
            return _alertsSensitivityElasticSearchRepository.GetFilterRespondents(request.CompanyId, request.SurveyId,
                request.Triggers, request.TimeInterval, request.ComparisionDays);
        }
    }

    public class SqlData : IAlertsSensitivityDataSource
    {
        public Task<AlertSensitivityResponse> GetAlertsSensitivityCharts(AlertSensitivityRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
