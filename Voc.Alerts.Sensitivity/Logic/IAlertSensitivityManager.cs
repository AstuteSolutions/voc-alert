﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Voc.Alerts.Sensitivity.ElasticSearchRepository;
using Voc.Alerts.Sensitivity.Filters;
using Voc.Alerts.Sensitivity.Models;

namespace Voc.Alerts.Sensitivity.Logic
{
    public interface IAlertSensitivityManager
    {
        Task<AlertSensitivityResponse> GetAlertSensitivityChart(AlertSensitivityRequest alertSensitivityRequest);
    }


    public class AlertSensitivityManager : IAlertSensitivityManager
    {
        private readonly IAlertsSensitivityDataSource _alertsSensitivityDataSource;

        public AlertSensitivityManager(IAlertsSensitivityDataSource alertsSensitivityDataSource)
        {
            _alertsSensitivityDataSource = alertsSensitivityDataSource;
        }

        public Task<AlertSensitivityResponse> GetAlertSensitivityChart(AlertSensitivityRequest alertSensitivityRequest)
        {
            return _alertsSensitivityDataSource.GetAlertsSensitivityCharts(alertSensitivityRequest);
        }
    }
}
