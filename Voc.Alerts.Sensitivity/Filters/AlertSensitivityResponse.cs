﻿using System;
using System.Collections.Generic;
using System.Text;
using Voc.Alerts.Sensitivity.Models;

namespace Voc.Alerts.Sensitivity.Filters
{
    public class AlertSensitivityResponse
    {
        public AlertSensitivityResponse(IEnumerable<DocumentsCountByKey> respondentData, IEnumerable<DocumentsCountByKey> triggersData)
        {
            RespondentData = respondentData;
            TriggersData = triggersData;
        }
        public IEnumerable<DocumentsCountByKey> RespondentData { get; }
        public IEnumerable<DocumentsCountByKey> TriggersData { get; }
    }
}
