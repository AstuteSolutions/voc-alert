﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emplifi.VoC.Responses.Domain.Enums;
using Nest;
using DateRange = Emplifi.VoC.Responses.Domain.CustomModels.DateRange;

namespace Voc.Alerts.Sensitivity.Filters
{
    public class Filters
    {
        private const int MaxNumberOfRecords = 10000;
        private int _size;
        private int _from;

        public Filters(int surveyId, DateRange dateRange, int from = 0, int size = 20)
        {
            SurveyId = surveyId;
            DateRange = dateRange;
            Size = size;
            From = from;
        }
        public int SurveyId { get; }



        public DateRange DateRange { get; }

        public int Size
        {
            get => _size;
            private set
            {
                if (value <= 0 || value > MaxNumberOfRecords) throw new NotSupportedException($" Size should not be lesser than 0 or  greater than {MaxNumberOfRecords}");
                _size = value;
            }
        }
        public int From
        {
            get => _from;
            private set
            {
                if (value < 0 || value > MaxNumberOfRecords) throw new NotSupportedException($" From should not be lesser than 0 or greater than {MaxNumberOfRecords}");
                _from = value;
            }
        }

        public string Type { get; }
    }




    public class SearchFilters: Filters
    {
        private string[] _sentiment;
        public Dictionary<string, Dictionary<int, object>> Questions { get; set; }=new Dictionary<string, Dictionary<int, object>>();
        public string[] Themes { get; set; } = Array.Empty<string>();
        public double[] NpsRange { get; set; } = Array.Empty<double>();

        public string[] Sentiment
        {
            get => _sentiment; set
            {
                if (value == null)
                { _sentiment = Array.Empty<string>(); return; }
                if (!value.Any()) { _sentiment = Array.Empty<string>(); return; }
                _sentiment = new string[value.Length];
                foreach (var sentiment in value.Select((sentimentValue, index) => new { sentimentValue, index }))
                {
                    if (sentiment.sentimentValue.Equals("positive", StringComparison.InvariantCultureIgnoreCase) ||
                        sentiment.sentimentValue.Equals("negative", StringComparison.InvariantCultureIgnoreCase) || sentiment.sentimentValue.Equals("neutral", StringComparison.InvariantCultureIgnoreCase))
                        _sentiment[sentiment.index] = sentiment.sentimentValue;
                    else
                        throw new NotSupportedException(" Sentiments can be either positive,negative,neutral");
                }

            }
        }
        public SearchFilters(int surveyId, DateRange dateRange, int from = 0, int size = 20) : base(surveyId, dateRange, from, size)
        {
        }
    }
   
}
