﻿using System;
using System.Collections.Generic;
using System.Text;
using VoC.Alerts.Domain;
using VoC.Alerts.Logic.Triggers;

namespace Voc.Alerts.Sensitivity.Models
{
    public class AlertSensitivityRequest
    {
        public int CompanyId { get; set; }
        public int SurveyId { get; set; }
        public IEnumerable<object> Triggers { get; set; } = Array.Empty<object>();
        public int TimeInterval { get; set; }
        public int ComparisionDays { get; set; }
    }
}
