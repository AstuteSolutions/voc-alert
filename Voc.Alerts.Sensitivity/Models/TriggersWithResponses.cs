﻿using System;
using System.Collections.Generic;
using Emplifi.VoC.Responses.Domain.Records;

namespace Voc.Alerts.Sensitivity.Models
{
    public class TriggersWithResponses
    {
        public List<VocDocument> Responses { get; set; } = new List<VocDocument>();
        public Guid Id { get; set; }
    }
}