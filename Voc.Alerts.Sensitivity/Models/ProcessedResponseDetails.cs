﻿using System;

namespace Voc.Alerts.Sensitivity.Models
{
    public class ProcessedResponseDetails
    {
        public long RespondentId { get; set; }
        public DateTime SubmittedDateTime { get; set; }
    }
}