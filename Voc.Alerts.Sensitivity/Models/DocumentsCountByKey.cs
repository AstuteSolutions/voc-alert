﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Voc.Alerts.Sensitivity.Models
{
    public class DocumentsCountByKey
    {
        public DocumentsCountByKey(long count, string key)
        {
            Count = count;
            Key = key;
        }
        public string Key { get; }
        public long Count { get; }
        public DateTime Date { get; set; }
    }

    public class DocumentsCountByAverage
    {
        public DocumentsCountByAverage(double average, string key)
        {
            Average = average;
            Key = key;
        }
        public string Key { get; }
        public double Average { get; }
    }

    public class SmartDetectionAverage
    {
        public IEnumerable<DocumentsCountByAverage> DocumentsCountByAverage { get; set; } =
            Array.Empty<DocumentsCountByAverage>();
        public double Average { get; set; }
    }
}
