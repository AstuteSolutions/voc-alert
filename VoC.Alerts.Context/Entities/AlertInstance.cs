﻿using System;
using VoC.Alerts.Constants;

namespace VoC.Alerts.Context.Entities
{
    public class AlertInstance
    {
        public Guid Id { get; set; }
        public Guid AlertDefinitionId { get; set; }
        public virtual AlertDefinition AlertDefinition { get; set; }
        public int CompanyId { get; set; }
        public Guid CommunicationDefinitionId { get; set; }
        public virtual AlertCommunicationDefinition CommunicationDefinition { get; set; }
        public string Message { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public AlertState State { get; set; }
        public Guid TriggerSetId { get; set; }
        public virtual AlertTriggerSet TriggerSet { get; set; }
    }
}
