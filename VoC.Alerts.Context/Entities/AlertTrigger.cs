﻿using System;
using System.Collections.Generic;

namespace VoC.Alerts.Context.Entities
{
    public class AlertTrigger
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public int SourceId { get; set; }
        public AlertSource Source { get; set; }
        public Guid TriggerSetId { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public virtual AlertTriggerSet TriggerSet { get; set; }
        public virtual ICollection<TriggerLookup> Lookups { get; set; }
        public virtual ICollection<TriggerEvent> TriggerEvents { get; set; }
    }
}
