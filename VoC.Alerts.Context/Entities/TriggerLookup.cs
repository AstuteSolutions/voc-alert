﻿using System;

namespace VoC.Alerts.Context.Entities
{
    public class TriggerLookup
    {
        public Guid Id { get; set; }
        public string Value { get; set; }
        public int TriggerId { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public virtual AlertTrigger Trigger { get; set; }
    }
}
