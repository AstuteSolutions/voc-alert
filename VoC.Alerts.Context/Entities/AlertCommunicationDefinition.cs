﻿using System;
using System.Collections.Generic;

namespace VoC.Alerts.Context.Entities
{
    public class AlertCommunicationDefinition
    {
        public Guid Id { get; set; }
        public Guid AlertDefinitionId { get; set; }
        public int CompanyId { get; set; }
        public string Message { get; set; }
        public virtual AlertDefinition Definition { get; set; }
        public virtual ICollection<AlertCommunicationRecipient> Recipients { get; set; }
    }
}
