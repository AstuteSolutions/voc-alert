﻿using System;

namespace VoC.Alerts.Context.Entities
{
    public class TriggerEvent
    {
        public Guid Id { get; set; }
        public int CompanyId { get; set; }
        public DateTime ExecutionTime { get; set; }
        public DateTime? ProcessedOn { get; set; }
        public bool Output { get; set; }
        public Guid TriggersSetId { get; set; }
        public AlertTriggerSet TriggersSet { get; set; }
        public int TriggerId { get; set; }
        public virtual AlertTrigger Trigger { get; set; }
        public Guid? MatchLookupId { get; set; }
        public virtual TriggerLookup MatchLookup { get; set; }
        public string TargetMatchId { get; set; }
    }
}
