﻿using System;

namespace VoC.Alerts.Context.Entities
{
    public class AlertCommunicationRecipient
    {
        public Guid Id { get; set; }
        public Guid CommunicationDefinitionId { get; set; }
        public virtual AlertCommunicationDefinition CommunicationDefinition { get; set; }
        public int CommunicationChannelId { get; set; }
        public string Address { get; set; }
        public bool Enabled { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
    }

    public class EmailCommunicationRecipient : AlertCommunicationRecipient
    {
    }
}
