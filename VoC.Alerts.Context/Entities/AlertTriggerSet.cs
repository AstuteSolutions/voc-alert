﻿using System;
using System.Collections.Generic;

namespace VoC.Alerts.Context.Entities
{
    public class AlertTriggerSet
    {
        public Guid Id { get; set; }
        public Guid AlertDefinitionId { get; set; }
        public int LogicalOperation { get; set; } = 2;
        public decimal? Threshold { get; set; }
        public int? TimeIntervalInMinutes { get; set; }
        public int? NotificationFrequencyInHours { get; set; }
        public bool? IsMultipleNotificationTrigger { get; set; }
        public decimal? SmartDetectionPercentage { get; set; }
        public DateTime? LastRun { get; set; }
        public Guid CommunicationDefinitionId { get; set; }
        public DateTime? LastResetDateTime { get; set; }
        public virtual AlertDefinition AlertDefinition { get; set; }
        public virtual AlertCommunicationDefinition CommunicationDefinition { get; set; }
        public virtual ICollection<AlertTrigger> Triggers { get; set; }
        public virtual ICollection<AlertInstance> Instances { get; set; }
        public virtual ICollection<TriggerEvent> TriggerEvents { get; set; }
        public bool? TriggersBelowThreshold { get; set;}
    }
}
