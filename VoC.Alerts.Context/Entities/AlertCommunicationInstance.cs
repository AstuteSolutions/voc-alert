﻿using System;
using VoC.Alerts.Constants;

namespace VoC.Alerts.Context.Entities
{
    public class AlertCommunicationInstance
    {
        public Guid Id { get; set; }
        public Guid AlertInstanceId { get; set; }
        public virtual AlertInstance AlertInstance { get; set; }
        public Guid RecipientId { get; set; }
        public virtual AlertCommunicationRecipient Recipient { get; set; }
        public CommunicationState State { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
