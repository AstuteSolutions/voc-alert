﻿using System;
using System.Collections.Generic;

namespace VoC.Alerts.Context.Entities
{
    public class AlertEventInstance
    {
        public Guid Id { get; set; }
        public int CompanyId { get; set; }
        public DateTime CreatedOn { get; set; }
        public virtual ICollection<AlertCommunicationRecipient> Recipients { get; set; }
    }
}
