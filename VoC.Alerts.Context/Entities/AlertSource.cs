﻿namespace VoC.Alerts.Context.Entities
{
    public class AlertSource
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
    }
}
