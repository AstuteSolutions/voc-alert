﻿using System;

namespace VoC.Alerts.Context.Entities
{
    public class AlertDefinition
    {
        public Guid Id { get; set; }
        public int SourceId { get; set; }
        public AlertSource Source { get; set; }
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public bool Enabled { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public DateTime? ActivatedOn { get; set; }
        public virtual AlertCommunicationDefinition AlertCommunicationDefinition { get; set; }
        public virtual AlertTriggerSet TriggersSet { get; set; }
    }
}
