﻿using Microsoft.EntityFrameworkCore;
using VoC.Alerts.Context.Entities;

namespace VoC.Alerts.Context
{
    public class AlertsContext : DbContext
    {
        public AlertsContext(DbContextOptions<AlertsContext> options) : base(options)
        {
        }

        public DbSet<AlertDefinition> Definitions { get; set; }
        public DbSet<AlertTrigger> Triggers { get; set; }
        public DbSet<TriggerLookup> TriggerLookUps { get; set; }
        public DbSet<AlertSource> Sources { get; set; }
        public DbSet<TriggerEvent> TriggerEvents { get; set; }
        public DbSet<AlertCommunicationDefinition> CommunicationDefinitions { get; set; }
        public DbSet<AlertInstance> AlertInstances { get; set; }
        public DbSet<AlertTriggerSet> TriggerSets { get; set; }
        public DbSet<AlertCommunicationInstance> CommunicationInstances { get; set; }
        public DbSet<AlertCommunicationRecipient> CommunicationRecipients { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AlertSource>(e =>
            {
                e.ToTable("Sources", "Alert");
                e.HasKey(k => k.Id);
            });

            modelBuilder.Entity<AlertDefinition>(e =>
            {
                e.ToTable("Definitions", "Alert");
                e.HasKey(k => k.Id);
                e.HasOne(s => s.Source);
                e.HasOne(o => o.TriggersSet).WithOne(oo => oo.AlertDefinition);
            });

            modelBuilder.Entity<AlertTriggerSet>(e =>
            {
                e.ToTable("TriggerSets", "Alert");
                e.HasKey(k => k.Id);
                e.HasOne(o => o.AlertDefinition).WithOne(oo => oo.TriggersSet)
                    .HasForeignKey<AlertTriggerSet>(f => f.AlertDefinitionId);
                e.HasOne(o => o.CommunicationDefinition).WithMany().HasForeignKey(f => f.CommunicationDefinitionId);
                e.HasMany(m => m.Triggers).WithOne(o => o.TriggerSet).HasForeignKey(f => f.TriggerSetId);
                e.HasMany(m => m.Instances).WithOne(o => o.TriggerSet).HasForeignKey(f => f.TriggerSetId);
                e.HasMany(m => m.TriggerEvents).WithOne(o => o.TriggersSet).HasForeignKey(f => f.TriggersSetId);
            });

            modelBuilder.Entity<TriggerLookup>(e =>
            {
                e.ToTable("TriggerLookups", "Alert");
                e.HasKey(k => k.Id);
                e.HasOne(o => o.Trigger).WithMany(i => i.Lookups).HasForeignKey(f => f.TriggerId);
            });

            modelBuilder.Entity<AlertTrigger>(e =>
            {
                e.ToTable("Triggers", "Alert");
                e.HasKey(k => k.Id);
                e.HasOne(o => o.Source).WithMany();
                e.HasMany(m => m.Lookups).WithOne(o => o.Trigger).HasForeignKey(f => f.TriggerId);
                e.HasOne(o => o.TriggerSet).WithMany(m => m.Triggers).HasForeignKey(f => f.TriggerSetId);
                e.HasMany(t => t.TriggerEvents).WithOne(at => at.Trigger).HasForeignKey(f=>f.TriggerId);
            });

            modelBuilder.Entity<TriggerEvent>(e =>
            {
                e.ToTable("TriggerEvents", "Alert");
                e.HasKey(k => k.Id);
                e.HasOne(o => o.TriggersSet).WithMany(m=>m.TriggerEvents).HasForeignKey(f => f.TriggersSetId);
                e.HasOne(o => o.Trigger).WithMany(m=>m.TriggerEvents).HasForeignKey(f => f.TriggerId);
                e.HasOne(o => o.MatchLookup).WithMany().HasForeignKey(f => f.MatchLookupId);
            });

            modelBuilder.Entity<AlertCommunicationDefinition>(e =>
            {
                e.ToTable("CommunicationDefinitions", "Alert");
                e.HasKey(k => k.Id);
                e.HasOne(o => o.Definition).WithOne(cd=>cd.AlertCommunicationDefinition)
                    .HasForeignKey<AlertCommunicationDefinition>(f => f.AlertDefinitionId);
                e.HasMany(m => m.Recipients)
                    .WithOne(d => d.CommunicationDefinition)
                    .HasForeignKey(f => f.CommunicationDefinitionId);
            });

            modelBuilder.Entity<AlertCommunicationRecipient>(e =>
            {
                e.ToTable("CommunicationRecipients", "Alert");
                e.HasKey(k => k.Id);
                e.HasOne(o => o.CommunicationDefinition).WithMany(m => m.Recipients)
                    .HasForeignKey(f => f.CommunicationDefinitionId);
            });

            modelBuilder.Entity<AlertInstance>(e =>
            {
                e.ToTable("Instances", "Alert");
                e.HasKey(k => k.Id);
                e.HasOne(o => o.AlertDefinition).WithMany().HasForeignKey(f => f.AlertDefinitionId);
                e.HasOne(o => o.CommunicationDefinition).WithMany().HasForeignKey(f => f.CommunicationDefinitionId);
                e.HasOne(o => o.TriggerSet).WithMany(m => m.Instances).HasForeignKey(f => f.TriggerSetId);
            });

            modelBuilder.Entity<AlertCommunicationInstance>(e =>
            {
                e.ToTable("CommunicationInstances", "Alert");
                e.HasKey(k => k.Id);
                e.HasOne(o => o.AlertInstance).WithMany().HasForeignKey(f => f.AlertInstanceId);
                e.HasOne(o => o.Recipient).WithMany().HasForeignKey(f => f.RecipientId);
            });
        }
    }
}
