﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using VoC.Alerts.Entities.ViewModels;
using VoC.Alerts.Manager.Commands;

namespace VoC.Alerts.Web.Api.Controllers
{
    [Route("api/[controller]")]
    public class AlertsController : BaseController
    {
        private readonly IMediator _mediator;

        public AlertsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<ActionResult<Guid>> CreateAsync([FromBody] DefineAlertCommand command)
        {
            var result = await _mediator.Send(command).ConfigureAwait(false);
            return new JsonResult(result);
        }

        [HttpPut]
        public async Task<ActionResult<bool>> UpdateAsync([FromBody] UpdateAlertCommand command)
        {
            var result = await _mediator.Send(command).ConfigureAwait(false);
            if (!result)
            {
                return new JsonResult(false)
                {
                    Value = $"Error deleting alert definition with Id {command.Id}",
                    StatusCode = (int)HttpStatusCode.BadRequest
                };
            }
            return new JsonResult(true);
        }

        [HttpDelete("{alertId:guid}")]
        public async Task<ActionResult<bool>> Delete([FromRoute] Guid alertId)
        {
            var result = await _mediator.Send(new BaseCommand<bool>()
            {
                AlertId = alertId
            }).ConfigureAwait(false);
            if (!result)
            {
                return new JsonResult(false)
                {
                    Value = $"Error deleting alert definition with Id {alertId}",
                    StatusCode = (int)HttpStatusCode.BadRequest
                };
            }
            return new JsonResult(true);
        }

        [HttpGet("{alertId:guid}")]
        public async Task<ActionResult<AlertDefinitionViewModel>> Get([FromRoute] Guid alertId)
        {
            var result = await _mediator.Send(new BaseCommand<AlertDefinitionViewModel>()
            {
                AlertId = alertId
            }).ConfigureAwait(false);
            if (result == null)
            {
                return new JsonResult(false)
                {
                    Value = $"Error getting alert for Alert with Id {alertId}",
                    StatusCode = (int)HttpStatusCode.BadRequest
                };
            }
            return new JsonResult(result);
        }


        [HttpGet("company/{companyId:int}")]
        public async Task<ActionResult<IEnumerable<AlertDefinitionViewModel>>> GetCompanyAlerts([FromRoute] int companyId)
        {
            var result = await _mediator.Send(new BaseCommand<IEnumerable<AlertDefinitionViewModel>>()
            {
                CompanyId = companyId
            }).ConfigureAwait(false);
            if (result == null)
            {
                return new JsonResult(false)
                {
                    Value = $"Error getting alerts for company with Id {companyId}",
                    StatusCode = (int)HttpStatusCode.BadRequest
                };
            }
            return new JsonResult(result);
        }

        [HttpPost("{alertId:guid}/message")]
        public async Task<ActionResult<Guid>> SetCommunicationMessage([FromRoute] Guid alertId, [FromBody] DefineAlertCommunicationCommand command)
        {
            command.AlertId = alertId;
            var result = await _mediator.Send(command).ConfigureAwait(false);
            return new JsonResult(result);
        }



        [HttpPost("{alertId:guid}/triggers")]
        public async Task<ActionResult<List<TriggerData>>> AddTriggers([FromRoute] Guid alertId, [FromBody] AddOrUpdateTriggersCommand command)
        {
            command.AlertId = alertId;
            var result = await _mediator.Send(command).ConfigureAwait(false);
            return new JsonResult(result);
        }

        [HttpPost("{alertId:guid}/triggers/activate/{activate:bool}")]
        public async Task<ActionResult<bool>> ActivateTriggers([FromRoute] Guid alertId, [FromRoute] bool activate)
        {
            var result = await _mediator.Send(new ActivateTriggersCommand
            {
                AlertId = alertId,
                Activate = activate
            }).ConfigureAwait(false);
            return new JsonResult(result);
        }

        [HttpPost("{alertId:guid}/triggers/threshold/{threshold:decimal?}")]
        public async Task<JsonResult> SetTriggerThreshold([FromRoute] Guid alertId, [FromRoute] decimal? threshold)
        {
            var result = await _mediator.Send(new SetTriggersThresholdCommand(alertId, threshold))
                .ConfigureAwait(false);
            return new JsonResult(result);
        }


        [HttpPost("evaluate/{alertId:guid}")]
        public async Task<ActionResult<AlertEvalutionResponse>> EvaluateAlert([FromRoute] Guid alertId, [FromBody] List<long> RespondentIds)
        {
            var result = await _mediator.Send(new AlertEvaluateCommand()
            {
                AlertId = alertId,
                Respondents = RespondentIds
            })
                .ConfigureAwait(false);
            return new JsonResult(result);
        }

        [HttpPost("evaluateByDateRange/{alertId:guid}")]
        public async Task<ActionResult<AlertEvalutionResponse>> EvaluateAlert([FromRoute] Guid alertId, [FromBody] DateRange DateRange)
        {
            var result = await _mediator.Send(new AlertEvaluateByDateRangeCommand()
            {
                AlertId = alertId,
                DateRange = DateRange
            })
                .ConfigureAwait(false);
            return new JsonResult(result);
        }
    }
}
