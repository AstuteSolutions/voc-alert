﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using VoC.Alerts.Entities.ViewModels;
using VoC.Alerts.Manager.Commands;

namespace VoC.Alerts.Web.Api.Controllers
{
    [Route("api/[controller]")]
    public class AlertsRecipientsController :BaseController

    {
        private readonly IMediator _mediator;

        public AlertsRecipientsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("{alertId:guid}")]
        public async Task<ActionResult<IEnumerable<AlertRecipientsViewModel>>> AddRecipients([FromRoute] Guid alertId, [FromBody] AddAlertRecipientsCommand command)
        {
            command.AlertId = alertId;
            var result = await _mediator.Send(command).ConfigureAwait(false);
            return new JsonResult(result);
        }

        [HttpPut]
        public async Task<ActionResult<AlertRecipientsViewModel>> UpdateRecipientDetails([FromBody] UpdateAlertRecipientsCommand command)
        {
            var result = await _mediator.Send(command).ConfigureAwait(false);
            if (result == null)
            {
                return new JsonResult($"Error updating alert recipients with Id {command.Id}")
                {
                    StatusCode = 400,
                };
            }
            return new JsonResult(result);
        }
    }
}
