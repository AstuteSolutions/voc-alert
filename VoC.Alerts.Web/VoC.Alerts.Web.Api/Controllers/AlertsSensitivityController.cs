﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using VoC.Alerts.Manager.Commands;
using Voc.Alerts.Sensitivity;
using Voc.Alerts.Sensitivity.ElasticSearchRepository;
using Voc.Alerts.Sensitivity.Filters;
using Voc.Alerts.Sensitivity.Logic;

namespace VoC.Alerts.Web.Api.Controllers
{
    [Route("api/[controller]")]
    public class AlertsSensitivityController : BaseController
    {
        private readonly IMediator _mediator;

        public AlertsSensitivityController(IMediator mediator)
        {
            _mediator = mediator;
        }
        [HttpPost]
        public async Task<ActionResult<AlertSensitivityResponse>> AlertSensitivityChart([FromBody] AlertSensitivityRequestCommand command)
        {
            var result = await _mediator.Send(command).ConfigureAwait(false);
            return new JsonResult(result);
        }
    }
}
