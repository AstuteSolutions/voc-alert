﻿using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace VoC.Alerts.Web.Api.Controllers
{
    
        [ApiController]
        public class StatusController : ControllerBase
        {
            /// <summary>
            /// Basic Healthcheck.
            /// </summary>
            /// <returns>A response code indicating success or failure</returns>
            [HttpGet("ping")]
            [SwaggerOperation(OperationId = "HealthCheck")]
            public ActionResult Get()
            {
                return Ok(new { status = "OK" });
            }
        }
    
}
