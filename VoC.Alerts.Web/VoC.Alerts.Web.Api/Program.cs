using System.IO;
using Iper.Common.Core.Logging;
using Iper.Common.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using Serilog;

namespace VoC.Alerts.Web.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureLogging((hostContext, loggingBuilder) =>
                {
                    loggingBuilder.Services.AddSingleton(
                        new ElasticSearchLogger(hostContext.Configuration.GetSection("ElasticSearchLoggerConnectionInfo")
                            .Get<RabbitMqConnectionInfo>()));
                    loggingBuilder.AddElasticSearchIperLoggerProvider(hostContext.Configuration["Environment"], Applications.AlertingApi, LogLevel.Information);
                    loggingBuilder.AddNLog();
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
