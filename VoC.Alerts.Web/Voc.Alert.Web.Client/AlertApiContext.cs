﻿using Iper.Common.WebUtilities.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Voc.Alert.Web.Client
{
    public class AlertApiContext : IIdentityServerContext
    {
        public string ApiGatewayUrl { get;set; }

        public string IdentityServerUrl { get;set; }

        public string IdentityClientId { get;set; }

        public string IdentityClientSecret { get;set; }

        public string Scope { get;set; }

        public string AesKey { get;set; }

        public string ApiGatewayPrefix { get;set; }

        public bool DisableApiGateway { get;set; }

        public bool DisableIdentityServer { get;set; }

        public string GetRootUrl { get;set; }

        public IDictionary<string, string> HeaderSettings { get;set; }= new Dictionary<string, string>();
    }
}
