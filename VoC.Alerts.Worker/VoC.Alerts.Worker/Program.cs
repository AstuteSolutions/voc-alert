using System.IO;
using Autofac.Extensions.DependencyInjection;
using Iper.Common.Core.Logging;
using Iper.Common.EntityFramework;
using Iper.Common.Logging;
using Iperloop.Core.Events.Extensions;
using Iperloop.Core.Events.Models.Options;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using Serilog;
using VoC.Alerts.Abstraction.Events;
using VoC.Alerts.Abstraction.Sources;
using VoC.Alerts.Context;
using VoC.Alerts.Data;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Domain;
using VoC.Alerts.Logic;
using VoC.Alerts.Logic.Events;
using VoC.Alerts.Logic.Handlers;
using VoC.Alerts.Logic.Sources;
using VoC.Alerts.Logic.Triggers;
using Voc.Alerts.TriggerEventResolvers;
using Voc.Alerts.TriggerEventResolvers.TriggerEventCreators;
using Voc.Alerts.TriggerEventResolvers.TriggerEventCreators.CustomTriggerEventResolvers;
using VoC.Alerts.Triggers.Worker.Controllers;
using System;
using VoC.Alerts.Triggers.Worker.Extensions;

namespace VoC.Alerts.Triggers.Worker
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseWindowsService()
            .ConfigureAppConfiguration((hostingContext, config) =>
            {
                var settingsLocation = AppDomain.CurrentDomain.BaseDirectory ?? string.Empty;
                var environmentLongName = config.BuildAppSettingsConfigurations().GetEnvironmentLongName();
                config.AddJsonFile(
                        Path.Combine(settingsLocation, "appSettings.json"),
                        optional: false, reloadOnChange: true)
                    .AddJsonFile(
                        Path.Combine(settingsLocation,
                            $"appSettings.{environmentLongName}.json"),
                        optional: true, reloadOnChange: true);

            }) .ConfigureLogging((hostContext, loggingBuilder) =>
                {
                    loggingBuilder.Services.AddSingleton(
                        new ElasticSearchLogger(hostContext.Configuration.GetSection("ElasticSearchLoggerConnectionInfo")
                            .Get<RabbitMqConnectionInfo>()));
                    loggingBuilder.AddElasticSearchIperLoggerProvider(hostContext.Configuration["Environment"], Applications.AlertingService, LogLevel.Information);
                    loggingBuilder.AddNLog();
                })
                .ConfigureServices((hostContext, services) =>
                {
                    var brokerConfigurations = hostContext.Configuration.GetSection("BrokerConnection").Get<BrokerConnectionOptions>();
                    services.AddDbContext<AlertsContext>(options =>
                    {
                        options.UseSqlServer(hostContext.Configuration.GetConnectionString("wValidator"));
                        if (hostContext.Configuration.GetSection("Environment")?.Value.Equals("DEV") == true)
                        {
                            options.EnableSensitiveDataLogging(true);
                            options.EnableDetailedErrors(true);
                        }
                    });

                    services.AddDbContext<WebValidatorContext>(options =>
                    {
                        options.UseSqlServer(hostContext.Configuration.GetConnectionString("wValidator"));
                        if (hostContext.Configuration.GetSection("Environment")?.Value.Equals("DEV") == true)
                        {
                            options.EnableSensitiveDataLogging(true);
                            options.EnableDetailedErrors(true);
                        }
                    });

                    _ = services.AddTransient<ISourceHandler<AlertDefinition>, SurveySourceHandler>()
                        .AddMediatR(typeof(SingleSelectQuestionTriggerHandler),
                            typeof(OpenTextQuestionTriggerHandler),
                            typeof(SingleSelectScoreQuestionTriggerHandler))
                        .AddTransient<IVocResponseServices, VocResponseServices>()
                    .AddTransient<IEventsStore<TriggerEvent>, EventsStore>()
                    .AddTransient<IEventsRepository, EventsRepository>()
                    .AddTransient<IAlertsRepository, AlertsRepository>()
                        .AddTransient<ITriggerCreator, TriggerCreator>()
                        .AddTransient<ITriggerEventResolver, OpenTextTriggerEventResolver>()
                        .AddTransient<ITriggerEventResolver, SingleSelectQuestionTriggerEventResolver>()
                        .AddTransient<ITriggerEventResolver, SingleSelectScoreQuestionTriggerEventResolver>()
                        .AddTransient<ITriggerEventResolver, SurveyVolumeTriggerEventResolver>()
                        .AddTransient<ISettingsRepository, SettingsRepository>()
                        .AddEventDispatcher(dispatcherOptions =>
                        {
                            dispatcherOptions.ExchangeName = hostContext.Configuration["EventDispatcher:ExchangeName"];
                            dispatcherOptions.ExchangeType = hostContext.Configuration["EventDispatcher:ExchangeType"];
                        }, options =>
                        {
                            options.AppId = brokerConfigurations.AppId;
                            options.HostName = brokerConfigurations.HostName;
                            options.Username = brokerConfigurations.Username;
                            options.Password = brokerConfigurations.Password;
                            options.VirtualHost = brokerConfigurations.VirtualHost;
                        });
                    services.AddHostedService<EvaluationWorkersController>();
                })
                .UseServiceProviderFactory(new AutofacServiceProviderFactory());
    }
}
