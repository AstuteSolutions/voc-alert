﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoC.Alerts.Triggers.Worker.Extensions
{
    public static class ConfigurationExtensions
    {
        public static string GetCurrentEnvironment(this IConfiguration configuration)
        {
            return configuration.GetValue<string>("Environment");
        }

        public static string GetEnvironmentLongName(this IConfigurationRoot configurationRoot)
        {
            switch (configurationRoot.GetCurrentEnvironment().ToLower())
            {
                case "dev":
                case "development":
                    return "Development";
                case "preprod":
                case "pp":
                    return "PreProd";
                case "prod":
                case "production":
                    return "Production";
                case "local":
                    return "Local";
                default:
                    return string.Empty;
            }
        }

        public static IConfigurationRoot BuildAppSettingsConfigurations(this IConfigurationBuilder builder)
        {
            var settingsLocation = AppDomain.CurrentDomain.BaseDirectory ?? string.Empty;
            return builder.AddJsonFile(Path.Combine(settingsLocation, "appSettings.json")).Build();
        }
    }
}
