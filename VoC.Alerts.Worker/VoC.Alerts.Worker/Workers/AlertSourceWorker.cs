﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Iperloop.Core.Workers.Abstraction;
using Microsoft.Extensions.Logging;
using VoC.Alerts.Abstraction.Sources;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Domain;

namespace VoC.Alerts.Triggers.Worker.Workers
{
    public class AlertSourceWorker : BackgroundService
    {
        private readonly IAlertsRepository _alertsRepository;
        private readonly ISourceHandler<AlertDefinition> _sourceHandler;
        private readonly int _companyId;

        public AlertSourceWorker(ILogger<AlertSourceWorker> logger, int companyId,
            ISourceHandler<AlertDefinition> sourceHandler, IAlertsRepository alertsRepository) : base(logger)
        {
            _companyId = companyId;
            _sourceHandler = sourceHandler;
            _alertsRepository = alertsRepository;
            Name = $"VoC.Alerts.Worker for {companyId}";
        }

        protected override async Task ProcessAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                foreach (var companyAlert in _alertsRepository.GetAlertDefinitions(_companyId))
                {
                    Logger.LogInformation($"Checking incoming responses for alert {companyAlert.Definition.Name} of source type {companyAlert.Definition.Source.Type}");
                    try
                    {
                        await _sourceHandler.Execute(companyAlert.Definition).ConfigureAwait(false);
                        await _alertsRepository.SetDefinitionLastRunAsync(companyAlert.Definition.Id, stoppingToken).ConfigureAwait(false);
                        Logger.LogInformation("Executed triggers.");
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex.Message, ex);
                    }
                }
                await Task.Delay(TimeSpan.FromMinutes(5), stoppingToken).ConfigureAwait(true);
            }
        }

        public override string Name { get; }
    }
}
