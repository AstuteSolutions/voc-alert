﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Iperloop.Core.Workers.Abstraction;
using Iperloop.Core.Workers.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Triggers.Worker.Workers;

namespace VoC.Alerts.Triggers.Worker.Controllers
{
    public class EvaluationWorkersController : BackgroundServiceController<AlertSourceWorker>
    {
        public EvaluationWorkersController(IServiceProvider serviceProvider, ILogger<AlertSourceWorker> logger) : base(serviceProvider, logger)
        {
        }

        protected override async Task<IEnumerable<ServiceClient>> GetServiceClients(CancellationToken stoppingToken)
        {
            var repository = ServiceProvider.GetRequiredService<ISettingsRepository>();
            var activeCompanies = await repository.GetEnabledCompaniesForService("alerts").ConfigureAwait(true);
            return activeCompanies.Select(x =>
                new ServiceClient(Guid.NewGuid(), x.CompanyId, $"company {x.CompanyId}"));
        }

        protected override Action<ContainerBuilder> BuildServiceClientContainer(ServiceClient serviceClient)
        {
            return builder => builder.RegisterType<AlertSourceWorker>().AsSelf().WithParameter("companyId", serviceClient.CompanyId);
        }
    }
}
