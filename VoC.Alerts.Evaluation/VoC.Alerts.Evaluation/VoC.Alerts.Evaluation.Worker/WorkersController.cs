﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Iperloop.Core.Workers.Abstraction;
using Iperloop.Core.Workers.Models;
using Microsoft.Extensions.Logging;

namespace VoC.Alerts.Evaluation.Worker
{
    public class WorkersController : BackgroundServiceController<EvaluationWorker>
    {
        public WorkersController(IServiceProvider serviceProvider, ILogger<EvaluationWorker> logger) : base(serviceProvider, logger)
        {
        }

        protected override Task<IEnumerable<ServiceClient>> GetServiceClients(CancellationToken stoppingToken)
        {
            return Task.FromResult(new List<ServiceClient>
            {
                new (Guid.NewGuid(), 0, "Global Service Client")
            }.AsEnumerable());
        }

        protected override Action<ContainerBuilder> BuildServiceClientContainer(ServiceClient serviceClient)
        {
            return builder => builder.RegisterType<EvaluationWorker>().AsSelf();
        }
    }
}
