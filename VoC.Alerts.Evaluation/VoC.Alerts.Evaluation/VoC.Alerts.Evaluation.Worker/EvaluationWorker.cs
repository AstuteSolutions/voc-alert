using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using Iperloop.Core.Events.Abstraction;
using Microsoft.Extensions.Configuration;
using VoC.Alerts.Domain;

namespace VoC.Alerts.Evaluation.Worker
{
    public class EvaluationWorker : Iperloop.Core.Workers.Abstraction.BackgroundService
    {
        private readonly ILogger<EvaluationWorker> _logger;
        private readonly IEventConsumer _eventConsumer;
        private readonly IConsumerEventHandler<EventMessage> _eventsHandler;
        private readonly string _queueName;
        private readonly string _exchangeName;
        private readonly string _routingKey;

        public EvaluationWorker(ILogger<EvaluationWorker> logger, IConsumerEventHandler<EventMessage> eventsHandler,
            IEventConsumer eventConsumer,
            IConfiguration configuration) :base(logger)
        {
            _logger = logger;
            _eventsHandler = eventsHandler;
            _eventConsumer = eventConsumer;
            Name = "VoC.Alerts.Evaluation EvaluationWorker.";
            _queueName = configuration["BrokerConnection:Queue"];
            _routingKey = configuration["BrokerConnection:RoutingKey"];
            _exchangeName = configuration["EventDispatcher:ExchangeName"];
        }

        protected override Task ProcessAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("EvaluationWorker running at: {time}", DateTimeOffset.Now);
            _eventConsumer.Subscribe(_eventsHandler, _queueName, _exchangeName, _routingKey, stoppingToken,false);
            return Task.CompletedTask;
        }

        public override string Name { get; }
    }
}
