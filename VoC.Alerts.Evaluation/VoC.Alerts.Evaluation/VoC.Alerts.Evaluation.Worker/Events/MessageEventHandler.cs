﻿using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Iperloop.Core.Events.Abstraction;
using Iperloop.Core.Events.Enums;
using Microsoft.Extensions.Logging;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Domain;
using VoC.Alerts.Logic.Events;

namespace VoC.Alerts.Evaluation.Worker.Events
{
    public class MessageEventHandler : IConsumerEventHandler<EventMessage>
    {
        private readonly ILogger<MessageEventHandler> _logger;
        private readonly IEventProcessor _eventProcessor;
        private readonly IEventsRepository _eventsRepository;

        public MessageEventHandler(ILogger<MessageEventHandler> logger, IEventProcessor eventProcessor, IEventsRepository eventsRepository)
        {
            _logger = logger;
            _eventProcessor = eventProcessor;
            _eventsRepository = eventsRepository;
        }

        public async Task HandleAsync(EventMessage @event, CancellationToken cancellationToken)
        {
            try
            {
                _logger.LogInformation($"Consuming event message: '{@event.CompanyId}-{@event.AlertId}'");
                var eventRecord = await _eventsRepository.GetAwaitingEventById(@event.CorrelationId).ConfigureAwait(false);
                // Event not found, or incorrect IDs, illegal msg.
                if (eventRecord == null)
                {
                    EventState = Iperloop.Core.Events.Enums.EventState.Rejected;
                    return;
                }

                await _eventProcessor.ProcessAsync(eventRecord, cancellationToken).ConfigureAwait(false);
                EventState = Iperloop.Core.Events.Enums.EventState.Accepted;
            }
            catch (TaskCanceledException)
            {
                EventState = Iperloop.Core.Events.Enums.EventState.Requeue;
                _logger.LogInformation($"Cancellation token has been requested on {MethodBase.GetCurrentMethod().Name}");
            }
            catch (Exception ex)
            {
                EventState = Iperloop.Core.Events.Enums.EventState.Rejected;
                _logger.LogError(ex, "Error on Processing the Evaluation for AlertEvent Id:{eventId}", @event?.TriggerId);
            }

        }

        public EventState? EventState { get; private set; }
    }
}
