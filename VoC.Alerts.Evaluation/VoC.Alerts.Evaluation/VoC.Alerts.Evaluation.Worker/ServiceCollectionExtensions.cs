﻿using System;
using Iperloop.Core.Events.Extensions;
using Iperloop.Core.Events.Models.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace VoC.Alerts.Evaluation.Worker
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddEventsConsumer(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddEventConsumer(BrokerConnectionBuilder(configuration), consumerOptions =>
            {
                consumerOptions.AutoAcknowledge = false;
            });
        }

        private static Action<BrokerConnectionOptions> BrokerConnectionBuilder(IConfiguration configuration)
        {
            var brokerConnectionSettings = configuration.GetSection("BrokerConnection")
                .Get<BrokerConnectionOptions>();

            return builder =>
            {
                builder.HostName = brokerConnectionSettings.HostName;
                builder.Username = brokerConnectionSettings.Username;
                builder.Password = brokerConnectionSettings.Password;
                builder.VirtualHost = brokerConnectionSettings.VirtualHost;
                builder.AppId = brokerConnectionSettings.AppId;
            };
        }
    }
}
