using System.IO;
using Autofac.Extensions.DependencyInjection;
using Iper.Common.Core.Logging;
using Iper.Common.Logging;
using Iperloop.Core.Events;
using Iperloop.Core.Events.Abstraction;
using Iperloop.Core.Events.Extensions;
using Iperloop.Core.Events.Infrastructure;
using Iperloop.Core.Events.Models.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using Serilog;
using VoC.Alerts.Abstraction.Events;
using VoC.Alerts.Context;
using VoC.Alerts.Data;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Domain;
using VoC.Alerts.Evaluation.Worker.Events;
using VoC.Alerts.Logic.Communications;
using VoC.Alerts.Logic.Events;

namespace VoC.Alerts.Evaluation.Worker
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseWindowsService()
                .ConfigureLogging((hostContext, loggingBuilder) =>
                {
                    loggingBuilder.Services.AddSingleton(
                        new ElasticSearchLogger(hostContext.Configuration.GetSection("ElasticSearchLoggerConnectionInfo")
                            .Get<RabbitMqConnectionInfo>()));
                    loggingBuilder.AddElasticSearchIperLoggerProvider(hostContext.Configuration["Environment"], Applications.AlertingEvalService, LogLevel.Information);
                    loggingBuilder.AddNLog();
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddDbContext<AlertsContext>(options =>
                    {
                        options.UseSqlServer(hostContext.Configuration.GetConnectionString("wValidator"));
                        if (hostContext.Configuration.GetSection("Environment")?.Value.Equals("DEV") == true)
                        {
                            options.EnableSensitiveDataLogging(true);
                            options.EnableDetailedErrors(true);
                        }
                    });
                    services.AddSingleton<IBrokerConnectionFactory>(_ =>
                    {
                        var options = hostContext.Configuration.GetSection("BrokerConnection")
                            .Get<BrokerConnectionOptions>();
                        return new BrokerConnectionFactory
                        {
                            Hostname = options.HostName,
                            Username = options.Username,
                            Password = options.Password,
                            VirtualHost = options.VirtualHost,
                            AppId = options.AppId
                        };
                    });

                    var brokerConfigurations = hostContext.Configuration.GetSection("BrokerConnection").Get<BrokerConnectionOptions>();
                    services.AddEventDispatcher(dispatcherOptions =>
                    {
                        dispatcherOptions.ExchangeName = hostContext.Configuration["EventDispatcher:ExchangeName"];
                        dispatcherOptions.ExchangeType = hostContext.Configuration["EventDispatcher:ExchangeType"];
                    }, options =>
                    {
                        options.AppId = brokerConfigurations.AppId;
                        options.HostName = brokerConfigurations.HostName;
                        options.Username = brokerConfigurations.Username;
                        options.Password = brokerConfigurations.Password;
                        options.VirtualHost = brokerConfigurations.VirtualHost;
                    });
                        services.AddScoped<IEventConsumer, EventConsumer>();
                        services.AddTransient<IConsumerEventHandler<EventMessage>, MessageEventHandler>()
                            .AddTransient<IEventsStore<TriggerEvent>, EventsStore>()
                            .AddTransient<IEventProcessor, Logic.Events.EventProcessor>()
                            .AddTransient<IEventCommunicationHandler, EventCommunicationHandler>()
                            .AddTransient<IEventsRepository, EventsRepository>()
                            .AddTransient<IAlertsRepository, AlertsRepository>();
                    services.AddHostedService<WorkersController>();
                }).UseServiceProviderFactory(new AutofacServiceProviderFactory());
    }
}
