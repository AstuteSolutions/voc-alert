﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Manager.Commands;

namespace VoC.Alerts.Manager.Handlers
{
    public class SetTriggersThresholdHandler : IRequestHandler<SetTriggersThresholdCommand, bool>
    {
        private readonly IAlertsRepository _alertsRepository;
        private readonly ILogger<SetTriggersThresholdHandler> _logger;

        public SetTriggersThresholdHandler(IAlertsRepository alertsRepository, ILogger<SetTriggersThresholdHandler> logger)
        {
            _alertsRepository = alertsRepository;
            _logger = logger;
        }

        public async Task<bool> Handle(SetTriggersThresholdCommand request, CancellationToken cancellationToken)
        {
            try
            {
                if (await _alertsRepository.SetThresholdAsync(request.AlertDefinitionId, request.Threshold)
                        .ConfigureAwait(false) == request.Threshold)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }

            return false;
        }
    }
}
