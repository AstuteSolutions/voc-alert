﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Manager.Commands;

namespace VoC.Alerts.Manager.Handlers
{
    public class DeleteAlertHandler : BaseHandler<BaseCommand<bool>, bool>
    {
        private readonly IAlertsRepository _alertsRepository;
        public DeleteAlertHandler(ILogger<DeleteAlertHandler> logger, IAlertsRepository alertsRepository) : base(logger)
        {
            _alertsRepository = alertsRepository;
        }

        protected override async Task<bool> HandleAsync(BaseCommand<bool> request, CancellationToken cancellationToken)
        {
            try
            {
                return await _alertsRepository.DeleteAlert(request.AlertId,cancellationToken).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, ex.Message);
            }
            return default;
        }
    }
}
