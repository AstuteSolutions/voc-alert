﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Manager.Commands;

namespace VoC.Alerts.Manager.Handlers
{
    public class DefineAlertHandler : BaseHandler<DefineAlertCommand, Guid>
    {
        private readonly IAlertsRepository _alertsRepository;

        public DefineAlertHandler(ILogger<DefineAlertHandler> logger, IAlertsRepository alertsRepository) : base(logger)
        {
            _alertsRepository = alertsRepository;
        }

        protected override async Task<Guid> HandleAsync(DefineAlertCommand request, CancellationToken cancellationToken)
        {
            try
            {
                return await _alertsRepository.CreateSurveyAlert(request.CompanyId, request.SurveyId, request.Title,request.MessageContent,
                    cancellationToken).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, ex.Message);
            }
            return default;
        }
    }
}
