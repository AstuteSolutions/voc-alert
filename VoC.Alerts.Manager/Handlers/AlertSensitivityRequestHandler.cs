﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using VoC.Alerts.Domain;
using VoC.Alerts.Manager.Commands;
using Voc.Alerts.Sensitivity.Filters;
using Voc.Alerts.Sensitivity.Logic;
using Voc.Alerts.Sensitivity.Models;
using Voc.Alerts.TriggerEventResolvers;
using Voc.Alerts.TriggerEventResolvers.TriggerDefinitions;

namespace VoC.Alerts.Manager.Handlers
{
    public class AlertSensitivityRequestHandler : BaseHandler<AlertSensitivityRequestCommand, AlertSensitivityResponse>
    {
        private readonly IAlertSensitivityManager _alertSensitivityManager;
        private readonly ITriggerCreator _triggerCreator;
        public AlertSensitivityRequestHandler(ILogger<BaseHandler<AlertSensitivityRequestCommand, AlertSensitivityResponse>> logger, IAlertSensitivityManager alertSensitivityManager, ITriggerCreator triggerCreator) : base(logger)
        {
            _alertSensitivityManager = alertSensitivityManager;
            _triggerCreator = triggerCreator;
        }

        protected override async Task<AlertSensitivityResponse> HandleAsync(AlertSensitivityRequestCommand request, CancellationToken cancellationToken)
        {
            var triggers = new List<object>();
            foreach (var trigger in request.Triggers)
            {
                var domainRequest = new Domain.TriggerData
                {
                    LookupId = trigger.Value,
                    Type = trigger.Type,
                    Answers = trigger.Lookups.Select(x => new TriggerStringLookup(Guid.NewGuid(), 1, x.Value))
                        .ToArray()
                };
                var triggerType = TriggerTypes.TriggerTypesDictionary.TryGetValue(domainRequest.Type, out var type) ? type : throw new NotSupportedException($"Trigger Type {domainRequest.Type} is not supported");
                triggers.Add(_triggerCreator.CreateTrigger(domainRequest, request.CompanyId, request.SurveyId, triggerType));
            }

            return await _alertSensitivityManager.GetAlertSensitivityChart(new AlertSensitivityRequest()
            {
                CompanyId = request.CompanyId,
                TimeInterval = GetTimeIntervalInHours(request.TimeIntervalInMinutes),
                ComparisionDays = request.DataToCompareInDays,
                SurveyId = request.SurveyId,
                Triggers = triggers
            });
        }

        private static int GetTimeIntervalInHours(int? timeIntervalInMinutes)
        {
            if (timeIntervalInMinutes.HasValue)
                return (int)(timeIntervalInMinutes / 60);
            return 6;
        }
    }
}
