﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Voc.Alerts.TriggerEventResolvers;
using Voc.Alerts.TriggerEventResolvers.TriggerDefinitions;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Domain;
using VoC.Alerts.Entities.ViewModels;
using VoC.Alerts.Manager.Commands;
using VoC.Alerts.Logic.Triggers;
using MediatR;
using System.Linq;
using VoC.Alerts.Entities.Enums;
using Newtonsoft.Json;

namespace VoC.Alerts.Manager.Handlers
{
    public class AlertEvaluateCommandHandler : BaseHandler<AlertEvaluateCommand, AlertEvalutionResponse>
    {
        private readonly IAlertsRepository _alertsRepository;
        private readonly Dictionary<string, Type> _triggerTypesDictionary;
        private readonly ITriggerCreator _triggerCreator;
        private readonly IMediator _mediator;
        public AlertEvaluateCommandHandler(ILogger<BaseHandler<AlertEvaluateCommand, AlertEvalutionResponse>> logger, IAlertsRepository alertsRepository, ITriggerCreator triggerCreator, IMediator mediator) : base(logger)
        {
            _alertsRepository = alertsRepository;
            _triggerTypesDictionary = TriggerTypes.TriggerTypesDictionary;
            _triggerCreator = triggerCreator;
            _mediator = mediator;
        }
        protected override async Task<AlertEvalutionResponse> HandleAsync(AlertEvaluateCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException($"{nameof(request)} is null");
            var getAlert = await _alertsRepository.GetAlertDefinitionById(request.AlertId);
            if (getAlert == null)
                throw new Exception($"Alert with definition Id: {request.AlertId} doesn't exist");
            try
            {
                var alertDefinition = getAlert.Definition;
                var alertTriggers = getAlert.Definition.Triggers;
                var events = new List<TriggerEvent>();
                foreach (var triggerData in alertTriggers)
                {
                    var trigger = CreateTriggerObject(triggerData, getAlert.Definition, request.Respondents);
                    if (trigger == null)
                    {
                        continue;
                    }
                    var result = (TriggerEvents)await _mediator.Send(trigger, CancellationToken.None).ConfigureAwait(false);
                    if (result == null)
                    {
                        continue;
                    }
                    events.AddRange(result.Events);
                }
                return EvaluateTheResponses(events, alertTriggers.Select(x => x.Id), alertDefinition);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, $"Error Evaluating the Response for the request :{request}", JsonConvert.SerializeObject(request));
            }
            return default;
        }

        private AlertEvalutionResponse EvaluateTheResponses(List<TriggerEvent> triggerEvents, IEnumerable<int> triggerIds, AlertDefinition alertDefinition)
        {
            var evalutedRespondents = new List<EvaluationByRespondent>();
            var groupByRespondentId = triggerEvents.GroupBy(x => x.TargetMatchId);
            
            foreach (var respondentTriggers in groupByRespondentId)
            {
                //Get all outcome matched triggers and groupByTriggerId
                var matchedTriggers = respondentTriggers.Where(x => x.Outcome).GroupBy(x => x.TriggerId);

                //If matchedTrigger count equals to triggers condition, all the triggers are satisfied for the response in case of  logical "AND"
                if (alertDefinition.AlertTriggerSets.LogicalOperation == (int)LogicalOperator.And)
                {
                    if (matchedTriggers.Count() == triggerIds.Count())

                        evalutedRespondents.Add(new EvaluationByRespondent()
                        {
                            EvalutionResult = true,
                            RespondentId = long.Parse(respondentTriggers.Key)
                        });
                    else
                        evalutedRespondents.Add(new EvaluationByRespondent()
                        {
                            EvalutionResult = false,
                            RespondentId = long.Parse(respondentTriggers.Key)
                        });
                }
                //Logical "OR"
                else
                {
                    if (matchedTriggers.Any())

                        evalutedRespondents.Add(new EvaluationByRespondent()
                        {
                            EvalutionResult = true,
                            RespondentId = long.Parse(respondentTriggers.Key)
                        });
                    else
                        evalutedRespondents.Add(new EvaluationByRespondent()
                        {
                            EvalutionResult = false,
                            RespondentId = long.Parse(respondentTriggers.Key)
                        });
                }


            }
            return new AlertEvalutionResponse()
            {
                EvaluationByRespondents = evalutedRespondents,
                AlertDefintionId = alertDefinition.Id
            };
        }

        private object CreateTriggerObject(Domain.TriggerData triggerData, AlertDefinition alertDefinition, List<long> respondents)
        {
            if (_triggerTypesDictionary.TryGetValue(triggerData.Type, out var triggerType))
            {
                return CreateTrigger(triggerData, alertDefinition.Company.Id, ((SurveySource)alertDefinition.Source).SurveyId, triggerType, respondents);
            }

            return default;
        }

        private object CreateTrigger(Domain.TriggerData triggerData, int companyId, int surveyId, Type triggerType, List<long> respondents)
        {
            return _triggerCreator.CreateTrigger(triggerData, companyId, surveyId, respondents, triggerType);
        }
    }
}
