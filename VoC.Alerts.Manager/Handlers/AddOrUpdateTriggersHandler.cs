﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using VoC.Alerts.Context.Entities;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Entities.Enums;
using VoC.Alerts.Manager.Commands;

namespace VoC.Alerts.Manager.Handlers
{
    public class AddOrUpdateTriggersHandler : BaseHandler<AddOrUpdateTriggersCommand, List<TriggerData>>
    {
        private readonly IAlertsRepository _alertsRepository;

        public AddOrUpdateTriggersHandler(ILogger<AddOrUpdateTriggersHandler> logger, IAlertsRepository alertsRepository) : base(logger)
        {
            _alertsRepository = alertsRepository;
        }

        protected override async Task<List<TriggerData>> HandleAsync(AddOrUpdateTriggersCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var result = await _alertsRepository.AddTriggers(request.AlertId, new AlertTriggerSet()
                {
                    TimeIntervalInMinutes = request.TriggerSet?.TimeIntervalInMinutes,
                    Id = request.TriggerSet?.Id ?? Guid.NewGuid(),
                    IsMultipleNotificationTrigger = request.TriggerSet?.IsMultipleNotificationTrigger,
                    LogicalOperation = request.TriggerSet != null && request.TriggerSet.LogicalOperator.Equals("and", StringComparison.CurrentCultureIgnoreCase) ? (int)LogicalOperator.And : (int)LogicalOperator.Or,
                    NotificationFrequencyInHours = request.TriggerSet?.NotificationFrequencyInHours,
                    SmartDetectionPercentage = request.TriggerSet?.SmartDetectionPercentage,
                    Threshold = request.TriggerSet?.Threshold,
                    TriggersBelowThreshold = request.TriggerSet?.TriggersBelowThreshold
                },
                    request.Triggers.Select(t => new AlertTrigger
                    {
                        Id = t.Id,
                        Type = t.Type,
                        Value = t.Value,
                        Lookups = t.Lookups.Select(l => new TriggerLookup
                        {
                            Value = l.Value,
                            Id = l.Id
                        }).ToArray()
                    }).ToList(), cancellationToken).ConfigureAwait(false);
                return result.Select(x => new TriggerData
                {
                    Type = x.Type,
                    Value = x.Value,
                    Lookups = x.Lookups.Select(l => new LookUps()
                    {
                        Value = l.Value,
                        Id = l.Id
                    }).ToArray()
                }).ToList();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, ex.Message);
            }
            return default;
        }
    }
}
