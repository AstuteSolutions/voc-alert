﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VoC.Alerts.Context.Entities;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Entities.ViewModels;
using VoC.Alerts.Manager.Commands;
using VoC.Alerts.Entities.Enums;

namespace VoC.Alerts.Manager.Handlers
{
    public class UpdateAlertHandler : BaseHandler<UpdateAlertCommand, bool>
    {
        private readonly IAlertsRepository _alertsRepository;

        public UpdateAlertHandler(ILogger<UpdateAlertHandler> logger, IAlertsRepository alertsRepository) : base(logger)
        {
            _alertsRepository = alertsRepository;
        }
        protected override async Task<bool> HandleAsync(UpdateAlertCommand request, CancellationToken cancellationToken)
        {
            try
            {
                await _alertsRepository.UpdateAlertName(request.Id, request.Name, cancellationToken);
                await _alertsRepository.UpdateRecipients(request.Id, request.Recipients.Select(x => x.Email).ToArray(), cancellationToken).ConfigureAwait(false);
                var triggerSet = new AlertTriggerSet()
                {
                    Id = request.Id,
                    IsMultipleNotificationTrigger = request.TriggersSet?.IsMultipleNotificationTrigger,
                    LogicalOperation = request.TriggersSet != null && request.TriggersSet.LogicalOperator.Equals("and", StringComparison.CurrentCultureIgnoreCase) ? (int)LogicalOperator.And : (int)LogicalOperator.Or,
                    NotificationFrequencyInHours = request.TriggersSet?.NotificationFrequencyInHours,
                    SmartDetectionPercentage = request.TriggersSet?.SmartDetectionPercentage,
                    Threshold = request.TriggersSet?.Threshold,
                    TimeIntervalInMinutes = request.TriggersSet?.TimeIntervalInMinutes,
                    TriggersBelowThreshold = request.TriggersSet?.TriggersBelowThreshold
                };
                await _alertsRepository.UpdateTriggers(request.Id, triggerSet, request.Triggers.Select(t => new AlertTrigger
                {
                    Id = t.Id,
                    Type = t.Type,
                    Value = t.Value,
                    Lookups = t.Lookups.Select(l => new TriggerLookup
                    {
                        Value = l.Value,
                        Id = l.Id
                    }).ToArray()
                }).ToList(), cancellationToken).ConfigureAwait(false);
                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, ex.Message);
            }
            return false;
        }
    }
}
