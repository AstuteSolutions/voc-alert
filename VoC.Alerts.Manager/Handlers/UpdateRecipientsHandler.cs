﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Entities.Models;
using VoC.Alerts.Entities.ViewModels;
using VoC.Alerts.Manager.Commands;

namespace VoC.Alerts.Manager.Handlers
{
    public class UpdateRecipientsHandler : BaseHandler<UpdateAlertRecipientsCommand, AlertRecipientsViewModel>
    {
        private readonly IAlertsRecipientRepository _alertsRecipientRepository;
        public UpdateRecipientsHandler(ILogger<UpdateRecipientsHandler> logger, IAlertsRecipientRepository alertsRecipientRepository) : base(logger)
        {
            _alertsRecipientRepository = alertsRecipientRepository;
        }

        protected override async Task<AlertRecipientsViewModel> HandleAsync(UpdateAlertRecipientsCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var updatedRecipient = await _alertsRecipientRepository.UpdateAlertCommunicationRecipient(new AlertRecipient()
                {
                    Email = request.Email,
                    Enabled = request.Enabled,
                    Id = request.Id
                });

                return new AlertRecipientsViewModel()
                {
                    Email = updatedRecipient.Address,
                    Id = updatedRecipient.Id,
                    Enabled = updatedRecipient.Enabled
                };
            }
            catch (Exception e)
            {
                Logger.LogError(e, $"error updating alert recipient with Id {request.Id}");
            }
            return default;
        }
    }
}
