﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Manager.Commands;

namespace VoC.Alerts.Manager.Handlers
{
    public class DefineAlertCommunicationHandler : BaseHandler<DefineAlertCommunicationCommand, Guid>
    {
        private readonly IAlertsRepository _alertsRepository;

        public DefineAlertCommunicationHandler(ILogger<DefineAlertCommunicationHandler> logger, IAlertsRepository alertsRepository) : base(logger)
        {
            _alertsRepository = alertsRepository;
        }

        protected override async Task<Guid> HandleAsync(DefineAlertCommunicationCommand request, CancellationToken cancellationToken)
        {
            try
            {
                return await _alertsRepository
                    .SetAlertCommunicationMessage(request.AlertId, request.MessageContent, cancellationToken)
                    .ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, ex.Message);
            }
            return default;
        }
    }
}
