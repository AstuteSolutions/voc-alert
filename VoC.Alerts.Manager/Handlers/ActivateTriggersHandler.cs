﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Manager.Commands;

namespace VoC.Alerts.Manager.Handlers
{
    public class ActivateTriggersHandler : BaseHandler<ActivateTriggersCommand, bool>
    {
        private readonly IAlertsRepository _alertsRepository;
        public ActivateTriggersHandler(ILogger<ActivateTriggersHandler> logger, IAlertsRepository alertsRepository) : base(logger)
        {
            _alertsRepository = alertsRepository;
        }

        protected override async Task<bool> HandleAsync(ActivateTriggersCommand request, CancellationToken cancellationToken)
        {
            try
            {
                return await _alertsRepository.ActivateTriggers(request.AlertId, request.Activate,cancellationToken).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, ex.Message);
            }
            return default;
        }
    }
}
