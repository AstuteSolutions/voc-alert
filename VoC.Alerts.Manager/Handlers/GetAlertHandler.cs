﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using VoC.Alerts.Constants;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Entities.ViewModels;
using VoC.Alerts.Manager.Commands;
using VoC.Alerts.Manager.Mappers;

namespace VoC.Alerts.Manager.Handlers
{
    public class GetAlertHandler : BaseHandler<BaseCommand<AlertDefinitionViewModel>, AlertDefinitionViewModel>
    {
        private readonly IAlertsRepository _alertsRepository;
        public GetAlertHandler(ILogger<GetAlertHandler> logger, IAlertsRepository alertsRepository) : base(logger)
        {
            _alertsRepository = alertsRepository;
        }

        protected override async Task<AlertDefinitionViewModel> HandleAsync(BaseCommand<AlertDefinitionViewModel> request, CancellationToken cancellationToken)
        {
            try
            {
                var alert = await _alertsRepository.GetAlertDefinition(request.AlertId);
                return AlertDefinitionMapper.Map(alert);
            }
            catch (Exception e)
            {
                Logger.LogError(e, $"Error getting alert definition with Id: {request.AlertId}");
            }

            return default;
        }
    }
}
