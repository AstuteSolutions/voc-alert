﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using VoC.Alerts.Constants;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Domain;
using VoC.Alerts.Entities.ViewModels;
using VoC.Alerts.Manager.Commands;
using VoC.Alerts.Manager.Mappers;

namespace VoC.Alerts.Manager.Handlers
{
    public class GetCompanyAlertsHandler : BaseHandler<BaseCommand<IEnumerable<AlertDefinitionViewModel>>, IEnumerable<AlertDefinitionViewModel>>
    {
        private readonly IAlertsRepository _alertsRepository;
        public GetCompanyAlertsHandler(ILogger<GetCompanyAlertsHandler> logger, IAlertsRepository alertsRepository) : base(logger)
        {
            _alertsRepository = alertsRepository;
        }

        protected override Task<IEnumerable<AlertDefinitionViewModel>> HandleAsync(BaseCommand<IEnumerable<AlertDefinitionViewModel>> request, CancellationToken cancellationToken)
        {
            try
            {
                var alerts = _alertsRepository.GetAlertDefinitionsByCompany(request.CompanyId);
                var alertDef = alerts.Select(alert => AlertDefinitionMapper.Map(alert)).ToList();
                return Task.Run(() => alertDef.AsEnumerable(), cancellationToken);

            }
            catch (Exception ex)
            {
                Logger.LogError(ex, ex.Message);
            }
            return default;
        }
    }
}
