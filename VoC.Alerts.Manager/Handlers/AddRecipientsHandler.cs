﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Entities.ViewModels;
using VoC.Alerts.Manager.Commands;

namespace VoC.Alerts.Manager.Handlers
{
    public class AddRecipientsHandler : BaseHandler<AddAlertRecipientsCommand, IEnumerable<AlertRecipientsViewModel>>
    {
        private readonly IAlertsRepository _alertsRepository;

        public AddRecipientsHandler(ILogger<AddRecipientsHandler> logger, IAlertsRepository alertsRepository) : base(logger)
        {
            _alertsRepository = alertsRepository;
        }

        protected override async Task<IEnumerable<AlertRecipientsViewModel>> HandleAsync(AddAlertRecipientsCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var createdRecipients= await _alertsRepository.AddRecipients(request.AlertId, request.Emails, cancellationToken).ConfigureAwait(false);
                return createdRecipients.Select(x => new AlertRecipientsViewModel()
                {
                    Email = x.Email,
                    Enabled = x.Enabled,
                    Id = x.Id
                });
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, ex.Message);
            }
            return default;
        }
    }
}
