﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;

namespace VoC.Alerts.Manager.Handlers
{
    public abstract class BaseHandler<TCommand, TResult> : IRequestHandler<TCommand, TResult>
    where TCommand : IRequest<TResult>
    {
        protected readonly ILogger<BaseHandler<TCommand, TResult>> Logger;

        protected BaseHandler(ILogger<BaseHandler<TCommand, TResult>> logger)
        {
            Logger = logger;
        }

        public virtual async Task<TResult> Handle(TCommand request, CancellationToken cancellationToken)
        {
            LogCommand();
            return await HandleAsync(request, cancellationToken).ConfigureAwait(false);
        }

        protected abstract Task<TResult> HandleAsync(TCommand request, CancellationToken cancellationToken);

        private void LogCommand()
        {
            Logger.LogInformation($"Handling new command of type '{typeof(TCommand).Name}'");
        }
    }
}
