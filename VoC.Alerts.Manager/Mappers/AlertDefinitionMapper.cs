﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VoC.Alerts.Constants;
using VoC.Alerts.Context.Entities;
using VoC.Alerts.Entities.ViewModels;

namespace VoC.Alerts.Manager.Mappers
{
    public static class AlertDefinitionMapper
    {
        public static AlertDefinitionViewModel Map(AlertDefinition alert)
        {
            return new AlertDefinitionViewModel()
            {
                Id = alert.Id,
                ActivatedOn = alert.ActivatedOn,
                CompanyId = alert.CompanyId,
                CreatedOn = alert.CreatedOn,
                DeletedBy = alert.DeletedBy,
                DeletedOn = alert.DeletedOn,
                Enabled = alert.Enabled,
                Name = alert.Name,
                TotalNotifications = alert.TriggersSet.Instances.Count(x => x.State == AlertState.Initiated || x.State == AlertState.Communicated),
                Source = new AlertSourceViewModel()
                    { Id = alert.Source.Id, Value = alert.Source.Value, Type = alert.Source.Type },
                TriggersSet = new AlertTriggerSetViewModel()
                {
                    Id = alert.TriggersSet.Id,
                    AlertDefinitionId = alert.TriggersSet.AlertDefinitionId,
                    CommunicationDefinitionId = alert.TriggersSet.CommunicationDefinitionId,
                    Triggers = alert.TriggersSet.Triggers.Where(t => !t.DeletedOn.HasValue).Select(x => new AlertTriggerViewModel()
                    {
                        Id = x.Id,
                        Value = x.Value,
                        Lookups = x.Lookups.Where(l => !l.DeletedOn.HasValue).Select(y => new TriggerLookupViewModel()
                            { Id = y.Id, Value = y.Value, DeletedOn = y.DeletedOn, DeletedBy = y.DeletedBy }),
                        SourceId = x.SourceId,
                        TriggerSetId = x.TriggerSetId,
                        Type = x.Type,
                        DeletedBy = x.DeletedBy,
                        DeletedOn = x.DeletedOn
                    }),
                    IsMultipleNotificationTrigger = alert.TriggersSet.IsMultipleNotificationTrigger,
                    Threshold = alert.TriggersSet.Threshold,
                    TimeIntervalInMinutes = alert.TriggersSet.TimeIntervalInMinutes,
                    TriggersBelowThreshold = alert.TriggersSet.TriggersBelowThreshold
                },
                AlertCommunicationDefinition = new AlertCommunicationDefinitionViewModel()
                {
                    CompanyId = alert.AlertCommunicationDefinition.CompanyId,
                    AlertDefinitionId = alert.AlertCommunicationDefinition.AlertDefinitionId,
                    Id = alert.AlertCommunicationDefinition.Id,
                    Recipients = alert.AlertCommunicationDefinition.Recipients.Where(rec => !rec.DeletedOn.HasValue).Select(r =>
                        new AlertCommunicationRecipientViewModel()
                        {
                            Enabled = r.Enabled,
                            Address = r.Address,
                            Id = r.Id,
                            CommunicationChannelId = r.CommunicationChannelId,
                            CommunicationDefinitionId = r.CommunicationDefinitionId,
                            DeletedBy = r.DeletedBy,
                            DeletedOn = r.DeletedOn
                        }),
                    Message = alert.AlertCommunicationDefinition.Message
                }
            };
        }
    }
}
