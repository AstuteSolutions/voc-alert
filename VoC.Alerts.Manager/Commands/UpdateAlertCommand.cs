﻿using MediatR;
using System;
using System.Collections.Generic;
using VoC.Alerts.Entities.Models;

namespace VoC.Alerts.Manager.Commands
{
    public class UpdateAlertCommand : IRequest<bool>
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<AlertRecipient> Recipients { get; set; } = new List<AlertRecipient>();
        public TriggerSet TriggersSet { get; set; }
        public TriggerData[] Triggers { get; set; }
    }
}
