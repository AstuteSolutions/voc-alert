﻿using System;
using System.Collections.Generic;
using Voc.Alerts.Sensitivity.Filters;

namespace VoC.Alerts.Manager.Commands
{

    public class AlertSensitivityRequestCommand : BaseCommand<AlertSensitivityResponse>
    {
        public int? TimeIntervalInMinutes { get; set; }
        public TriggerData[] Triggers { get; set; }
        public int DataToCompareInDays { get; set; } = 7;
        public int SurveyId { get; set; }
    }



}