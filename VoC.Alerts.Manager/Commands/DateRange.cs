﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoC.Alerts.Manager.Commands
{
    public class DateRange
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
