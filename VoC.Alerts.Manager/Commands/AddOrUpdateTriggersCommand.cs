﻿using System;
using System.Collections.Generic;

namespace VoC.Alerts.Manager.Commands
{
    public class AddOrUpdateTriggersCommand : BaseCommand<List<TriggerData>>
    {
        public TriggerSet TriggerSet { get; set; }
        public TriggerData[] Triggers { get; set; }
    }


    public class TriggerSet
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string LogicalOperator { get; set; } = "OR";
        public decimal? Threshold { get; set; }
        public int? TimeIntervalInMinutes { get; set; }
        public int? NotificationFrequencyInHours { get; set; }
        public decimal? SmartDetectionPercentage { get; set; }
        public bool? IsMultipleNotificationTrigger { get; set; }
        public bool? TriggersBelowThreshold { get; set;}
    }

    public class TriggerData
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
        public LookUps[] Lookups { get; set; }
    }

    public class LookUps
    {
        public Guid Id { get; set; }
        public string Value { get; set; }
    }
}
