﻿using System;
using System.Collections.Generic;
using VoC.Alerts.Entities.ViewModels;

namespace VoC.Alerts.Manager.Commands
{
    public class AddAlertRecipientsCommand : BaseCommand<IEnumerable<AlertRecipientsViewModel>>
    {
        public string[] Emails { get; set; }
    }
}
