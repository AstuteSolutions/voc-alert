﻿using System;

namespace VoC.Alerts.Manager.Commands
{
    public class DefineAlertCommunicationCommand : BaseCommand<Guid>
    {
        public string MessageContent { get; set; }
    }
}
