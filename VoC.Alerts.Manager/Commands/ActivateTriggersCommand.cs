﻿using System;
using MediatR;

namespace VoC.Alerts.Manager.Commands
{
    public class ActivateTriggersCommand : IRequest<bool>
    {
        public Guid AlertId { get; set; }
        public bool Activate { get; set; }
    }
}
