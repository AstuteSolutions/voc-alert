﻿using System;
using MediatR;

namespace VoC.Alerts.Manager.Commands
{
    public class SetTriggersThresholdCommand : IRequest<bool>
    {
        public SetTriggersThresholdCommand(Guid alertDefinitionId, decimal? threshold)
        {
            AlertDefinitionId = alertDefinitionId;
            Threshold = threshold;
        }

        public decimal? Threshold { get; }
        public Guid AlertDefinitionId { get; }

    }
}
