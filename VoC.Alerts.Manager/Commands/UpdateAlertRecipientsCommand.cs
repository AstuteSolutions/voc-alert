﻿using System;
using System.Collections.Generic;
using System.Text;
using VoC.Alerts.Entities.ViewModels;

namespace VoC.Alerts.Manager.Commands
{
    public class UpdateAlertRecipientsCommand: BaseCommand<AlertRecipientsViewModel>
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public bool Enabled { get; set; }
    }
}
