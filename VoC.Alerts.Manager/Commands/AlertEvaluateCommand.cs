﻿using System;
using System.Collections.Generic;
using System.Text;
using VoC.Alerts.Entities.ViewModels;

namespace VoC.Alerts.Manager.Commands
{
    public class AlertEvaluateCommand : BaseCommand<AlertEvalutionResponse>
    {
        public AlertEvaluateCommand()
        {
            Respondents = new List<long>();
        }
        public List<long> Respondents { get; set; }
    }
}
