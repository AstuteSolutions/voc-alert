﻿using System;
using System.Collections.Generic;
using System.Text;
using VoC.Alerts.Entities.ViewModels;

namespace VoC.Alerts.Manager.Commands
{
    public class AlertEvaluateByDateRangeCommand:BaseCommand<AlertEvalutionResponse>
    {
        public DateRange DateRange { get; set; }
    }
}
