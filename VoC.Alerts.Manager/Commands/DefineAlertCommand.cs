﻿using System;
using MediatR;

namespace VoC.Alerts.Manager.Commands
{
    public class DefineAlertCommand : IRequest<Guid>
    {
        public string Title { get; set; }
        public int SurveyId { get; set; }
        public int CompanyId { get; set; }
        public string SourceType { get; set; }
        public string MessageContent { get; set; }

        public DefineAlertCommand(int companyId, int surveyId, string title, string messageContent)
        {
            SourceType = "Survey";
            CompanyId = companyId;
            SurveyId = surveyId;
            Title = title;
            MessageContent = string.IsNullOrEmpty(messageContent) ? throw new ArgumentNullException(nameof(messageContent)) : messageContent;
        }
    }
}
