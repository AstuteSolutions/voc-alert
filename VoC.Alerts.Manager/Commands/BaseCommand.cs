﻿using System;
using MediatR;

namespace VoC.Alerts.Manager.Commands
{
    public class BaseCommand<TResult> : IRequest<TResult>
    {
        public Guid AlertId { get; set; }
        public int CompanyId { get; set; }
    }
}
