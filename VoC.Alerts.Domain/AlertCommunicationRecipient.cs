﻿using System;

namespace VoC.Alerts.Domain
{
    public class AlertCommunicationRecipient
    {
        public Guid Id { get; set; }
        public Guid CommunicationDefinitionId { get; set; }
        public int CommunicationChannelId { get; set; }
        public string Address { get; set; }
        public bool Enabled { get; set; }
    }
}