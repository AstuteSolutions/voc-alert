﻿using System;

namespace VoC.Alerts.Domain
{
    public class TriggerData
    {
        public int Id { get; set; }
        public Guid SetId { get; set; }
        public string Type { get; set; }
        public string LookupId { get; set; }
        public TriggerStringLookup[] Answers { get; set; }
        public Type SourceType { get; set; }
        public DateTime LastRun { get; set; }
        public DateTime? DeletedOn { get; set; }
    }
}
