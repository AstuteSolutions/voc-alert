﻿using System;

namespace VoC.Alerts.Domain
{
    public abstract class Trigger
    {
        public int Id { get; }
        public string Type { get; }
        public Guid SetId { get; set; }
        protected Trigger(int id, string type)
        {
            Id = id;
            Type = type;
        }
    }

    public class TriggerNumericLookup : TriggerLookup
    {
        public int Value { get; set; }

        public TriggerNumericLookup(Guid id, int triggerId, int value) : base(id, triggerId)
        {
            Value = value;
        }
    }

    public class TriggerStringLookup : TriggerLookup
    {
        public string Value { get; }

        public TriggerStringLookup(Guid id, int triggerId, string value) : base(id, triggerId)
        {
            Value = value;
        }
    }

    public class TriggerLookup
    {
        public Guid Id { get; }
        public int TriggerId { get; }

        public TriggerLookup(Guid id, int triggerId)
        {
            Id = id;
            TriggerId = triggerId;
        }
    }
}
