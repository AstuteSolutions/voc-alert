﻿using System;

namespace VoC.Alerts.Domain
{
    public class TriggerEvent
    {
        public Guid Id { get; }
        public int TriggerId { get; }
        public Guid TriggersSetId { get; }
        public int CompanyId { get; }
        public bool Outcome { get; }
        public DateTime ExecutionTime { get; }
        public TriggerStringLookup Match { get; }
        public string TargetMatchId { get; set; }

        public TriggerEvent(Guid id, int triggerId, Guid triggersSetId, DateTime executionTime, int companyId)
        {
            Id = id;
            TriggerId = triggerId;
            TriggersSetId = triggersSetId;
            Outcome = false;
            CompanyId = companyId;
            ExecutionTime = executionTime;
        }

        public TriggerEvent(Guid id, int triggerId, Guid triggersSetId, DateTime executionTime, int companyId, TriggerStringLookup match)
            : this(id, triggerId, triggersSetId, executionTime, companyId)
        {
            if (match == null || string.IsNullOrEmpty(match.Value))
            {
                throw new ArgumentException(nameof(match));
            }

            Match = match;
            Outcome = true;
        }
    }
}
