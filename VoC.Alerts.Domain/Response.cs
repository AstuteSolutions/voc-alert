﻿namespace VoC.Alerts.Domain
{
    public class Response
    {
        public long Id { get; }
        public Question Question { get; set; }
        public Survey Survey { get; set; }
        public string Answer { get; set; }
        public int[] TagId { get; set; }

        public Response(long respondentId)
        {
            Id = respondentId;
        }
    }
}
