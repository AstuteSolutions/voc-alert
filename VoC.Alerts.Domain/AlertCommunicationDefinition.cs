﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoC.Alerts.Domain
{
    public class AlertCommunicationDefinition
    {
        public Guid Id { get; set; }
        public Guid AlertDefinitionId { get; set; }
        public int CompanyId { get; set; }
        public string Message { get; set; }

        public virtual IEnumerable<AlertCommunicationRecipient> Recipients { get; set; } =
            new List<AlertCommunicationRecipient>();
    }
}
