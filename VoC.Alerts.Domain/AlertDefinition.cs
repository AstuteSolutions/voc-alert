﻿using System;
using System.Collections.Generic;

namespace VoC.Alerts.Domain
{
    // Alert data and properties.
    public class AlertDefinition
    {
        public Guid Id { get; }
        public string Name { get; }
        public Source Source { get; set; }
        public Company Company { get; set; }
        public bool Enabled { get; set; }
        public string CreatedBy { get; }
        public DateTime CreatedOn { get; }

        public DateTime? ActivatedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public string DeletedBy { get; set; }
        public IEnumerable<TriggerData> Triggers { get; }
        public decimal? Threshold { get; set; }
        public AlertCommunicationDefinition AlertCommunicationDefinition { get; }

        public AlertTriggerSets AlertTriggerSets { get; set; }

        public AlertDefinition(Guid id, string name, string createdBy, DateTime createdOn, List<TriggerData> triggers)
        {
            Id = id;
            Name = name;
            CreatedBy = createdBy;
            CreatedOn = createdOn;
            Triggers = triggers;
        }
        public AlertDefinition(Guid id, string name, string createdBy, DateTime createdOn, List<TriggerData> triggers, AlertCommunicationDefinition alertCommunicationDefinition)
        {
            Id = id;
            Name = name;
            CreatedBy = createdBy;
            CreatedOn = createdOn;
            Triggers = triggers;
            AlertCommunicationDefinition = alertCommunicationDefinition;
        }

        public AlertDefinition(string name, string createdBy, List<TriggerData> triggers) : this(Guid.NewGuid(), name, createdBy, DateTime.UtcNow, triggers)
        {
        }
    }
}
