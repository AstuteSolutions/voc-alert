﻿namespace VoC.Alerts.Domain
{
    public abstract class Source
    {
        public int Id { get; set; }
        public string Type { get; protected set; }
        public string Value { get; protected set; }
        protected Source(string type, string value)
        {
            Type = type;
            Value = value;
        }
    }

    public class SurveySource : Source
    {
        public int SurveyId { get; }

        public SurveySource(int surveyId) : base("Survey", surveyId.ToString())
        {
            SurveyId = surveyId;
        }
    }
}
