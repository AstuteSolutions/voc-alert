﻿using System;

namespace VoC.Alerts.Domain
{
    public class Recipient
    {
        public Guid Id { get; }
        public int CommunicationId { get; } = 1;
        public string Address { get; set; }

        public Recipient(Guid id)
        {
            Id = id;
        }
    }
}
