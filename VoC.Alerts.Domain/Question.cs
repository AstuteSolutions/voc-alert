﻿namespace VoC.Alerts.Domain
{
    public class Question
    {
        public int Id { get; }
        public string Text { get; set; }
        public int TypeId { get; set; }
        public QuestionType Type { get; set; }

        public Question(int questionId)
        {
            Id = questionId;
        }
    }

    public enum QuestionType
    {

    }
}
