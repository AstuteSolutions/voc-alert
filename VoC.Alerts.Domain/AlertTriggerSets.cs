﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoC.Alerts.Domain
{
    public class AlertTriggerSets
    {
        public Guid Id { get; set; }
        public int LogicalOperation { get; set; } = 2;
        public decimal? Threshold { get; set; }
        public DateTime? LastRun { get; set; }
        public int? NotificationInHours { get; set; }
        public int? TimeIntervalInMinutes { get; set; }
        public virtual ICollection<TriggerData> Triggers { get; set; }
        public bool? TriggersBelowThreshold { get; set;}
    }
}
