﻿namespace VoC.Alerts.Domain
{
    public class Company
    {
        public int Id { get; }
        public string Name { get; }

        public Company(int companyId)
        {
            Id = companyId;
        }

        public Company(int companyId, string companyName) :this(companyId)
        {
            Name = companyName;
        }

        public override string ToString()
        {
            return string.IsNullOrEmpty(Name) ? $"{Id}" : Name;
        }
    }
}
