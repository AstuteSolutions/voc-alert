﻿using System;

namespace VoC.Alerts.Domain
{
    public class Alert
    {
        public Guid Id { get; }
        public string Name { get; }
        public AlertDefinition Definition { get; }

        public Alert(string name, AlertDefinition definition)
        {
            Id = Guid.NewGuid();
            Name = name;
            Definition = definition;
        }

        public Alert(Guid id, string name, AlertDefinition definition)
        {
            Id = id;
            Name = name;
            Definition = definition;
        }
    }
}
