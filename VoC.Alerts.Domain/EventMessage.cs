﻿using System;
using Iperloop.Core.Events.Models;

namespace VoC.Alerts.Domain
{
    public class EventMessage : EventBase
    {
        public int CompanyId { get; set; }
        public Guid AlertId { get; set; }
        public string Type { get; set; }
        public int TriggerId { get; set; }
        public Guid TriggerSetId { get; set; }
        public DateTime ExecutionTime { get; set; }
        public new Guid CorrelationId { get; set; }

        public EventMessage(Guid eventId) : base(eventId, DateTime.UtcNow)
        {   
        }
    }
}
