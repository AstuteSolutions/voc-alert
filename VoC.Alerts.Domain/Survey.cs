﻿using System.Collections.Generic;

namespace VoC.Alerts.Domain
{
    public class Survey
    {
        public int Id { get; }

        public string Title { get; set; }

        public Survey(int surveyId)
        {
            Id = surveyId;
        }
    }
}
