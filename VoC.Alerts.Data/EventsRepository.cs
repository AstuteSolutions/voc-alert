﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using VoC.Alerts.Constants;
using VoC.Alerts.Context;
using VoC.Alerts.Context.Entities;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Domain;
using TriggerEvent = VoC.Alerts.Domain.TriggerEvent;
using TriggerLookup = VoC.Alerts.Domain.TriggerLookup;

namespace VoC.Alerts.Data
{
    public class EventsRepository : IEventsRepository
    {
        private readonly AlertsContext _context;
        private readonly ILogger<EventsRepository> _logger;

        public EventsRepository(AlertsContext context, ILogger<EventsRepository> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task ProcessEventRecordAsync(Guid eventId, CancellationToken cancellationToken)
        {
            try
            {
                var entity = await _context.TriggerEvents.FindAsync(eventId);
                if (entity == null)
                {
                    throw new InvalidOperationException();
                }

                entity.ProcessedOn = DateTime.UtcNow;
                _context.TriggerEvents.Update(entity);
                await _context.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }

        public async Task<TriggerEvent> GetAwaitingEventById(Guid eventId)
        {
            try
            {
                var eventEntity = await _context.TriggerEvents.Include(i => i.TriggersSet)
                    .Include(i => i.Trigger)
                    .Include(i => i.MatchLookup)
                    .FirstOrDefaultAsync(e => e.Id.Equals(eventId) && !e.ProcessedOn.HasValue);
                if (eventEntity == null)
                {
                    return default;
                }

                return eventEntity.MatchLookupId.HasValue
                    ? new TriggerEvent(eventEntity.Id, eventEntity.TriggerId, eventEntity.TriggersSetId, eventEntity.ExecutionTime,
                        eventEntity.CompanyId,
                        new TriggerStringLookup(eventEntity.MatchLookup.Id, eventEntity.TriggerId, eventEntity.MatchLookup.Value))
                    : new TriggerEvent(eventEntity.Id, eventEntity.TriggerId, eventEntity.TriggersSetId, eventEntity.ExecutionTime,
                        eventEntity.CompanyId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }

        public async Task GenerateAlertInstanceAsync(Guid id, TriggerEvent @event, AlertState state, CancellationToken cancellationToken)
        {
            try
            {
                var triggersSet = await _context.TriggerSets.Include(i => i.CommunicationDefinition)
                    .Include(i => i.AlertDefinition)
                    .ThenInclude(src => src.Source)
                    .FirstOrDefaultAsync(x => x.Id.Equals(@event.TriggersSetId), cancellationToken: cancellationToken)
                    .ConfigureAwait(false);
                _logger.LogInformation("triggerSet :{triggerSetId}", triggersSet.Id);
                _logger.LogInformation($"SurveyId:{triggersSet.AlertDefinition.Source.Value}");
                _context.AlertInstances.Add(new AlertInstance
                {
                    Id = id,
                    AlertDefinitionId = triggersSet.AlertDefinitionId,
                    CompanyId = @event.CompanyId,
                    CreatedOn = DateTime.UtcNow,
                    CommunicationDefinitionId = triggersSet.CommunicationDefinitionId,
                    Message = string.Format(triggersSet.CommunicationDefinition.Message,
                        triggersSet.AlertDefinition.Name, triggersSet.AlertDefinition.Source.Value),
                    State = state,
                    TriggerSetId = triggersSet.Id
                });
                await _context.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }
    }
}
