﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using VoC.Alerts.Context;
using VoC.Alerts.Context.Entities;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Entities.Models;

namespace VoC.Alerts.Data
{
    public class AlertsRecipientRepository : IAlertsRecipientRepository
    {
        private readonly AlertsContext _context;
        private readonly ILogger<AlertsRecipientRepository> _logger;
        public AlertsRecipientRepository(AlertsContext context, ILogger<AlertsRecipientRepository> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<AlertCommunicationRecipient> UpdateAlertCommunicationRecipient(AlertRecipient alertRecipient)
        {
            var recipient = await _context.CommunicationRecipients.
                FirstOrDefaultAsync(x => x.Id.Equals(alertRecipient.Id));
            if (recipient == null)
            {
                throw new ArgumentException($"Cannot find Alert Recipient with ID: '{alertRecipient.Id}'");
            }

            recipient.Address = alertRecipient.Email;
            recipient.Enabled = alertRecipient.Enabled;

            _context.Update(recipient);
            await _context.SaveChangesAsync();
            return recipient;
        }
    }
}
