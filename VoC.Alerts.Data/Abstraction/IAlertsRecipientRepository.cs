﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VoC.Alerts.Context.Entities;
using VoC.Alerts.Entities.Models;

namespace VoC.Alerts.Data.Abstraction
{
    public interface IAlertsRecipientRepository
    {
        Task<AlertCommunicationRecipient> UpdateAlertCommunicationRecipient(AlertRecipient alertRecipient);
    }
}
