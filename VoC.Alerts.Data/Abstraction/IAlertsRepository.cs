﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using VoC.Alerts.Context.Entities;
using VoC.Alerts.Domain;
using VoC.Alerts.Entities.Models;

namespace VoC.Alerts.Data.Abstraction
{
    public interface IAlertsRepository
    {
        Task<AlertTriggerSet> GetTriggerSetAsync(Guid triggerSetId);
        IEnumerable<Alert> GetAlertDefinitions(int companyId);
        Task SetDefinitionLastRunAsync(Guid alertDefinitionId, CancellationToken cancellationToken);
        Task<Guid> CreateSurveyAlert(int companyId, int surveyId, string title, string messageContent,CancellationToken cancellationToken);
        Task<Guid> SetAlertCommunicationMessage(Guid alertDefinitionId, string messageContent, CancellationToken cancellationToken);
        Task<List<AlertRecipient>> AddRecipients(Guid alertDefinitionId, string[] recipients, CancellationToken cancellationToken);
        Task UpdateAlertName(Guid alertDefinitionId, string name, CancellationToken cancellationToken);
        Task<bool> UpdateTriggers(Guid alertDefinitionId, AlertTriggerSet triggerSet, IList<AlertTrigger> triggers, CancellationToken cancellationToken);
        Task<List<AlertRecipient>> UpdateRecipients(Guid alertDefinitionId, string[] recipients, CancellationToken cancellationToken);
        Task<IList<AlertTrigger>> AddTriggers(Guid alertDefinitionId,AlertTriggerSet triggerSet, IList<AlertTrigger> triggers, CancellationToken cancellationToken);
        Task<bool> ActivateTriggers(Guid alertDefinitionId,bool activate, CancellationToken cancellationToken);
        Task<bool> DeleteAlert(Guid alertDefinitionId, CancellationToken cancellationToken);
        IEnumerable<VoC.Alerts.Context.Entities.AlertDefinition> GetAlertDefinitionsByCompany(int companyId);
        Task<VoC.Alerts.Context.Entities.AlertDefinition> GetAlertDefinition(Guid alertDefinitionId);
        Task<decimal?> SetThresholdAsync(Guid alertDefinitionId, decimal? thresholdValue);
        Task<Alert> GetAlertDefinitionById(Guid alertDefinitonId);

        Task<bool> UpdateAlertTriggerSetAsync(AlertTriggerSet alertTriggerSet);
    }
}
