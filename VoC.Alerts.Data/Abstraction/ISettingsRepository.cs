﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace VoC.Alerts.Data.Abstraction
{
    public interface ISettingsRepository
    {
        Task<IEnumerable<CompanyServiceSettings>> GetEnabledCompaniesForService(string serviceCode);
    }

    public class CompanyServiceSettings
    {
        public int CompanyId { get; set; }
        public string Data { get; set; }
    }
}
