﻿using System;
using System.Threading;
using System.Threading.Tasks;
using VoC.Alerts.Constants;
using VoC.Alerts.Domain;

namespace VoC.Alerts.Data.Abstraction
{
    public interface IEventsRepository
    {
        Task ProcessEventRecordAsync(Guid eventId, CancellationToken cancellationToken);
        Task<TriggerEvent> GetAwaitingEventById(Guid eventId);
        Task GenerateAlertInstanceAsync(Guid id,TriggerEvent @event, AlertState state, CancellationToken cancellationToken);
    }
}
