﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using VoC.Alerts.Context;
using VoC.Alerts.Context.Entities;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Domain;
using VoC.Alerts.Entities.Models;
using AlertCommunicationDefinition = VoC.Alerts.Context.Entities.AlertCommunicationDefinition;
using AlertCommunicationRecipient = VoC.Alerts.Context.Entities.AlertCommunicationRecipient;
using AlertDefinition = VoC.Alerts.Domain.AlertDefinition;


namespace VoC.Alerts.Data
{
    public class AlertsRepository : IAlertsRepository
    {
        private readonly AlertsContext _context;
        private readonly ILogger<AlertsRepository> _logger;

        public AlertsRepository(AlertsContext context, ILogger<AlertsRepository> logger)
        {
            _context = context;
            _logger = logger;
        }
        //ignore past 10 days events
        public async Task<AlertTriggerSet> GetTriggerSetAsync(Guid triggerSetId)
        {
            try
            {
                return await _context.TriggerSets.Include(i => i.CommunicationDefinition)
                    .Include(t => t.Triggers.Where(t => !t.DeletedOn.HasValue))
                    .Include(i => i.AlertDefinition)
                    .Include(i => i.Instances.Where(x=>x.CreatedOn>DateTime.Now.AddDays(-10)))
                    .Include(i => i.TriggerEvents.Where(x => x.ExecutionTime > DateTime.Now.AddDays(-10)))
                    .FirstOrDefaultAsync(x => x.Id.Equals(triggerSetId))
                    .ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }

        public IEnumerable<Alert> GetAlertDefinitions(int companyId)
        {
            var alertEntities = _context.Definitions.Include(i => i.Source)
                .Include(i => i.TriggersSet)
                .Include(i => i.TriggersSet).ThenInclude(ii => ii.Triggers)
                .Include(i => i.TriggersSet).ThenInclude(ii => ii.Triggers.Where(x => !x.DeletedOn.HasValue)).ThenInclude(iii => iii.TriggerSet)
                .Include(i => i.TriggersSet).ThenInclude(ii => ii.Triggers.Where(x => !x.DeletedOn.HasValue)).ThenInclude(iii => iii.Lookups)
                .Include(cd => cd.AlertCommunicationDefinition)
                .ThenInclude(cdr => cdr.Recipients)
                .Where(x => x.CompanyId.Equals(companyId) && x.Enabled);
            var mappedAlerts = new List<Alert>();

            foreach (var entity in alertEntities)
            {
                var alert = new Alert(entity.Id, entity.Name, new AlertDefinition(entity.Id, entity.Name, entity.CreatedBy, entity.CreatedOn,
                    entity.TriggersSet.Triggers.Select(t => new TriggerData
                    {
                        Id = t.Id,
                        LookupId = t.Value,
                        Type = t.Type,
                        SetId = t.TriggerSetId,
                        LastRun = t.TriggerSet.LastRun ?? entity.ActivatedOn.GetValueOrDefault(),
                        Answers = t.Lookups.Select(c => new TriggerStringLookup(c.Id, t.Id, c.Value)).ToArray()
                    }).ToList(), new Domain.AlertCommunicationDefinition()
                    {
                        Id = entity.AlertCommunicationDefinition.Id,
                        CompanyId = entity.AlertCommunicationDefinition.CompanyId,
                        Recipients = entity.AlertCommunicationDefinition?.Recipients.Where(r => !r.DeletedOn.HasValue).Select(x => new Domain.AlertCommunicationRecipient()
                        {
                            Address = x.Address,
                            Enabled = x.Enabled,
                            Id = x.Id,
                            CommunicationChannelId = x.CommunicationChannelId,
                            CommunicationDefinitionId = x.CommunicationDefinitionId,
                        }),
                        AlertDefinitionId = entity.AlertCommunicationDefinition.AlertDefinitionId,
                        Message = entity.AlertCommunicationDefinition?.Message
                    })
                {
                    Enabled = entity.Enabled,
                    Source = CreateSource(entity.Source),
                    Company = new Company(entity.CompanyId),
                    Threshold = entity.TriggersSet.Threshold,
                    AlertTriggerSets = new AlertTriggerSets()
                    {
                        Id = entity.TriggersSet.Id,
                        LastRun = entity.TriggersSet.LastRun,
                        LogicalOperation = entity.TriggersSet.LogicalOperation,
                        Threshold = entity.TriggersSet.Threshold,
                        NotificationInHours=entity.TriggersSet.NotificationFrequencyInHours,
                        TimeIntervalInMinutes=entity.TriggersSet.TimeIntervalInMinutes,
                        TriggersBelowThreshold=entity.TriggersSet.TriggersBelowThreshold
                    }
                });

                mappedAlerts.Add(alert);
            }
            return mappedAlerts;
        }

        public async Task SetDefinitionLastRunAsync(Guid alertDefinitionId, CancellationToken cancellationToken)
        {
            var triggerSets = _context.TriggerSets.Where(x => x.AlertDefinitionId.Equals(alertDefinitionId));
            foreach (var set in triggerSets)
            {
                set.LastRun = DateTime.UtcNow;
            }

            _context.UpdateRange(triggerSets);
            await _context.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
        }

        private static Source CreateSource(AlertSource source)
        {
            switch (source.Type)
            {
                case "Survey":
                    if (int.TryParse(source.Value, out var surveyId))
                    {
                        return new SurveySource(surveyId);
                    }
                    break;
                default:
                    throw new NotImplementedException();
            }
            throw new NotImplementedException();
        }

        public async Task<Guid> CreateSurveyAlert(int companyId, int surveyId, string title, string messageContent, CancellationToken cancellationToken)
        {
            var definitionEntity = new Context.Entities.AlertDefinition
            {
                Id = Guid.NewGuid(),
                CompanyId = companyId,
                ActivatedOn = DateTime.UtcNow,
                CreatedBy = "alerts.api",
                CreatedOn = DateTime.UtcNow,
                Enabled = true,
                Name = title,
                Source = new AlertSource
                {
                    Type = "Survey",
                    Value = $"{surveyId}"
                },
                AlertCommunicationDefinition = new AlertCommunicationDefinition
                {
                    Id = Guid.NewGuid(),
                    Message = messageContent,
                    CompanyId = companyId
                }
            };
            _context.Definitions.Add(definitionEntity);
            await _context.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
            return definitionEntity.Id;
        }

        public async Task<Guid> SetAlertCommunicationMessage(Guid alertDefinitionId, string messageContent, CancellationToken cancellationToken)
        {
            var alertDefinition = await _context.Definitions.FindAsync(alertDefinitionId).ConfigureAwait(false);
            if (alertDefinition == null)
            {
                throw new ArgumentException($"Cannot find Alert Definition with ID: '{alertDefinitionId}'");
            }

            var communicationDefinition = await
                _context.CommunicationDefinitions.FirstOrDefaultAsync(x => x.AlertDefinitionId.Equals(alertDefinition), cancellationToken: cancellationToken).ConfigureAwait(false);
            if (communicationDefinition == null)
            {
                communicationDefinition = new AlertCommunicationDefinition
                {
                    Id = Guid.NewGuid(),
                    Message = messageContent,
                    CompanyId = alertDefinition.CompanyId,
                    AlertDefinitionId = alertDefinitionId
                };
                _context.CommunicationDefinitions.Add(communicationDefinition);
            }
            else
            {
                communicationDefinition.Message = messageContent;
                _context.CommunicationDefinitions.Update(communicationDefinition);
            }

            await _context.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
            return communicationDefinition.Id;
        }

        public async Task<List<AlertRecipient>> AddRecipients(Guid alertDefinitionId, string[] recipients, CancellationToken cancellationToken)
        {
            var addedRecipients = new List<AlertRecipient>();
            var communicationDefinition = await _context.CommunicationDefinitions
                .Include(i => i.Definition)
                .Include(i => i.Recipients)
                .FirstOrDefaultAsync(x => x.AlertDefinitionId.Equals(alertDefinitionId), cancellationToken: cancellationToken).ConfigureAwait(false);
            if (communicationDefinition == null)
            {
                throw new ArgumentException($"Cannot find Communication Definition for Alert with ID: '{alertDefinitionId}'");
            }

            foreach (var newRecipient in recipients)
            {
                if (communicationDefinition.Recipients.Any(x =>
                        x.Address.Equals(newRecipient, StringComparison.InvariantCultureIgnoreCase) && !x.DeletedOn.HasValue))
                {
                    continue;
                }

                var recipientRecord = new AlertCommunicationRecipient
                {
                    Id = Guid.NewGuid(),
                    CommunicationDefinitionId = communicationDefinition.Id,
                    CommunicationChannelId = 1,
                    Address = newRecipient,
                    Enabled = true
                };
                _context.CommunicationRecipients.Add(recipientRecord);
                addedRecipients.Add(new AlertRecipient()
                {
                    Id = recipientRecord.Id,
                    Email = newRecipient,
                    Enabled = true
                });
            }

            await _context.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
            return addedRecipients;
        }

        public async Task<List<AlertRecipient>> UpdateRecipients(Guid alertDefinitionId, string[] recipients, CancellationToken cancellationToken)
        {
            var communicationDefinition = await _context.CommunicationDefinitions
                .Include(i => i.Definition)
                .Include(i => i.Recipients)
                .FirstOrDefaultAsync(x => x.AlertDefinitionId.Equals(alertDefinitionId), cancellationToken: cancellationToken).ConfigureAwait(false);
            if (communicationDefinition == null)
            {
                throw new ArgumentException($"Cannot find Communication definition with ID: '{alertDefinitionId}'");
            }
            var recipientsToDelete = communicationDefinition.Recipients.AsEnumerable().Where(r => !r.DeletedOn.HasValue
                && recipients.All(r2 => !r2.Equals(r.Address, StringComparison.InvariantCultureIgnoreCase)));
            foreach (var recipient in recipientsToDelete)
            {
                var recipientItem = communicationDefinition.Recipients.FirstOrDefault(x => x.Address.Equals(recipient.Address, StringComparison.InvariantCultureIgnoreCase));
                recipientItem.DeletedBy = "alerts.api";
                recipientItem.DeletedOn = DateTime.UtcNow;
                _context.CommunicationRecipients.Update(recipientItem);
            }
            return await AddRecipients(alertDefinitionId, recipients.Where(x => recipientsToDelete.All(x2 =>
                    !x2.Address.Equals(x, StringComparison.InvariantCultureIgnoreCase))).ToArray(), cancellationToken).ConfigureAwait(false);
        }

        public async Task UpdateAlertName(Guid alertDefinitionId, string name, CancellationToken cancellationToken)
        {
            var alertDefinition = await _context.Definitions.FirstOrDefaultAsync(x => x.Id.Equals(alertDefinitionId), cancellationToken: cancellationToken).ConfigureAwait(false);
            if (alertDefinition == null)
            {
                throw new ArgumentException($"Cannot find Alert Definition with ID: '{alertDefinitionId}'");
            }
            alertDefinition.Name = name;
        }

        public async Task<bool> UpdateTriggers(Guid alertDefinitionId, AlertTriggerSet triggerSet, IList<AlertTrigger> triggers, CancellationToken cancellationToken)
        {
            var alertDefinition = await _context.Definitions.Include(i => i.TriggersSet)
                .ThenInclude(ii => ii.Triggers)
                .FirstOrDefaultAsync(x => x.Id.Equals(alertDefinitionId), cancellationToken: cancellationToken)
                .ConfigureAwait(false);
            if (alertDefinition == null)
            {
                throw new ArgumentException($"Cannot find Alert Definition with ID: '{alertDefinitionId}'");
            }
            triggerSet.Id = _context.TriggerSets.FirstOrDefault(x => x.AlertDefinitionId.Equals(alertDefinitionId))?.Id ?? triggerSet.Id;
            await EditTriggerSet(triggerSet).ConfigureAwait(false);
            await UpdateTriggerData(triggers, alertDefinition).ConfigureAwait(false);
            await _context.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
            return true;
        }

        private async Task UpdateTriggerData(IEnumerable<AlertTrigger> alertTriggers, Context.Entities.AlertDefinition alertDefinition)
        {
            var triggersToDelete = alertDefinition.TriggersSet.Triggers.AsEnumerable().Where(t => !t.DeletedOn.HasValue && !alertTriggers.Any(t2 => t2.Value.Equals(t.Value)));
            foreach (var trigger in triggersToDelete)
            {
                RemoveTrigger(trigger);
            }
            foreach (var alertTrigger in alertTriggers.Where(x => triggersToDelete.All(x2 => !x2.Value.Equals(x.Value))))
            {
                var existingAlertTrigger = alertDefinition.TriggersSet.Triggers.FirstOrDefault(x => x.Value.Equals(alertTrigger.Value) && !x.DeletedOn.HasValue); // same question
                if (existingAlertTrigger != null)
                {
                    UpdateTriggerLookUps(existingAlertTrigger, alertTrigger.Lookups.AsEnumerable());
                }
                else
                {
                    var entityAlertTrigger = new AlertTrigger
                    {
                        SourceId = alertDefinition.SourceId,
                        TriggerSetId = alertDefinition.TriggersSet.Id,
                        Type = alertTrigger.Type,
                        Value = alertTrigger.Value,
                    };
                    _context.Triggers.Add(entityAlertTrigger);
                    await AddOrEditTriggerLookUps(alertTrigger.Lookups.AsEnumerable(), entityAlertTrigger);
                }
            }
        }

        private void RemoveTrigger(AlertTrigger alertTrigger)
        {
            foreach (var lookup in _context.TriggerLookUps.Where(l => l.TriggerId.Equals(alertTrigger.Id)))
            {
                DeleteTriggerLookup(lookup);
            }
            alertTrigger.DeletedBy = "alerts.api";
            alertTrigger.DeletedOn = DateTime.UtcNow;
            _context.Triggers.Update(alertTrigger);
        }

        private void UpdateTriggerLookUps(AlertTrigger alertTrigger, IEnumerable<Context.Entities.TriggerLookup> triggerLookups)
        {
            var lookupsToDelete = _context.TriggerLookUps.AsEnumerable()
                .Where(tl_1 => !tl_1.DeletedOn.HasValue && tl_1.TriggerId.Equals(alertTrigger.Id) && triggerLookups.All(tl_2 => !tl_2.Value.Equals(tl_1.Value)));
            foreach (var lookup in lookupsToDelete)
            {
                DeleteTriggerLookup(lookup);
            }
            foreach (var lookup in triggerLookups.Where(x => lookupsToDelete.All(x2 => !x2.Value.Equals(x.Value, StringComparison.InvariantCultureIgnoreCase))))
            {
                var existingLookUp = _context.TriggerLookUps.FirstOrDefault(x => x.TriggerId.Equals(alertTrigger.Id) && x.Value.Equals(lookup.Value) && !x.DeletedOn.HasValue);
                if (existingLookUp == null)
                {
                    var newLookUp = new VoC.Alerts.Context.Entities.TriggerLookup()
                    {
                        Id = Guid.NewGuid(),
                        TriggerId = alertTrigger.Id,
                        Trigger = alertTrigger,
                        Value = lookup.Value
                    };
                    _context.TriggerLookUps.Add(newLookUp);
                }
            }
        }

        private void DeleteTriggerLookup(Context.Entities.TriggerLookup lookup)
        {
            lookup.DeletedBy = "alerts.api";
            lookup.DeletedOn = DateTime.UtcNow;
            _context.TriggerLookUps.Update(lookup);
        }
        public async Task<IList<AlertTrigger>> AddTriggers(Guid alertDefinitionId, AlertTriggerSet triggerSet, IList<AlertTrigger> triggers, CancellationToken cancellationToken)
        {
            var alertDefinition = await _context.Definitions.Include(i => i.TriggersSet)
                .ThenInclude(ii => ii.Triggers)
                .FirstOrDefaultAsync(x => x.Id.Equals(alertDefinitionId), cancellationToken: cancellationToken)
                .ConfigureAwait(false);
            var communicationDefinition =
                await _context.CommunicationDefinitions.FirstOrDefaultAsync(x =>
                    x.AlertDefinitionId.Equals(alertDefinitionId), cancellationToken: cancellationToken).ConfigureAwait(false);
            if (alertDefinition == null || communicationDefinition == null)
            {
                throw new ArgumentException($"Cannot find Alert Definition or Communication definition with ID: '{alertDefinitionId}'");
            }

            if(triggerSet.TimeIntervalInMinutes > 4320 || triggerSet.TimeIntervalInMinutes  <= 0)
            {
                throw new ArgumentOutOfRangeException($"triggerSet.TimeIntervalInMinutes must be in range 1 - 4320. alertDefinitionId: '{alertDefinitionId}'");
            }

            if (alertDefinition.TriggersSet == null)
            {
                triggerSet.CommunicationDefinitionId = communicationDefinition.Id;
                alertDefinition.TriggersSet = triggerSet;
                _context.TriggerSets.Add(alertDefinition.TriggersSet);
            }
            else
            {
                await EditTriggerSet(triggerSet);
            }
            await AddOrEditTriggers(triggers, alertDefinition);
            await _context.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
            return triggers;
        }

        private async Task EditTriggerSet(AlertTriggerSet alertTriggerSet)
        {
            var triggerSet = await _context.TriggerSets.FirstOrDefaultAsync(x => x.Id.Equals(alertTriggerSet.Id));
            if (triggerSet != null)
            {
                triggerSet.IsMultipleNotificationTrigger = alertTriggerSet.IsMultipleNotificationTrigger;
                triggerSet.LogicalOperation = alertTriggerSet.LogicalOperation;
                triggerSet.NotificationFrequencyInHours = alertTriggerSet.NotificationFrequencyInHours;
                triggerSet.SmartDetectionPercentage = alertTriggerSet.SmartDetectionPercentage;
                triggerSet.Threshold = alertTriggerSet.Threshold;
                triggerSet.TimeIntervalInMinutes = alertTriggerSet.TimeIntervalInMinutes;
                triggerSet.TriggersBelowThreshold = alertTriggerSet.TriggersBelowThreshold;
                _context.TriggerSets.Update(triggerSet);
            }
        }

        private async Task AddOrEditTriggers(IEnumerable<AlertTrigger> alertTriggers,
            Context.Entities.AlertDefinition alertDefinition)
        {
            foreach (var alertTrigger in alertTriggers)
            {
                var existingAlertTrigger = await _context.Triggers.FirstOrDefaultAsync(x => x.Id.Equals(alertTrigger.Id) && !x.DeletedOn.HasValue);
                if (existingAlertTrigger != null)
                {
                    existingAlertTrigger.Type = alertTrigger.Type;
                    existingAlertTrigger.Value = alertTrigger.Value;
                    _context.Triggers.Update(existingAlertTrigger);
                    await AddOrEditTriggerLookUps(alertTrigger.Lookups.AsEnumerable(), existingAlertTrigger);
                }
                else
                {
                    var entityAlertTrigger = new AlertTrigger
                    {
                        SourceId = alertDefinition.SourceId,
                        TriggerSetId = alertDefinition.TriggersSet.Id,
                        Type = alertTrigger.Type,
                        Value = alertTrigger.Value,
                    };
                    _context.Triggers.Add(entityAlertTrigger);
                    await AddOrEditTriggerLookUps(alertTrigger.Lookups.AsEnumerable(), entityAlertTrigger);
                }
            }
        }

        private async Task AddOrEditTriggerLookUps(IEnumerable<Context.Entities.TriggerLookup> triggerLookups, AlertTrigger alertTrigger)
        {
            foreach (var lookup in triggerLookups)
            {
                var existingLookUp =
                    await _context.TriggerLookUps.FirstOrDefaultAsync(x => x.Id.Equals(lookup.Id) && !x.DeletedOn.HasValue);
                if (existingLookUp != null)
                {
                    existingLookUp.Value = lookup.Value;

                    _context.TriggerLookUps.Update(existingLookUp);
                }
                else
                {
                    var newLookUp = new VoC.Alerts.Context.Entities.TriggerLookup()
                    {
                        Id = Guid.NewGuid(),
                        TriggerId = alertTrigger.Id,
                        Trigger = alertTrigger,
                        Value = lookup.Value
                    };
                    _context.TriggerLookUps.Add(newLookUp);
                }
            }
        }

        public async Task<bool> ActivateTriggers(Guid alertDefinitionId, bool activate, CancellationToken cancellationToken)
        {
            var alertDefinition = await _context.Definitions.Include(i => i.TriggersSet)
                .ThenInclude(ii => ii.Triggers)
                .FirstOrDefaultAsync(x => x.Id.Equals(alertDefinitionId), cancellationToken)
                .ConfigureAwait(false);
            if (alertDefinition?.TriggersSet == null || alertDefinition.TriggersSet?.Triggers.Count == 0)
            {
                throw new ArgumentException($"Cannot find Alert or triggers for Alert ID: '{alertDefinitionId}'");
            }

            alertDefinition.ActivatedOn = DateTime.UtcNow;
            alertDefinition.Enabled = activate;
            _context.Definitions.Update(alertDefinition);
            await _context.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
            return true;
        }

        public async Task<bool> DeleteAlert(Guid alertDefinitionId, CancellationToken cancellationToken)
        {
            try
            {
                var alertDefinition = await _context.Definitions
                    .FirstOrDefaultAsync(x => x.Id.Equals(alertDefinitionId), cancellationToken)
                    .ConfigureAwait(false);
                if (alertDefinition == null)
                {
                    throw new ArgumentException($"Cannot find Alert  for Alert ID: '{alertDefinitionId}'");
                }

                alertDefinition.DeletedOn = DateTime.UtcNow;
                alertDefinition.DeletedBy = "alert-Api";
                _context.Definitions.Update(alertDefinition);
                await _context.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
                return true;
            }
            catch (DbUpdateException e)
            {
                _logger.LogError(e, $"Error deleting alert with Id {alertDefinitionId}");
                return false;
            }
        }

        public IEnumerable<Context.Entities.AlertDefinition> GetAlertDefinitionsByCompany(int companyId)
        {
            var alertEntities = _context.Definitions.Include(i => i.Source)
                .Include(i => i.TriggersSet)
                .ThenInclude(ii => ii.Triggers).ThenInclude(iii => iii.Lookups)
                .Include(cd => cd.AlertCommunicationDefinition)
                .ThenInclude(cdr => cdr.Recipients).Include(i => i.TriggersSet).ThenInclude(ai => ai.Instances)
                .Where(x => x.CompanyId.Equals(companyId) && !x.DeletedOn.HasValue);
            return alertEntities.AsEnumerable();
        }

        public async Task<Context.Entities.AlertDefinition> GetAlertDefinition(Guid alertDefinitionId)
        {

            return await _context.Definitions.Include(i => i.Source)
                 .Include(i => i.TriggersSet).ThenInclude(ii => ii.Triggers)
                 .ThenInclude(iii => iii.Lookups)
                 .Include(cd => cd.AlertCommunicationDefinition)
                 .ThenInclude(cdr => cdr.Recipients).Include(x => x.TriggersSet).ThenInclude(x => x.Instances)
                 .FirstOrDefaultAsync(x => x.Id.Equals(alertDefinitionId) && !x.DeletedOn.HasValue);
        }

        public async Task<decimal?> SetThresholdAsync(Guid alertDefinitionId, decimal? thresholdValue)
        {
            var definition = await _context.Definitions.Include(i => i.TriggersSet)
                .FirstOrDefaultAsync(x => x.Id.Equals(alertDefinitionId)).ConfigureAwait(false);
            if (definition != null)
            {
                definition.TriggersSet.Threshold = thresholdValue;
                _context.Definitions.Update(definition);
                await _context.SaveChangesAsync().ConfigureAwait(false);
                return thresholdValue;
            }

            return default;
        }

        public async Task<Alert> GetAlertDefinitionById(Guid alertDefinitonId)
        {
            var entity = await _context.Definitions.Include(i => i.Source)
                .Include(i => i.TriggersSet)
                .Include(i => i.TriggersSet).ThenInclude(ii => ii.Triggers)
                .Include(i => i.TriggersSet).ThenInclude(ii => ii.Triggers.Where(x => !x.DeletedOn.HasValue)).ThenInclude(iii => iii.TriggerSet)
                .Include(i => i.TriggersSet).ThenInclude(ii => ii.Triggers.Where(x => !x.DeletedOn.HasValue)).ThenInclude(iii => iii.Lookups)
                .Include(cd => cd.AlertCommunicationDefinition)
                .ThenInclude(cdr => cdr.Recipients)
                .FirstOrDefaultAsync(x => x.Id.Equals(alertDefinitonId) && x.Enabled);

            return new Alert(entity.Id, entity.Name, new AlertDefinition(entity.Id, entity.Name, entity.CreatedBy, entity.CreatedOn,
                entity.TriggersSet.Triggers.Select(t => new TriggerData
                {
                    Id = t.Id,
                    LookupId = t.Value,
                    Type = t.Type,
                    SetId = t.TriggerSetId,
                    LastRun = t.TriggerSet.LastRun ?? entity.ActivatedOn.GetValueOrDefault(),
                    Answers = t.Lookups.Select(c => new TriggerStringLookup(c.Id, t.Id, c.Value)).ToArray(),
                    DeletedOn = t.DeletedOn
                }).ToList(), new Domain.AlertCommunicationDefinition()
                {
                    Id = entity.AlertCommunicationDefinition.Id,
                    CompanyId = entity.AlertCommunicationDefinition.CompanyId,
                    Recipients = entity.AlertCommunicationDefinition?.Recipients.Where(r => !r.DeletedOn.HasValue).Select(x => new Domain.AlertCommunicationRecipient()
                    {
                        Address = x.Address,
                        Enabled = x.Enabled,
                        Id = x.Id,
                        CommunicationChannelId = x.CommunicationChannelId,
                        CommunicationDefinitionId = x.CommunicationDefinitionId,
                    }),
                    AlertDefinitionId = entity.AlertCommunicationDefinition.AlertDefinitionId,
                    Message = entity.AlertCommunicationDefinition?.Message
                })
            {
                Enabled = entity.Enabled,
                Source = CreateSource(entity.Source),
                Company = new Company(entity.CompanyId),
                Threshold = entity.TriggersSet.Threshold,
                AlertTriggerSets = new AlertTriggerSets()
                {
                    Id = entity.TriggersSet.Id,
                    LastRun = entity.TriggersSet.LastRun,
                    LogicalOperation = entity.TriggersSet.LogicalOperation,
                    Threshold = entity.TriggersSet.Threshold,
                    NotificationInHours=entity.TriggersSet.NotificationFrequencyInHours,
                    TimeIntervalInMinutes=entity.TriggersSet.TimeIntervalInMinutes,
                    TriggersBelowThreshold=entity.TriggersSet.TriggersBelowThreshold,
                }
            });


        }

        public async Task<bool> UpdateAlertTriggerSetAsync(AlertTriggerSet alertTriggerSet)
        {
            if (alertTriggerSet is null )
            {
                throw new ArgumentNullException(nameof(alertTriggerSet));
            }
            _context.Update(alertTriggerSet);
            return await _context.SaveChangesAsync() > 0;
        }
    }
}
