﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using VoC.Alerts.Data.Abstraction;

namespace VoC.Alerts.Data
{
    public class SettingsRepository : ISettingsRepository
    {
        private readonly ILogger<SettingsRepository> _logger;
        private readonly string _connectionString;

        public SettingsRepository(IConfiguration configuration, ILogger<SettingsRepository> logger)
        {
            _logger = logger;
            _connectionString = configuration.GetConnectionString("wValidator");
        }
        
        public async Task<IEnumerable<CompanyServiceSettings>> GetEnabledCompaniesForService(string serviceCode)
        {
            var sqlConnection = new SqlConnection(_connectionString);
            try
            {
                return await sqlConnection.QueryAsync<int, string, CompanyServiceSettings>(GetActiveCompaniesQuery, map:
                    (companyId, data) =>
                    {
                        if (companyId > 0)
                        {
                            return new CompanyServiceSettings
                            {
                                CompanyId = companyId,
                                Data = data
                            };
                        }

                        return default;
                    },
                    new { nameCode = serviceCode }, splitOn: "Data", commandTimeout: TimeSpan.FromMinutes(5).Seconds).ConfigureAwait(true);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                throw;
            }
        }

        private const string GetActiveCompaniesQuery = @"SELECT C.[CompanyId], C.[Data]
  FROM [Settings].[Services] AS S
  INNER JOIN [Settings].[CompanyServices] AS C ON S.[Id] = C.[ServiceId]
  WHERE S.[NameCode] = @nameCode
  AND S.[Active] = 1
  AND C.[Enabled] = 1";
    }
}
