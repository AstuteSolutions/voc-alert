﻿namespace VoC.Alerts.Constants
{
    public enum CommunicationState
    {
        Await = 1,
        Sent = 2,
        Delivered = 3,
        Failed = 4
    }
}
