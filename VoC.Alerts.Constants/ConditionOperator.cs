﻿namespace VoC.Alerts.Constants
{
    public enum ConditionOperator
    {
        AND = 1,
        OR = 2,
    }
}
