﻿namespace VoC.Alerts.Constants
{
    public enum AlertState
    {
        Initiated = 1,
        Communicated = 2,
        Skipped = 3,
        Invalid = 0
    }
}
