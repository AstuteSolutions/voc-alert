# Alerting System #

This repository contains 3 solutions as components of Alerting System
* VoC.Alerts.Worker
    - Alerts worker is a background service, its main responsibility is to run active alerts triggers against Respondents data
* VoC.Alerts.Evaluation
    - Alerts Evaluation worker is a background service, its main responsibility is to execute logic on generated alert events and trigger communications
* VoC.Alerts.Web
    - Alerts Web API is there to manage creation, updates, and overall management of alerts definitions and triggers

## Diagrams

* Sequence Diagram:
![sequence diagram](Diagrams/alerts-sequence.png)

