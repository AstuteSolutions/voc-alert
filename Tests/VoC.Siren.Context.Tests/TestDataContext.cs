﻿using Iper.Common.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace VoC.Siren.Context.Tests
{
    public class TestDataContext : WebValidatorContext
    {
        public TestDataContext(DbContextOptions<WebValidatorContext> options) : base(options)
        {
            Seed();
        }

        private void Seed()
        {
            var respondetA = 12312836123;
            var surveyId = 120012;
            var companyId = 15018;

            Database.EnsureDeleted();
            Database.EnsureCreated();

            MasterDatas.Add(new Iper.Common.Models.MasterData
            {
                RespondentId = respondetA,
                ProjectId = surveyId,
                Answer = "something urgent here",
                QuestionTypeId = 4,
                QuestionId = 12344
            });
            RespondentDatas.Add(new Iper.Common.Models.RespondentData
            {
                RespondentId = respondetA,
                LangId = 1,
                SurveyId = surveyId
            });
            SurveyAccounts.Add(new Iper.Common.Models.SurveyAccount
            {
                SurveyId = surveyId,
                CompanyId = companyId,
                SurveyTitle = "Survey A"
            });
            SurveyQuestionnaireSetups.Add(new Iper.Common.Models.SurveyQuestionnaireSetup
            {
                SurveyId = surveyId,
                LangId = 1,
                QuestionId = 12344,
                QuestionTypeId = 4,
                VersionId = 2
            });
            MasterQuestionVersions.Add(new Iper.Common.Models.MasterQuestionVersion
            {
                QuestionId = 12344,
                VersionId = 2,
                LongDescId = 123
            });
            QuestionLongDescs.Add(new Iper.Common.Models.QuestionLongDesc
            {
                LongDescId = 123,
                LangId = 1,
                LongText = "how do you like our product?",
                QuestionId = 12344
            });

            SaveChanges();
        }
    }
}
