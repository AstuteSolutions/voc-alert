﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using VoC.Alerts.Constants;
using VoC.Alerts.Context.Entities;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Domain;
using VoC.Alerts.Logic.Communications;
using VoC.Alerts.Logic.Events;

namespace VoC.Alerts.Evaluation.Worker.Tests
{
    [TestClass]
    public class EventProcessorTests
    {
        private IEventProcessor _eventProcessor;
        private AlertTriggerSet alertTriggerSet;
        private Domain.TriggerEvent triggerEvent;

        [TestInitialize]
        public void Init()
        {
            var eventComunicationHandler = new Mock<IEventCommunicationHandler>();
            var eventsRepository = new Mock<IEventsRepository>();
            var alertsRepository = new Mock<IAlertsRepository>();
            alertTriggerSet = new AlertTriggerSet() { Instances = new List<AlertInstance>() { new AlertInstance() { State = AlertState.Skipped }, new AlertInstance() { State = AlertState.Skipped }, new AlertInstance() { State = AlertState.Skipped }, new AlertInstance() { State = AlertState.Skipped }, }, Threshold = 3, TriggersBelowThreshold = true, Triggers = new List<AlertTrigger>() { new AlertTrigger() { Type = "type" } } };
            alertsRepository.Setup(repository => repository.GetTriggerSetAsync(It.IsAny<Guid>())).ReturnsAsync(alertTriggerSet);
            _eventProcessor = new EventProcessor(eventComunicationHandler.Object, eventsRepository.Object, alertsRepository.Object);
            triggerEvent = new Domain.TriggerEvent(Guid.NewGuid(), 555, Guid.NewGuid(), DateTime.UtcNow.AddSeconds(-50), 0, new TriggerStringLookup(Guid.Empty, 0, "value"));
        }

        [TestMethod]
        public async Task Process_Returns_Correct_AlertState()
        {
            AlertState alertState = await _eventProcessor.ProcessAsync(triggerEvent, CancellationToken.None).ConfigureAwait(false);
            Assert.AreEqual(AlertState.Skipped, alertState);

            alertTriggerSet.Threshold = 8;
            alertState = await _eventProcessor.ProcessAsync(triggerEvent, CancellationToken.None).ConfigureAwait(false);
            Assert.AreEqual(AlertState.Initiated, alertState);

            alertTriggerSet.TriggersBelowThreshold = false;
            alertState = await _eventProcessor.ProcessAsync(triggerEvent, CancellationToken.None).ConfigureAwait(false);
            Assert.AreEqual(AlertState.Skipped, alertState);

            alertTriggerSet.Threshold = 2;
            alertState = await _eventProcessor.ProcessAsync(triggerEvent, CancellationToken.None).ConfigureAwait(false);
            Assert.AreEqual(AlertState.Initiated, alertState);
            alertTriggerSet.Threshold = 4;
            alertTriggerSet.TriggersBelowThreshold = true;
            alertState = await _eventProcessor.ProcessAsync(triggerEvent, CancellationToken.None).ConfigureAwait(false);
            Assert.AreEqual(AlertState.Initiated, alertState);
            alertTriggerSet.Threshold = 4;
            alertTriggerSet.TriggersBelowThreshold = false;
            alertState = await _eventProcessor.ProcessAsync(triggerEvent, CancellationToken.None).ConfigureAwait(false);
            Assert.AreEqual(AlertState.Initiated, alertState);
        }

        [TestMethod]
        public async Task Process_Returns_Correct_AlertState_When_PastCommunicated_Events()
        {
            // When the triggerSet property TriggersBelowThreshold == true, count all events inside of threshold time. When it is set to false, count events from after the last communicated event inside the threshold.
            var eventComunicationHandler = new Mock<IEventCommunicationHandler>();
            var eventsRepository = new Mock<IEventsRepository>();
            var alertsRepository = new Mock<IAlertsRepository>();
            alertTriggerSet = new AlertTriggerSet() { Instances = new List<AlertInstance>() { new AlertInstance() { State = AlertState.Communicated, CreatedOn = DateTime.UtcNow.AddMinutes(-70) }, new AlertInstance() { State = AlertState.Skipped, CreatedOn = DateTime.UtcNow.AddMinutes(-77) }, new AlertInstance() { State = AlertState.Skipped, CreatedOn = DateTime.UtcNow.AddMinutes(-88) }, new AlertInstance() { State = AlertState.Skipped, CreatedOn = DateTime.UtcNow.AddMinutes(-91) }, }, Threshold = 3, TriggersBelowThreshold = true, TimeIntervalInMinutes = 120, IsMultipleNotificationTrigger = true, NotificationFrequencyInHours = 1, Triggers = new List<AlertTrigger>() { new AlertTrigger() { Type = "type" } } };
            alertsRepository.Setup(repository => repository.GetTriggerSetAsync(It.IsAny<Guid>())).ReturnsAsync(alertTriggerSet);
            _eventProcessor = new EventProcessor(eventComunicationHandler.Object, eventsRepository.Object, alertsRepository.Object);
            triggerEvent = new Domain.TriggerEvent(Guid.NewGuid(), 555, Guid.NewGuid(), DateTime.UtcNow.AddSeconds(-50), 0, new TriggerStringLookup(Guid.Empty, 0, "value"));

            AlertState alertState = await _eventProcessor.ProcessAsync(triggerEvent, CancellationToken.None).ConfigureAwait(false);
            Assert.AreEqual(AlertState.Initiated, alertState );

            alertTriggerSet.Threshold = 2;
            alertState = await _eventProcessor.ProcessAsync(triggerEvent, CancellationToken.None).ConfigureAwait(false);
            Assert.AreEqual(AlertState.Skipped, alertState );

            alertTriggerSet.TriggersBelowThreshold = false;
            alertTriggerSet.Threshold = 3;
            alertState = await _eventProcessor.ProcessAsync(triggerEvent, CancellationToken.None).ConfigureAwait(false);
            Assert.AreEqual(AlertState.Skipped, alertState );

        }
    }
}