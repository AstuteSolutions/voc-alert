using System;
using System.Threading;
using System.Threading.Tasks;
using Iperloop.Core.Events.Enums;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Domain;
using VoC.Alerts.Evaluation.Worker.Events;
using VoC.Alerts.Logic.Events;

namespace VoC.Alerts.Evaluation.Worker.Tests
{
    [TestClass]
    public class MessageEventHandlerTests
    {
        private Mock<ILogger<MessageEventHandler>> _loggerMock;
        private Mock<IEventsRepository> _eventsStoreMock;
        private Mock<IEventProcessor> _eventProcessorMock;

        private Guid _triggerSetId;
        private Guid _eventId;
        private Guid _alertId;
        private int _triggerId;
        private int _companyId;
        private DateTime _executionTime;

        [TestInitialize]
        public void Init()
        {
            _triggerSetId = Guid.NewGuid();
            _eventId = Guid.NewGuid();
            _alertId = Guid.NewGuid();
            _triggerId = 3;
            _companyId = 15018;
            _executionTime = DateTime.UtcNow.AddSeconds(-10);

            _loggerMock = new Mock<ILogger<MessageEventHandler>>();
            _eventsStoreMock = new Mock<IEventsRepository>();
            _eventsStoreMock.Setup(x => x.GetAwaitingEventById(It.IsAny<Guid>()))
                .ReturnsAsync(new TriggerEvent(_eventId, _triggerId, _triggerSetId, _executionTime, 15018,
                    new TriggerStringLookup(Guid.NewGuid(), _triggerId, "something")));

            _eventProcessorMock = new Mock<IEventProcessor>();
            _eventProcessorMock.Setup(x => x.ProcessAsync(It.IsAny<TriggerEvent>(), CancellationToken.None));
        }
        
        [TestMethod]
        public async Task Handle_SetsMessageToAccepted_WhenEventIsProcessed()
        {
            var handler =
                new MessageEventHandler(_loggerMock.Object, _eventProcessorMock.Object, _eventsStoreMock.Object);

            await handler.HandleAsync(new EventMessage(Guid.NewGuid())
            {
                TriggerId = _triggerId,
                AlertId = _alertId,
                CompanyId = _companyId,
                ExecutionTime = _executionTime,
                CorrelationId = _eventId,
                TriggerSetId = _triggerSetId,
                Type = "event"
            }, CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(handler);
            Assert.AreEqual(EventState.Accepted, handler.EventState);
        }

        [TestMethod]
        public async Task Handle_SetsMessageToRejected_WhenEventIsNotFound()
        {
            _eventsStoreMock.Setup(x => x.GetAwaitingEventById(It.IsAny<Guid>()))
                .ReturnsAsync((TriggerEvent)null);

            var handler =
                new MessageEventHandler(_loggerMock.Object, _eventProcessorMock.Object, _eventsStoreMock.Object);
            await handler.HandleAsync(new EventMessage(Guid.NewGuid())
            {
                TriggerId = _triggerId,
                AlertId = _alertId,
                CompanyId = _companyId,
                ExecutionTime = _executionTime,
                CorrelationId = _eventId,
                TriggerSetId = _triggerSetId,
                Type = "event"
            }, CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(handler);
            Assert.AreEqual(EventState.Rejected, handler.EventState);
        }
    }
}
