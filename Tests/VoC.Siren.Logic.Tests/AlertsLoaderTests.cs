using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using VoC.Alerts.Context;
using VoC.Alerts.Data;

namespace VoC.Siren.Logic.Tests
{
    [TestClass]
    public class AlertsLoaderTests
    {
        [TestMethod]
        public void GetAlerts_ReturnsActiveCompanyAlerts()
        {
            var contextOptions = new DbContextOptionsBuilder<AlertsContext>();
            contextOptions.UseInMemoryDatabase("AlertsContext");
            var dataContext = new TestDataContext(contextOptions.Options);

            var repository = new AlertsRepository(dataContext, new Mock<ILogger<AlertsRepository>>().Object);
            var alerts = repository.GetAlertDefinitions(1234);

            Assert.IsNotNull(alerts);
            Assert.IsTrue(alerts.Any());
        }
    }
}
