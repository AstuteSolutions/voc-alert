﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using VoC.Alerts.Constants;
using VoC.Alerts.Context.Entities;
using VoC.Alerts.Data.Abstraction;
using VoC.Alerts.Domain;
using VoC.Alerts.Logic.Communications;
using VoC.Alerts.Logic.Events;
using AlertDefinition = VoC.Alerts.Context.Entities.AlertDefinition;
using TriggerEvent = VoC.Alerts.Domain.TriggerEvent;
using TriggerLookup = VoC.Alerts.Domain.TriggerLookup;

namespace VoC.Siren.Logic.Tests
{
    [TestClass]
    public class EventProcessorTests
    {
        private Mock<IEventCommunicationHandler> _eventCommunicationHandlerMock;
        private Mock<IEventsRepository> _eventsRepositoryMock;
        private Mock<IAlertsRepository> _alertsRepositoryMock;

        [TestInitialize]
        public void Init()
        {
            _eventCommunicationHandlerMock = new Mock<IEventCommunicationHandler>();
            _eventsRepositoryMock = new Mock<IEventsRepository>();
            _alertsRepositoryMock = new Mock<IAlertsRepository>();
        }

        [TestMethod]
        public async Task Process_ReturnsInvalidState_WhenSameEventWasProcessedInOneSecond()
        {
            _alertsRepositoryMock.Setup(x => x.GetTriggerSetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => new AlertTriggerSet
                {
                    Instances = new List<AlertInstance>
                    {
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1)},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-2)},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddSeconds(-1)},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddMilliseconds(-30)}
                    }
                });
            var processor = new EventProcessor(_eventCommunicationHandlerMock.Object, _eventsRepositoryMock.Object,
                _alertsRepositoryMock.Object);

            var state = await processor
                .ProcessAsync(
                    new TriggerEvent(Guid.NewGuid(), 1, Guid.NewGuid(), DateTime.UtcNow, 15018),
                    CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(state);
            Assert.AreEqual(AlertState.Invalid, state);
        }

        [TestMethod]
        public async Task Process_ReturnsInvalidState_WhenTriggerSetLogicIsNotSupported()
        {
            _alertsRepositoryMock.Setup(x => x.GetTriggerSetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => new AlertTriggerSet
                {
                    LogicalOperation = 5
                });
            var processor = new EventProcessor(_eventCommunicationHandlerMock.Object, _eventsRepositoryMock.Object,
                _alertsRepositoryMock.Object);

            var state = await processor
                .ProcessAsync(
                    new TriggerEvent(Guid.NewGuid(), 1, Guid.NewGuid(), DateTime.UtcNow, 15018),
                    CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(state);
            Assert.AreEqual(AlertState.Invalid, state);
        }

        [TestMethod]
        public async Task Process_ReturnsSkipped_WhenEventDoesNotMeetTriggerThreshold()
        {
            _alertsRepositoryMock.Setup(x => x.GetTriggerSetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => new AlertTriggerSet
                {
                    LogicalOperation = 2,
                    Threshold = 50,
                });
            var processor = new EventProcessor(_eventCommunicationHandlerMock.Object, _eventsRepositoryMock.Object,
                _alertsRepositoryMock.Object);

            var state = await processor
                .ProcessAsync(
                    new TriggerEvent(Guid.NewGuid(), 1, Guid.NewGuid(), DateTime.UtcNow, 15018),
                    CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(state);
            Assert.AreEqual(AlertState.Skipped, state);
        }

        [TestMethod]
        public async Task Process_ReturnsInitiated_WhenTriggerIsAboveThresholdOfEvents()
        {
            _alertsRepositoryMock.Setup(x => x.GetTriggerSetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => new AlertTriggerSet
                {
                    LogicalOperation = 2,
                    Threshold = 4,
                    Instances = new List<AlertInstance>
                    {
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-3), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddSeconds(-11), State = AlertState.Initiated},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddDays(-2), State = AlertState.Communicated},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddDays(-1), State = AlertState.Initiated}
                    },
                    AlertDefinition = new AlertDefinition()
                    {
                        Source = new AlertSource()
                        {
                            Type = "NotSurveyVolumeTrigger"
                        }
                    },
                    Triggers = new List<AlertTrigger>()
                    {
                        new AlertTrigger()
                        {
                            Type = "NotSurveyVolumeTrigger"
                        }
                    }
                });
            var processor = new EventProcessor(_eventCommunicationHandlerMock.Object, _eventsRepositoryMock.Object,
                _alertsRepositoryMock.Object);

            var state = await processor
                .ProcessAsync(
                    new TriggerEvent(Guid.NewGuid(), 1, Guid.NewGuid(), DateTime.UtcNow, 15018, new TriggerStringLookup(Guid.NewGuid(), 1, "no")),
                    CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(state);
            Assert.AreEqual(AlertState.Initiated, state);
        }

        [TestMethod]
        public async Task Process_ReturnsInitiated_WhenTriggerIsDueWithNoThreshold()
        {
            _alertsRepositoryMock.Setup(x => x.GetTriggerSetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => new AlertTriggerSet
                {
                    LogicalOperation = 2,
                    Threshold = null,
                    Instances = new List<AlertInstance>
                    {
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-3), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddSeconds(-11), State = AlertState.Initiated},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddDays(-2), State = AlertState.Communicated},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddDays(-1), State = AlertState.Initiated}
                    }
                });
            var processor = new EventProcessor(_eventCommunicationHandlerMock.Object, _eventsRepositoryMock.Object,
                _alertsRepositoryMock.Object);

            var state = await processor
                .ProcessAsync(
                    new TriggerEvent(Guid.NewGuid(), 1, Guid.NewGuid(), DateTime.UtcNow, 15018, new TriggerStringLookup(Guid.NewGuid(), 1, "no")),
                    CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(state);
            Assert.AreEqual(AlertState.Initiated, state);
        }

        [TestMethod]
        public async Task Process_ReturnsInitiated_WhenTriggerIsAboveThresholdOfEventsAndLastCommunicatedMoreThanOneHour()
        {
            _alertsRepositoryMock.Setup(x => x.GetTriggerSetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => new AlertTriggerSet
                {
                    LogicalOperation = 2,
                    Threshold = 4,
                    NotificationFrequencyInHours = 1,
                    IsMultipleNotificationTrigger = true,
                    Instances = new List<AlertInstance>
                    {
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-3), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddSeconds(-11), State = AlertState.Initiated},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddDays(-2), State = AlertState.Communicated},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddDays(-1), State = AlertState.Initiated}
                    },
                    AlertDefinition = new AlertDefinition()
                    {
                        Source = new AlertSource()
                        {
                            Type = "NotSurveyVolumeTrigger"
                        }
                    },
                    Triggers = new List<AlertTrigger>()
                    {
                        new AlertTrigger()
                        {
                            Type = "NotSurveyVolumeTrigger"
                        }
                    }
                });
            var processor = new EventProcessor(_eventCommunicationHandlerMock.Object, _eventsRepositoryMock.Object,
                _alertsRepositoryMock.Object);

            var state = await processor
                .ProcessAsync(
                    new TriggerEvent(Guid.NewGuid(), 1, Guid.NewGuid(), DateTime.UtcNow, 15018, new TriggerStringLookup(Guid.NewGuid(), 1, "no")),
                    CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(state);
            Assert.AreEqual(AlertState.Initiated, state);
        }

        [TestMethod]
        public async Task Process_ReturnsInitiated_WhenTriggerIsAboveThresholdOfEventsAndLastCommunicatedMoreThanOneHourAndTimeInFrequency()
        {
            _alertsRepositoryMock.Setup(x => x.GetTriggerSetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => new AlertTriggerSet
                {
                    LogicalOperation = 2,
                    Threshold = 4,
                    NotificationFrequencyInHours = 1,
                    IsMultipleNotificationTrigger = true,
                    TimeIntervalInMinutes = 360,
                    Instances = new List<AlertInstance>
                    {
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-3), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddSeconds(-11), State = AlertState.Initiated},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddDays(-2), State = AlertState.Communicated},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddDays(-1), State = AlertState.Initiated}
                    },
                    AlertDefinition = new AlertDefinition()
                    {
                        Source = new AlertSource()
                        {
                            Type = "NotSurveyVolumeTrigger"
                        }
                    },
                    Triggers = new List<AlertTrigger>()
                    {
                        new AlertTrigger()
                        {
                            Type = "NotSurveyVolumeTrigger"
                        }
                    }
                });
            var processor = new EventProcessor(_eventCommunicationHandlerMock.Object, _eventsRepositoryMock.Object,
                _alertsRepositoryMock.Object);

            var state = await processor
                .ProcessAsync(
                    new TriggerEvent(Guid.NewGuid(), 1, Guid.NewGuid(), DateTime.UtcNow, 15018, new TriggerStringLookup(Guid.NewGuid(), 1, "no")),
                    CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(state);
            Assert.AreEqual(AlertState.Initiated, state);
        }

        [TestMethod]
        public async Task Process_ReturnsSkipped_WhenTriggerIsBelowThresholdOfEventsAndLastCommunicatedMoreThanOneHourAndTimeInFrequency()
        {
            _alertsRepositoryMock.Setup(x => x.GetTriggerSetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => new AlertTriggerSet
                {
                    LogicalOperation = 2,
                    Threshold = 4,
                    NotificationFrequencyInHours = 1,
                    IsMultipleNotificationTrigger = true,
                    TimeIntervalInMinutes = 60,
                    Instances = new List<AlertInstance>
                    {
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-3), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddSeconds(-11), State = AlertState.Initiated},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddDays(-2), State = AlertState.Communicated},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddDays(-1), State = AlertState.Initiated}
                    },
                    AlertDefinition = new AlertDefinition()
                    {
                        Source = new AlertSource()
                        {
                            Type = "NotSurveyVolumeTrigger"
                        }
                    },
                    Triggers = new List<AlertTrigger>()
                    {
                        new AlertTrigger()
                        {
                            Type = "NotSurveyVolumeTrigger"
                        }
                    }
                });
            var processor = new EventProcessor(_eventCommunicationHandlerMock.Object, _eventsRepositoryMock.Object,
                _alertsRepositoryMock.Object);

            var state = await processor
                .ProcessAsync(
                    new TriggerEvent(Guid.NewGuid(), 1, Guid.NewGuid(), DateTime.UtcNow, 15018, new TriggerStringLookup(Guid.NewGuid(), 1, "no")),
                    CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(state);
            Assert.AreEqual(AlertState.Skipped, state);
        }

        [TestMethod]
        public async Task Process_ReturnsSkipped_WhenTriggerIsAboveThresholdOfEventsAndBelowLastCommunicatedMoreThanOneHourAndTimeInFrequency()
        {
            _alertsRepositoryMock.Setup(x => x.GetTriggerSetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => new AlertTriggerSet
                {
                    LogicalOperation = 2,
                    Threshold = 4,
                    NotificationFrequencyInHours = 1,
                    IsMultipleNotificationTrigger = true,
                    TimeIntervalInMinutes = 360,
                    Instances = new List<AlertInstance>
                    {
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-3), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddSeconds(-11), State = AlertState.Initiated},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddDays(-2), State = AlertState.Communicated},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddMinutes(-30), State = AlertState.Communicated},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddDays(-1), State = AlertState.Initiated}
                    },
                    AlertDefinition = new AlertDefinition()
                    {
                        Source = new AlertSource()
                        {
                            Type = "NotSurveyVolumeTrigger"
                        }
                    },
                    Triggers = new List<AlertTrigger>()
                    {
                        new AlertTrigger()
                        {
                            Type = "NotSurveyVolumeTrigger"
                        }
                    }
                });
            var processor = new EventProcessor(_eventCommunicationHandlerMock.Object, _eventsRepositoryMock.Object,
                _alertsRepositoryMock.Object);

            var state = await processor
                .ProcessAsync(
                    new TriggerEvent(Guid.NewGuid(), 1, Guid.NewGuid(), DateTime.UtcNow, 15018, new TriggerStringLookup(Guid.NewGuid(), 1, "no")),
                    CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(state);
            Assert.AreEqual(AlertState.Skipped, state);
        }

        [TestMethod]
        public async Task Process_ReturnsInvalid_WhenTriggerIsSetToNotifyOnce()
        {
            _alertsRepositoryMock.Setup(x => x.GetTriggerSetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => new AlertTriggerSet
                {
                    LogicalOperation = 2,
                    Threshold = 4,
                    NotificationFrequencyInHours = 1,
                    IsMultipleNotificationTrigger = false,
                    TimeIntervalInMinutes = 360,
                    Instances = new List<AlertInstance>
                    {
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-3), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddDays(-2), State = AlertState.Communicated},
                    },
                    AlertDefinition = new AlertDefinition()
                    {
                        Source = new AlertSource()
                        {
                            Type = "NotSurveyVolumeTrigger"
                        }
                    }
                });
            var processor = new EventProcessor(_eventCommunicationHandlerMock.Object, _eventsRepositoryMock.Object,
                _alertsRepositoryMock.Object);

            var state = await processor
                .ProcessAsync(
                    new TriggerEvent(Guid.NewGuid(), 1, Guid.NewGuid(), DateTime.UtcNow, 15018, new TriggerStringLookup(Guid.NewGuid(), 1, "no")),
                    CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(state);
            Assert.AreEqual(AlertState.Invalid, state);
        }

        [TestMethod]
        public async Task Process_ReturnsInitiated_ForFirstTime_WhenTriggerIsSetToNotifyOnceAndThresholdMatched()
        {
            _alertsRepositoryMock.Setup(x => x.GetTriggerSetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => new AlertTriggerSet
                {
                    LogicalOperation = 2,
                    Threshold = 4,
                    NotificationFrequencyInHours = 1,
                    IsMultipleNotificationTrigger = false,
                    TimeIntervalInMinutes = 360,
                    Instances = new List<AlertInstance>
                    {
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-3), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                    },
                    AlertDefinition = new AlertDefinition()
                    {
                        Source = new AlertSource()
                        {
                            Type = "NotSurveyVolumeTrigger"
                        }
                    },
                    Triggers = new List<AlertTrigger>()
                    {
                        new AlertTrigger()
                        {
                            Type = "NotSurveyVolumeTrigger"
                        }
                    }
                });
            var processor = new EventProcessor(_eventCommunicationHandlerMock.Object, _eventsRepositoryMock.Object,
                _alertsRepositoryMock.Object);

            var state = await processor
                .ProcessAsync(
                    new TriggerEvent(Guid.NewGuid(), 1, Guid.NewGuid(), DateTime.UtcNow, 15018, new TriggerStringLookup(Guid.NewGuid(), 1, "no")),
                    CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(state);
            Assert.AreEqual(AlertState.Initiated, state);
        }

        [TestMethod]
        public async Task Process_ReturnsInitiated_ForFirstTime_WhenTriggerIsSetToNotifyMoreThanOnceAndNoThreshold()
        {
            _alertsRepositoryMock.Setup(x => x.GetTriggerSetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => new AlertTriggerSet
                {
                    LogicalOperation = 2,
                    Threshold = 0,
                    NotificationFrequencyInHours = 1,
                    IsMultipleNotificationTrigger = true,
                    TimeIntervalInMinutes = 360,
                    Instances = new List<AlertInstance>
                    {
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-3), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                    },
                    AlertDefinition = new AlertDefinition()
                    {
                        Source = new AlertSource()
                        {
                            Type = "NotSurveyVolumeTrigger"
                        }
                    },
                    Triggers = new List<AlertTrigger>()
                    {
                        new AlertTrigger()
                        {
                            Type = "NotSurveyVolumeTrigger"
                        }
                    }
                });
            var processor = new EventProcessor(_eventCommunicationHandlerMock.Object, _eventsRepositoryMock.Object,
                _alertsRepositoryMock.Object);

            var state = await processor
                .ProcessAsync(
                    new TriggerEvent(Guid.NewGuid(), 1, Guid.NewGuid(), DateTime.UtcNow, 15018, new TriggerStringLookup(Guid.NewGuid(), 1, "no")),
                    CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(state);
            Assert.AreEqual(AlertState.Initiated, state);
        }

        [TestMethod]
        public async Task Process_ReturnsInitiated_ForFirstTime_WhenTriggerIsSetToNotifyMoreThanOnceAndNoThresholdWithCommunicatedAlerts()
        {
            _alertsRepositoryMock.Setup(x => x.GetTriggerSetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => new AlertTriggerSet
                {
                    LogicalOperation = 2,
                    Threshold = 0,
                    NotificationFrequencyInHours = 1,
                    IsMultipleNotificationTrigger = true,
                    TimeIntervalInMinutes = 360,
                    Instances = new List<AlertInstance>
                    {
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-3), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-1), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddDays(-2), State = AlertState.Communicated},
                    },
                    AlertDefinition = new AlertDefinition()
                    {
                        Source = new AlertSource()
                        {
                            Type = "NotSurveyVolumeTrigger"
                        }
                    },
                    Triggers = new List<AlertTrigger>()
                    {
                        new AlertTrigger()
                        {
                            Type = "NotSurveyVolumeTrigger"
                        }
                    }
                });
            var processor = new EventProcessor(_eventCommunicationHandlerMock.Object, _eventsRepositoryMock.Object,
                _alertsRepositoryMock.Object);

            var state = await processor
                .ProcessAsync(
                    new TriggerEvent(Guid.NewGuid(), 1, Guid.NewGuid(), DateTime.UtcNow, 15018, new TriggerStringLookup(Guid.NewGuid(), 1, "no")),
                    CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(state);
            Assert.AreEqual(AlertState.Initiated, state);
        }



        [TestMethod]
        public async Task Process_ReturnsInitiated_WhenTriggerIsSetToNotifyMoreThanOnceAndThresholdWithCommunicatedAlertsForAllTriggers()
        {
            var triggerId1 = 1;
            var triggerId2 = 2;
            var triggerSetIdGuid = Guid.NewGuid();
            var @event1 = new Alerts.Context.Entities.TriggerEvent()
            {
                TriggerId = triggerId1,
                TargetMatchId = "123",
                ExecutionTime = DateTime.UtcNow,
                Output = true,
                TriggersSetId = triggerSetIdGuid
            };
            var @event2 = new Alerts.Context.Entities.TriggerEvent()
            {
                TriggerId = triggerId1,
                TargetMatchId = "1234",
                ExecutionTime = DateTime.UtcNow,
                Output = true,
                TriggersSetId = triggerSetIdGuid
            };
            var triggerEvents = new List<Alerts.Context.Entities.TriggerEvent>()
            {
               @event1,
                new Alerts.Context.Entities.TriggerEvent()
                {
                    TriggerId = triggerId2,
                    TargetMatchId = "123",
                    ExecutionTime = DateTime.UtcNow,
                    Output = true,
                    TriggersSetId = triggerSetIdGuid
                },
             @event2,
                new Alerts.Context.Entities.TriggerEvent()
                {
                    TriggerId = triggerId2,
                    TargetMatchId = "1234",
                    ExecutionTime = DateTime.UtcNow,
                    Output = true,
                    TriggersSetId = triggerSetIdGuid
                },
                new Alerts.Context.Entities.TriggerEvent()
                {
                    TriggerId = triggerId2,
                    TargetMatchId = "12345",
                    ExecutionTime = DateTime.UtcNow.AddHours(-2),
                    Output = true,
                    TriggersSetId = triggerSetIdGuid
                },
                new Alerts.Context.Entities.TriggerEvent()
                {
                    TriggerId = triggerId2,
                    TargetMatchId = "12345",
                    ExecutionTime = DateTime.UtcNow.AddHours(-2),
                    Output = true,
                    TriggersSetId = triggerSetIdGuid
                }
            };
            _alertsRepositoryMock.Setup(x => x.GetTriggerSetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => new AlertTriggerSet
                {
                    Id = triggerSetIdGuid,
                    LogicalOperation = 1,
                    Threshold = 2,
                    NotificationFrequencyInHours = 1,
                    IsMultipleNotificationTrigger = true,
                    TimeIntervalInMinutes = 60,
                    AlertDefinition = new AlertDefinition()
                    {
                        Source = new AlertSource()
                        {
                            Type = "NotSurveyVolumeTrigger"
                        }
                    },
                    Triggers = new List<AlertTrigger>()
                    {
                        new AlertTrigger()
                        {
                            Id = triggerId1,
                            TriggerEvents = triggerEvents,
                            Type = "NotSurveyVolumeTrigger"
                        },
                        new AlertTrigger()
                        {
                            Id = triggerId2,
                            TriggerEvents = triggerEvents,
                            Type = "NotSurveyVolumeTrigger"
                        }
                    },
                    TriggerEvents = triggerEvents,
                    Instances = new List<AlertInstance>
                    {
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-2), State = AlertState.Communicated},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddMinutes(-30), State = AlertState.Skipped},
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddMinutes(-30), State = AlertState.Skipped},
                    }

                });
            var processor = new EventProcessor(_eventCommunicationHandlerMock.Object, _eventsRepositoryMock.Object,
                _alertsRepositoryMock.Object);

            var state = await processor
                .ProcessAsync(
                    new TriggerEvent(Guid.NewGuid(), 1, triggerSetIdGuid, DateTime.UtcNow, 15018, new TriggerStringLookup(Guid.NewGuid(), 1, "no")),
                    CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(state);
            Assert.AreEqual(AlertState.Initiated, state);
        }

        [TestMethod]
        public async Task Process_ReturnsDefaults_WhenTriggerIsSetToNotifyMoreThanOnceAndThresholdNotReachedWithCommunicatedAlertsForAllTriggers()
        {
            var triggerId1 = 1;
            var triggerId2 = 2;
            var triggerSetIdGuid = Guid.NewGuid();
            var @event1 = new Alerts.Context.Entities.TriggerEvent()
            {
                TriggerId = triggerId1,
                TargetMatchId = "123",
                ExecutionTime = DateTime.UtcNow,
                Output = true,
                TriggersSetId = triggerSetIdGuid
            };
            var @event2 = new Alerts.Context.Entities.TriggerEvent()
            {
                TriggerId = triggerId1,
                TargetMatchId = "1234",
                ExecutionTime = DateTime.UtcNow,
                Output = true,
                TriggersSetId = triggerSetIdGuid
            };
            var triggerEvents = new List<Alerts.Context.Entities.TriggerEvent>()
            {
               @event1,
                new Alerts.Context.Entities.TriggerEvent()
                {
                    TriggerId = triggerId2,
                    TargetMatchId = "12456",
                    ExecutionTime = DateTime.UtcNow,
                    Output = true,
                    TriggersSetId = triggerSetIdGuid
                },
             @event2,
                new Alerts.Context.Entities.TriggerEvent()
                {
                    TriggerId = triggerId2,
                    TargetMatchId = "1234456",
                    ExecutionTime = DateTime.UtcNow,
                    Output = true,
                    TriggersSetId = triggerSetIdGuid
                },
                new Alerts.Context.Entities.TriggerEvent()
                {
                    TriggerId = triggerId2,
                    TargetMatchId = "12345",
                    ExecutionTime = DateTime.UtcNow.AddHours(-2),
                    Output = true,
                    TriggersSetId = triggerSetIdGuid
                },
                new Alerts.Context.Entities.TriggerEvent()
                {
                    TriggerId = triggerId2,
                    TargetMatchId = "12345",
                    ExecutionTime = DateTime.UtcNow.AddHours(-2),
                    Output = true,
                    TriggersSetId = triggerSetIdGuid
                }
            };
            _alertsRepositoryMock.Setup(x => x.GetTriggerSetAsync(It.IsAny<Guid>()))
                .ReturnsAsync(() => new AlertTriggerSet
                {
                    Id = triggerSetIdGuid,
                    LogicalOperation = 1,
                    Threshold = 2,
                    NotificationFrequencyInHours = 1,
                    IsMultipleNotificationTrigger = true,
                    TimeIntervalInMinutes = 60,
                    AlertDefinition = new AlertDefinition()
                    {
                        Source = new AlertSource()
                        {
                            Type = "NotSurveyVolumeTrigger"
                        }
                    },
                    Triggers = new List<AlertTrigger>()
                    {
                        new AlertTrigger()
                        {
                            Id = triggerId1,
                            TriggerEvents = triggerEvents,
                            Type = "NotSurveyVolumeTrigger"
                        },
                        new AlertTrigger()
                        {
                            Id = triggerId2,
                            TriggerEvents = triggerEvents,
                            Type = "NotSurveyVolumeTrigger"
                        }
                    },
                    TriggerEvents = triggerEvents,
                    Instances = new List<AlertInstance>
                    {
                        new AlertInstance {CreatedOn = DateTime.UtcNow.AddHours(-2), State = AlertState.Communicated}
                       
                    }

                });
            var processor = new EventProcessor(_eventCommunicationHandlerMock.Object, _eventsRepositoryMock.Object,
                _alertsRepositoryMock.Object);

            var state = await processor
                .ProcessAsync(
                    new TriggerEvent(Guid.NewGuid(), 1, triggerSetIdGuid, DateTime.UtcNow, 15018, new TriggerStringLookup(Guid.NewGuid(), 1, "no")),
                    CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(state);
            Assert.AreEqual(AlertState.Invalid, state);
        }
    }
}
