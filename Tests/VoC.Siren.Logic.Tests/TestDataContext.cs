﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using VoC.Alerts.Context;
using VoC.Alerts.Context.Entities;

namespace VoC.Siren.Logic.Tests
{
    public class TestDataContext : AlertsContext
    {
        public DbContextOptions<AlertsContext> Options { get; }
        public TestDataContext(DbContextOptions<AlertsContext> contextOptions) :base(contextOptions)
        {
            Options = contextOptions;
            Seed();
        }

        private void Seed()
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();

            var alertSource = new AlertSource { Id = 1, Type = "Survey", Value = "123505" };
            Sources.Add(alertSource);

            var alertTrigger = new AlertTrigger
            {
                Id = 1,
                Source = alertSource,
                Type = "Question",
                Value = "10432"
            };

            var alertTrigger2 = new AlertTrigger
            {
                Id = 2,
                Source = alertSource,
                Type = "Theme",
                Value = "000"
            };
            Triggers.AddRange(alertTrigger, alertTrigger2);

            var id = System.Guid.NewGuid();
            var definition = new AlertDefinition
            {
                Id = id,
                Name = "alert A",
                CompanyId = 1234,
                Enabled = true,
                Source = alertSource,
                CreatedBy = "test_agent",
                CreatedOn = System.DateTime.UtcNow,
                ActivatedOn = DateTime.UtcNow
            };
            Definitions.Add(definition);
            var communicationDef = new AlertCommunicationDefinition()
            {
                AlertDefinitionId = id,
                CompanyId = 1234,
                Definition = definition,
                Id = Guid.NewGuid(),
                Message = "test",
                Recipients = new List<AlertCommunicationRecipient>()
                {
                    new EmailCommunicationRecipient()
                    {
                        Id = Guid.NewGuid(),
                        Address = "test@iperceptions.com",
                        Enabled = true
                    }
                }

            };
            CommunicationDefinitions.Add(communicationDef);

            var triggerSet = new AlertTriggerSet()
            {
                AlertDefinitionId = id
            };
            TriggerSets.Add(triggerSet);
            
            SaveChanges();
        }
    }
}
