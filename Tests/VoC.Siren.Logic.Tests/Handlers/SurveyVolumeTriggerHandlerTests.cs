﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using VoC.Alerts.Domain;
using VoC.Alerts.Logic;
using VoC.Alerts.Logic.Handlers;
using VoC.Alerts.Logic.Triggers;
using Voc.Alerts.TriggerEventResolvers.TriggerEventCreators;

namespace VoC.Siren.Logic.Tests.Handlers
{
    [TestClass]
    public class SurveyVolumeTriggerHandlerTests
    {
        private Mock<IVocResponseServices> _vocResponseServiceMock;
        private Mock<ITriggerEventResolver> _eventResolverMock;
        [TestInitialize]
        public void InIt()
        {
            _vocResponseServiceMock = new Mock<IVocResponseServices>();
            _eventResolverMock = new Mock<ITriggerEventResolver>();
            _eventResolverMock.Setup(x => x.Type).Returns(nameof(SurveyVolumeTrigger));
        }


        [TestMethod]
        public async Task CreateTriggerEvent_When_VolumeAlertConditionSatisfiesForRightBoundedValues()
        {
            _vocResponseServiceMock.Setup(x =>
                    x.GetTotalNumberOfResponses(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(new ValueTask<int>(10));
           
            var surveyVolumeTriggerHandler = new SurveyVolumeTriggerHandler(_vocResponseServiceMock.Object, new[] { _eventResolverMock.Object });
            var leftBoundedGuidId= Guid.NewGuid();
            var rightBoundedGuidId = Guid.NewGuid();
            _eventResolverMock.Setup(x => x.ProcessData(It.IsAny<object>(), It.IsAny<Trigger<TriggerEvents>>()))
                .Returns(new TriggerEvent(It.IsAny<Guid>(), It.IsAny<int>(), It.IsAny<Guid>(), It.IsAny<DateTime>(), It.IsAny<int>(), new TriggerStringLookup(rightBoundedGuidId, It.IsAny<int>(), "123123"))
                );
            var result = await surveyVolumeTriggerHandler.Handle(new SurveyVolumeTrigger(1, 1, "SurveyVolumeTrigger", 1, new VolumeBoundedValues()
            {
                BoundVolume = 5,
                LookUpId = leftBoundedGuidId
            }, new VolumeBoundedValues()
            {
                BoundVolume =9,
                LookUpId = rightBoundedGuidId
            },60),CancellationToken.None);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Events.First().Outcome);
            Assert.AreEqual(result.Events.First().Match.Id, rightBoundedGuidId);
        }

        [TestMethod]
        public async Task CreateTriggerEvent_When_VolumeAlertConditionSatisfiesForLeftBoundedValues()
        {
            _vocResponseServiceMock.Setup(x =>
                    x.GetTotalNumberOfResponses(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(new ValueTask<int>(3));
            _eventResolverMock.Setup(x => x.ProcessData(It.IsAny<object>(), It.IsAny<Trigger<TriggerEvents>>()))
                .Returns(new TriggerEvent(It.IsAny<Guid>(), It.IsAny<int>(), It.IsAny<Guid>(), It.IsAny<DateTime>(), It.IsAny<int>(), new TriggerStringLookup(It.IsAny<Guid>(), It.IsAny<int>(), "123123"))
                );
            var surveyVolumeTriggerHandler = new SurveyVolumeTriggerHandler(_vocResponseServiceMock.Object, new[] { _eventResolverMock.Object });
            var leftBoundedGuidId = Guid.NewGuid();
            var rightBoundedGuidId = Guid.NewGuid();
            _eventResolverMock.Setup(x => x.ProcessData(It.IsAny<object>(), It.IsAny<Trigger<TriggerEvents>>()))
                .Returns(new TriggerEvent(It.IsAny<Guid>(), It.IsAny<int>(), It.IsAny<Guid>(), It.IsAny<DateTime>(), It.IsAny<int>(), new TriggerStringLookup(leftBoundedGuidId, It.IsAny<int>(), "123123"))
                );
            var result = await surveyVolumeTriggerHandler.Handle(new SurveyVolumeTrigger(1, 1, "SurveyVolumeTrigger", 1, new VolumeBoundedValues()
            {
                BoundVolume = 5,
                LookUpId = leftBoundedGuidId
            }, new VolumeBoundedValues()
            {
                BoundVolume = 9,
                LookUpId = rightBoundedGuidId
            }, 60), CancellationToken.None);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Events.First().Outcome);
            Assert.AreEqual(result.Events.First().Match.Id, leftBoundedGuidId);
        }

        [TestMethod]
        public async Task CreateEmptyTriggerEvent_When_VolumeAlertConditionUnSatisfiesForBoundedValues()
        {
            _vocResponseServiceMock.Setup(x =>
                    x.GetTotalNumberOfResponses(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(new ValueTask<int>(10));
            var surveyVolumeTriggerHandler = new SurveyVolumeTriggerHandler(_vocResponseServiceMock.Object, new[] { _eventResolverMock.Object });
            var leftBoundedGuidId = Guid.NewGuid();
            var rightBoundedGuidId = Guid.NewGuid();
            var result = await surveyVolumeTriggerHandler.Handle(new SurveyVolumeTrigger(1, 1, "SurveyVolumeTrigger", 1, new VolumeBoundedValues()
            {
                BoundVolume = 5,
                LookUpId = leftBoundedGuidId
            }, new VolumeBoundedValues()
            {
                BoundVolume = 15,
                LookUpId = rightBoundedGuidId
            }, 60), CancellationToken.None);

            Assert.IsNotNull(result);
            Assert.IsFalse(result.Events.First().Outcome);
            Assert.IsNull(result.Events.First().Match);
        }
    }
}
