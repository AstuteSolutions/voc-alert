﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using VoC.Alerts.Domain;
using VoC.Alerts.Logic;
using VoC.Alerts.Logic.Handlers;
using VoC.Alerts.Logic.Triggers;
using Voc.Alerts.TriggerEventResolvers.TriggerEventCreators;
using Voc.Alerts.TriggerEventResolvers.TriggerEventCreators.CustomTriggerEventResolvers;

namespace VoC.Siren.Logic.Tests.Handlers
{
    [TestClass]
    public class SingleSelectScoreQuestionTriggerHandlerTests
    {
        private Mock<IVocResponseServices> _vocResponseServiceMock;
        private Mock<ITriggerEventResolver> _eventResolverMock;
        [TestInitialize]
        public void Init()
        {
            _vocResponseServiceMock = new Mock<IVocResponseServices>();
            _vocResponseServiceMock.Setup(x =>
                    x.GetResponses(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>()))
                .Returns(new List<Response>().AsQueryable());
            _eventResolverMock = new Mock<ITriggerEventResolver>();
            _eventResolverMock.Setup(x => x.Type).Returns(nameof(SingleSelectScoreQuestionTrigger));
        }

        [TestMethod]
        public async Task Handle_ReturnsFalseOutcome_WhenScoreIsBelowTriggerThreshold()
        {
            var handler = new SingleSelectScoreQuestionTriggerHandler(_vocResponseServiceMock.Object, new[] { _eventResolverMock.Object });
            var result = await handler.Handle(
                new SingleSelectScoreQuestionTrigger(15018, 491, "SingleSelectScoreQuestion", 12311, 1)
                {
                    Lookups = new[] { new TriggerStringLookup(Guid.NewGuid(), 1491, "6") }
                }, CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(result);
            Assert.IsFalse(result.Events.First().Outcome);
        }

        [TestMethod]
        public async Task Handle_ReturnsTrueOutcome_WhenScoreIsEqualToThreshold()
        {
            _vocResponseServiceMock.Setup(x =>
                    x.GetResponses(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>()))
                .Returns(new List<Response>{new Response(123123)
                {
                    Survey = new Survey(1),
                    Question = new Question(12311),
                    Answer = "7"
                }}.AsQueryable());
            _eventResolverMock.Setup(x => x.ProcessData(It.IsAny<object>(), It.IsAny<Trigger<TriggerEvents>>()))
                .Returns(new TriggerEvent(It.IsAny<Guid>(), It.IsAny<int>(), It.IsAny<Guid>(), It.IsAny<DateTime>(), It.IsAny<int>(), new TriggerStringLookup(It.IsAny<Guid>(), It.IsAny<int>(), "123123"))
                );
            var handler = new SingleSelectScoreQuestionTriggerHandler(_vocResponseServiceMock.Object, new[] { new SingleSelectScoreQuestionTriggerEventResolver() });
            var result = await handler.Handle(
                new SingleSelectScoreQuestionTrigger(15018, 491, "SingleSelectScoreQuestion", 12311, 1)
                {
                    Lookups = new[] { new TriggerStringLookup(Guid.NewGuid(), 1491, "6") },
                    RightBound = 7,
                    LeftBound = 0
                }, CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Events.First().Outcome);
        }

        [TestMethod]
        public async Task Handle_ReturnsTrueOutcome_WhenScoreIsBelowToThreshold()
        {
            _vocResponseServiceMock.Setup(x =>
                    x.GetResponses(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>()))
                .Returns(new List<Response>{new Response(123123)
                {
                    Survey = new Survey(1),
                    Question = new Question(12311),
                    Answer = "7"
                }}.AsQueryable());
            _eventResolverMock.Setup(x => x.ProcessData(It.IsAny<object>(), It.IsAny<Trigger<TriggerEvents>>()))
                .Returns(new TriggerEvent(It.IsAny<Guid>(), It.IsAny<int>(), It.IsAny<Guid>(), It.IsAny<DateTime>(), It.IsAny<int>(), new TriggerStringLookup(It.IsAny<Guid>(), It.IsAny<int>(), "123123"))
                );
            var handler = new SingleSelectScoreQuestionTriggerHandler(_vocResponseServiceMock.Object, new[] { new SingleSelectScoreQuestionTriggerEventResolver() });
            var result = await handler.Handle(
                new SingleSelectScoreQuestionTrigger(15018, 491, "SingleSelectScoreQuestion", 12311, 1)
                {
                    Lookups = new[] { new TriggerStringLookup(Guid.NewGuid(), 1491, "6") },
                    RightBound = 8,
                    LeftBound = 0
                }, CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Events.First().Outcome);
        }

        [TestMethod]
        public async Task Handle_ReturnsFlaseOutcome_WhenScoreIsBelowToThreshold()
        {
            _vocResponseServiceMock.Setup(x =>
                    x.GetResponses(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>()))
                .Returns(new List<Response>{new Response(123123)
                {
                    Survey = new Survey(1),
                    Question = new Question(12311),
                    Answer = "7"
                }}.AsQueryable());
            _eventResolverMock.Setup(x => x.ProcessData(It.IsAny<object>(), It.IsAny<Trigger<TriggerEvents>>()))
                .Returns(new TriggerEvent(It.IsAny<Guid>(), It.IsAny<int>(), It.IsAny<Guid>(), It.IsAny<DateTime>(), It.IsAny<int>(), new TriggerStringLookup(It.IsAny<Guid>(), It.IsAny<int>(), "123123"))
                );
            var handler = new SingleSelectScoreQuestionTriggerHandler(_vocResponseServiceMock.Object, new[] { new SingleSelectScoreQuestionTriggerEventResolver() });
            var result = await handler.Handle(
                new SingleSelectScoreQuestionTrigger(15018, 491, "SingleSelectScoreQuestion", 12311, 1)
                {
                    Lookups = new[] { new TriggerStringLookup(Guid.NewGuid(), 1491, "6") },
                    RightBound = 6,
                    LeftBound = 0
                }, CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(result);
            Assert.IsFalse(result.Events.First().Outcome);
        }
    }
}
