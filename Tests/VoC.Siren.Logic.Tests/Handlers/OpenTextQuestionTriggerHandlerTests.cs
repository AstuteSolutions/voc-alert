﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using VoC.Alerts.Domain;
using VoC.Alerts.Logic;
using VoC.Alerts.Logic.Handlers;
using VoC.Alerts.Logic.Triggers;
using Voc.Alerts.TriggerEventResolvers.TriggerEventCreators;
using Voc.Alerts.TriggerEventResolvers.TriggerEventCreators.CustomTriggerEventResolvers;

namespace VoC.Siren.Logic.Tests.Handlers
{
    [TestClass]
    public class OpenTextQuestionTriggerHandlerTests
    {
        private Mock<IVocResponseServices> _vocResponseServiceMock;
        private Mock<ITriggerEventResolver> _eventResolverMock;
        [TestInitialize]
        public void Init()
        {
            _vocResponseServiceMock = new Mock<IVocResponseServices>();
            _vocResponseServiceMock.Setup(x =>
                    x.GetResponses(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>()))
                .Returns(new List<Response>().AsQueryable());
            _eventResolverMock = new Mock<ITriggerEventResolver>();
            _eventResolverMock.Setup(x => x.Type).Returns(nameof(OpenTextQuestionTrigger));
        }

        [TestMethod]
        public async Task Handle_ReturnsFalseOutcome_WhenTriggerFindsNoMatch()
        {
            var handler = new OpenTextQuestionTriggerHandler(_vocResponseServiceMock.Object, new List<ITriggerEventResolver>(){ _eventResolverMock.Object});
            var result = await handler.Handle(
                new OpenTextQuestionTrigger(15018, 491, "SingleSelectQuestion", 12311, 1)
                {
                    Lookups = new[] { new TriggerStringLookup(Guid.NewGuid(), 1491, "Nasty") }
                }, CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(result);
            Assert.IsFalse(result.Events.First().Outcome);
        }

        [TestMethod]
        public async Task Handle_ReturnsTrueOutcome_WhenTriggerFindsMatch()
        {
            _vocResponseServiceMock.Setup(x =>
                    x.GetResponses(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>()))
                .Returns(new List<Response>{new Response(123123)
                {
                    Survey = new Survey(1),
                    Question = new Question(12311),
                    Answer = "I don't like the service, assume im saying something nasty."
                }}.AsQueryable());
            _eventResolverMock.Setup(x => x.ProcessData(It.IsAny<object>(), It.IsAny<Trigger<TriggerEvents>>()))
                .Returns(new TriggerEvent(It.IsAny<Guid>(),It.IsAny<int>(), It.IsAny<Guid>(), It.IsAny<DateTime>(),It.IsAny<int>(),new TriggerStringLookup(It.IsAny<Guid>(),It.IsAny<int>() ,"123123"))
               );
            var handler = new OpenTextQuestionTriggerHandler(_vocResponseServiceMock.Object, new List<ITriggerEventResolver>() { _eventResolverMock.Object });
            var result = await handler.Handle(
                new OpenTextQuestionTrigger(15018, 491, "SingleSelectQuestion", 12311, 1)
                {
                    Lookups = new[] { new TriggerStringLookup(Guid.NewGuid(), 1491, "Nasty") }
                }, CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Events.First().Outcome);
        }

        [TestMethod]
        public async Task Handle_ReturnsTrueOutcome_WhenTriggerFindsEmptySpaceForMatch()
        {
            _vocResponseServiceMock.Setup(x =>
                    x.GetResponses(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<DateTime>()))
                .Returns(new List<Response>{new Response(123123)
                {
                    Survey = new Survey(1),
                    Question = new Question(12311),
                    Answer = "I don't like the service, assume im saying something nasty."
                }}.AsQueryable());
            _eventResolverMock.Setup(x => x.ProcessData(It.IsAny<object>(), It.IsAny<Trigger<TriggerEvents>>()))
                .Returns(new TriggerEvent(It.IsAny<Guid>(), It.IsAny<int>(), It.IsAny<Guid>(), It.IsAny<DateTime>(), It.IsAny<int>(), new TriggerStringLookup(It.IsAny<Guid>(), It.IsAny<int>(), "123123"))
                );
            var handler = new OpenTextQuestionTriggerHandler(_vocResponseServiceMock.Object, new List<ITriggerEventResolver>() { new OpenTextTriggerEventResolver() });
            var result = await handler.Handle(
                new OpenTextQuestionTrigger(15018, 491, "SingleSelectQuestion", 12311, 1)
                {
                    Lookups = new[] { new TriggerStringLookup(Guid.NewGuid(), 1491, " ") }
                }, CancellationToken.None).ConfigureAwait(false);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Events.First().Outcome);
        }
    }
}
