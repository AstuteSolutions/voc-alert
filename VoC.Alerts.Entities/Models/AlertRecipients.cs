﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoC.Alerts.Entities.Models
{
    public class AlertRecipient
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public bool Enabled { get; set; }
    }
}
