﻿using System;

namespace VoC.Alerts.Entities.ViewModels
{
    public class TriggerLookupViewModel
    {
        public Guid Id { get; set; }
        public string Value { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
    }
}