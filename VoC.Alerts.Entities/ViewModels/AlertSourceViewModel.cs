﻿namespace VoC.Alerts.Entities.ViewModels
{
    public  class AlertSourceViewModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
    }

}