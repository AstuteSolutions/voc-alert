﻿using System;

namespace VoC.Alerts.Entities.ViewModels
{
    public class AlertCommunicationRecipientViewModel
    {
        public Guid Id { get; set; }
        public Guid CommunicationDefinitionId { get; set; }
        public int CommunicationChannelId { get; set; }
        public string Address { get; set; }
        public bool Enabled { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
    }
}