﻿using System;
using System.Collections.Generic;

namespace VoC.Alerts.Entities.ViewModels
{
    public class AlertTriggerSetViewModel
    {
        public Guid Id { get; set; }
        public Guid AlertDefinitionId { get; set; }
        public int LogicalOperation { get; set; } = 2;
        public decimal? Threshold { get; set; }
        public DateTime? LastRun { get; set; }
        public Guid CommunicationDefinitionId { get; set; }
        public bool? IsMultipleNotificationTrigger { get; set; }
        public bool? TriggersBelowThreshold { get; set;}
        public int? TimeIntervalInMinutes { get; set; }
        public virtual IEnumerable<AlertTriggerViewModel> Triggers { get; set; } = new List<AlertTriggerViewModel>();
    }
}