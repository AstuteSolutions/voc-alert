﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoC.Alerts.Entities.ViewModels
{
   public class AlertDefinitionViewModel
    {
        public Guid Id { get; set; }
        public AlertSourceViewModel Source { get; set; }
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public bool Enabled { get; set; }
        public int TotalNotifications { get; set; }
       
        public DateTime CreatedOn { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public DateTime? ActivatedOn { get; set; }
        public AlertCommunicationDefinitionViewModel AlertCommunicationDefinition { get; set; }
        public AlertTriggerSetViewModel TriggersSet { get; set; }
    }
}
