﻿using System;
using System.Collections.Generic;

namespace VoC.Alerts.Entities.ViewModels
{
    public class AlertCommunicationDefinitionViewModel
    {
        public Guid Id { get; set; }
        public Guid AlertDefinitionId { get; set; }
        public int CompanyId { get; set; }
        public string Message { get; set; }
        public IEnumerable<AlertCommunicationRecipientViewModel> Recipients { get; set; } =
            new List<AlertCommunicationRecipientViewModel>();
    }
}