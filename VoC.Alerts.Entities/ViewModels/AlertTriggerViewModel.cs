﻿
using System;
using System.Collections.Generic;

namespace VoC.Alerts.Entities.ViewModels
{
    public class AlertTriggerViewModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public int SourceId { get; set; }
        public Guid TriggerSetId { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public  IEnumerable<TriggerLookupViewModel> Lookups { get; set; } = new List<TriggerLookupViewModel>();
    }
}