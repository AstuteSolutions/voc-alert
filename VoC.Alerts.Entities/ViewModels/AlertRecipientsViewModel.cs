﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoC.Alerts.Entities.ViewModels
{
    public class AlertRecipientsViewModel
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public bool Enabled { get; set; }
        public string DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
    }
}
