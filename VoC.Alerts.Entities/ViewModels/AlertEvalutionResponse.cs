﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoC.Alerts.Entities.ViewModels
{
    public class AlertEvalutionResponse
    {
        public IEnumerable<EvaluationByRespondent> EvaluationByRespondents { get; set; }
        public Guid AlertDefintionId { get; set; }
    }

    public class EvaluationByRespondent
    {
        public long RespondentId { get; set; }
        public bool EvalutionResult { get; set; }
    }
}
