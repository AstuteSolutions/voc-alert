﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoC.Alerts.Entities.Enums
{
    public enum LogicalOperator
    {
        And = 1,
        Or = 2
    }
}
