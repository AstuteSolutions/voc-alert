﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VoC.Alerts.Domain;
using VoC.Alerts.Logic.Triggers;

namespace Voc.Alerts.TriggerEventResolvers
{
    public class TriggerCreator : ITriggerCreator
    {
        public object CreateTrigger(TriggerData triggerData, int companyId, int surveyId, Type triggerType)
        {
            if (triggerType == typeof(SingleSelectQuestionTrigger))
            {
                var questionId = int.Parse(triggerData.LookupId);
                var numericLookups = new List<TriggerNumericLookup>();
                foreach (var triggerLookup in triggerData.Answers)
                {
                    if (int.TryParse(triggerLookup.Value, out var tagId))
                    {
                        numericLookups.Add(new TriggerNumericLookup(triggerLookup.Id, triggerLookup.TriggerId, tagId));
                    }
                }
                return new SingleSelectQuestionTrigger(companyId, triggerData.Id, triggerData.Type, questionId, surveyId)
                {
                    CutOffTime = triggerData.LastRun,
                    Lookups = numericLookups.ToArray(),
                    SetId = triggerData.SetId
                };
            }
            if (triggerType == typeof(OpenTextQuestionTrigger))
            {
                var questionId = int.Parse(triggerData.LookupId);
                return new OpenTextQuestionTrigger(companyId, triggerData.Id, triggerData.Type, questionId, surveyId)
                {
                    CutOffTime = triggerData.LastRun,
                    Lookups = triggerData.Answers,
                    SetId = triggerData.SetId
                };
            }

            if (triggerType == typeof(SingleSelectScoreQuestionTrigger))
            {
                var questionId = int.Parse(triggerData.LookupId);
                var n = 0;
                decimal? valueA = null;
                decimal? valueB = null;
                while (n < triggerData.Answers.Length - 1 && n <= 2)
                {
                    if (triggerData.Answers[n] != null &&
                        decimal.TryParse(triggerData.Answers[n].Value, out var a))
                    {
                        valueA = a;
                    }

                    if (triggerData.Answers[n + 1] != null &&
                        decimal.TryParse(triggerData.Answers[n + 1].Value, out var b))
                    {
                        valueB = b;
                    }
                    n++;
                }

                if (!valueA.HasValue && !valueB.HasValue)
                {
                    throw new InvalidOperationException();
                }

                var leftBound = valueA >= valueB ? valueB : valueA;
                var rightBound = leftBound == valueA ? valueB : valueA;
                return new SingleSelectScoreQuestionTrigger(companyId, triggerData.Id, triggerData.Type, questionId, surveyId)
                {
                    CutOffTime = triggerData.LastRun,
                    SetId = triggerData.SetId,
                    LeftBound = leftBound,
                    RightBound = rightBound
                };
            }
            if (triggerType == typeof(SurveyVolumeTrigger))
            {
                var durationInMinutes = int.Parse(triggerData.LookupId);
                var sortedVolumeBounds = triggerData.Answers.OrderBy(x => x.Value).ToList();
                var leftBoundVolume = new VolumeBoundedValues()
                {
                    BoundVolume = int.Parse(sortedVolumeBounds[0] != null ? sortedVolumeBounds[0].Value : "0"),
                    LookUpId = sortedVolumeBounds[0]?.Id ?? throw new NotSupportedException("Left bound volume is not configured")
                };
                var rightBoundVolume = new VolumeBoundedValues()
                {
                    BoundVolume = int.Parse(sortedVolumeBounds[1] != null ? sortedVolumeBounds[1].Value : "0"),
                    LookUpId = sortedVolumeBounds[1]?.Id ?? throw new NotSupportedException("Left bound volume is not configured")
                };

                return new SurveyVolumeTrigger(companyId, triggerData.Id, triggerData.Type, surveyId, leftBoundVolume, rightBoundVolume, durationInMinutes)
                {
                    SetId = triggerData.SetId
                };

            }
            throw new NotImplementedException();
        }

        public object CreateTrigger(TriggerData triggerData, int companyId, int surveyId,List<long> respondentId, Type triggerType)
        {
            if (triggerType == typeof(SingleSelectQuestionTrigger))
            {
                var questionId = int.Parse(triggerData.LookupId);
                var numericLookups = new List<TriggerNumericLookup>();
                foreach (var triggerLookup in triggerData.Answers)
                {
                    if (int.TryParse(triggerLookup.Value, out var tagId))
                    {
                        numericLookups.Add(new TriggerNumericLookup(triggerLookup.Id, triggerLookup.TriggerId, tagId));
                    }
                }
                return new SingleSelectQuestionTrigger(companyId, triggerData.Id, triggerData.Type, questionId, surveyId)
                {
                    CutOffTime = triggerData.LastRun,
                    Lookups = numericLookups.ToArray(),
                    SetId = triggerData.SetId,
                    RespondentId=respondentId
                };
            }
            if (triggerType == typeof(OpenTextQuestionTrigger))
            {
                var questionId = int.Parse(triggerData.LookupId);
                return new OpenTextQuestionTrigger(companyId, triggerData.Id, triggerData.Type, questionId, surveyId)
                {
                    CutOffTime = triggerData.LastRun,
                    Lookups = triggerData.Answers,
                    SetId = triggerData.SetId,
                    RespondentId = respondentId
                };
            }

            if (triggerType == typeof(SingleSelectScoreQuestionTrigger))
            {
                var questionId = int.Parse(triggerData.LookupId);
                var n = 0;
                decimal? valueA = null;
                decimal? valueB = null;
                while (n < triggerData.Answers.Length - 1 && n <= 2)
                {
                    if (triggerData.Answers[n] != null &&
                        decimal.TryParse(triggerData.Answers[n].Value, out var a))
                    {
                        valueA = a;
                    }

                    if (triggerData.Answers[n + 1] != null &&
                        decimal.TryParse(triggerData.Answers[n + 1].Value, out var b))
                    {
                        valueB = b;
                    }
                    n++;
                }

                if (!valueA.HasValue && !valueB.HasValue)
                {
                    throw new InvalidOperationException();
                }

                var leftBound = valueA >= valueB ? valueB : valueA;
                var rightBound = leftBound == valueA ? valueB : valueA;
                return new SingleSelectScoreQuestionTrigger(companyId, triggerData.Id, triggerData.Type, questionId, surveyId)
                {
                    CutOffTime = triggerData.LastRun,
                    SetId = triggerData.SetId,
                    LeftBound = leftBound,
                    RightBound = rightBound,
                    RespondentId = respondentId
                };
            }
            if (triggerType == typeof(SurveyVolumeTrigger))
            {
                var durationInMinutes = int.Parse(triggerData.LookupId);
                var sortedVolumeBounds = triggerData.Answers.OrderBy(x => x.Value).ToList();
                var leftBoundVolume = new VolumeBoundedValues()
                {
                    BoundVolume = int.Parse(sortedVolumeBounds[0] != null ? sortedVolumeBounds[0].Value : "0"),
                    LookUpId = sortedVolumeBounds[0]?.Id ?? throw new NotSupportedException("Left bound volume is not configured")
                };
                var rightBoundVolume = new VolumeBoundedValues()
                {
                    BoundVolume = int.Parse(sortedVolumeBounds[1] != null ? sortedVolumeBounds[1].Value : "0"),
                    LookUpId = sortedVolumeBounds[1]?.Id ?? throw new NotSupportedException("Left bound volume is not configured")
                };

                return new SurveyVolumeTrigger(companyId, triggerData.Id, triggerData.Type, surveyId, leftBoundVolume, rightBoundVolume, durationInMinutes)
                {
                    SetId = triggerData.SetId,
                    RespondentId = respondentId
                };

            }
            throw new NotImplementedException();
        }
    }
}
