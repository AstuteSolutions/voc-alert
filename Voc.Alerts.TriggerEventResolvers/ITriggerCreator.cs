﻿            using System;
using System.Collections.Generic;
using VoC.Alerts.Domain;

namespace Voc.Alerts.TriggerEventResolvers
{
    public interface ITriggerCreator
    {
        object CreateTrigger(TriggerData triggerData, int companyId, int surveyId, Type triggerType);
        object CreateTrigger(TriggerData triggerData, int companyId, int surveyId, List<long> respondentId, Type triggerType);
    }
}