﻿using System;
using System.Collections.Generic;
using System.Text;
using VoC.Alerts.Logic.Triggers;

namespace Voc.Alerts.TriggerEventResolvers.TriggerDefinitions
{
    public static  class TriggerTypes
    {
        public static Dictionary<string, Type> TriggerTypesDictionary= new Dictionary<string, Type>
        {
            {"OpenTextQuestion", typeof(OpenTextQuestionTrigger)},
            {"SingleSelectQuestion", typeof(SingleSelectQuestionTrigger)},
            {"SingleSelectScoreQuestion", typeof(SingleSelectScoreQuestionTrigger)},
            {"SurveyVolumeTrigger", typeof(SurveyVolumeTrigger)}
        };

        public static Type OpenTextQuestion = typeof(OpenTextQuestionTrigger);
        public static Type SingleSelectQuestion = typeof(SingleSelectQuestionTrigger);
        public static Type SingleSelectScoreQuestion = typeof(SingleSelectScoreQuestionTrigger);
        public static Type SurveyVolumeTrigger = typeof(SurveyVolumeTrigger);
    }
}
