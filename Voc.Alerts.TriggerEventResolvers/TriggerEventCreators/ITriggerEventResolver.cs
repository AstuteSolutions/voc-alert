﻿using VoC.Alerts.Domain;
using VoC.Alerts.Logic.Triggers;

namespace Voc.Alerts.TriggerEventResolvers.TriggerEventCreators
{
    public interface ITriggerEventResolver
    {
         string Type { get; }
         TriggerEvent? ProcessData(object data, Trigger<TriggerEvents> trigger);
    }



}
