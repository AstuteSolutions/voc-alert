﻿using System;
using System.Globalization;
using VoC.Alerts.Domain;
using VoC.Alerts.Logic.Triggers;

namespace Voc.Alerts.TriggerEventResolvers.TriggerEventCreators.CustomTriggerEventResolvers
{
    public class SurveyVolumeTriggerEventResolver : ITriggerEventResolver
    {
        public string Type => nameof(SurveyVolumeTrigger);
        public TriggerEvent? ProcessData(object data, Trigger<TriggerEvents> trigger)
        {
            var request = (SurveyVolumeTrigger)trigger;
            var totalResponseCount = (int)data;

            if (totalResponseCount < request.LeftBound.BoundVolume || totalResponseCount > request.RightBound.BoundVolume)
            {
                var volumeBounded = totalResponseCount < request.LeftBound.BoundVolume
                    ? request.LeftBound
                    : request.RightBound;
                return new TriggerEvent(Guid.NewGuid(), request.Id, request.SetId, DateTime.UtcNow,
                    request.CompanyId,
                    new TriggerStringLookup(volumeBounded.LookUpId, request.Id, volumeBounded.BoundVolume.ToString(CultureInfo.InvariantCulture)))
                {
                    TargetMatchId = totalResponseCount.ToString(CultureInfo.InvariantCulture)
                };
            }
            return null;
        }

    }
}