﻿using System;
using System.Globalization;
using VoC.Alerts.Domain;
using VoC.Alerts.Logic.Triggers;

namespace Voc.Alerts.TriggerEventResolvers.TriggerEventCreators.CustomTriggerEventResolvers
{
    public class SingleSelectScoreQuestionTriggerEventResolver : ITriggerEventResolver
    {
        public string Type => nameof(SingleSelectScoreQuestionTrigger);
        public TriggerEvent? ProcessData(object data, Trigger<TriggerEvents> trigger)
        {
            var request = (SingleSelectScoreQuestionTrigger)trigger;
            var record = (Response)data;

            if (decimal.TryParse(record.Answer, out var scoreValue) &&
                (!request.LeftBound.HasValue || scoreValue >= request.LeftBound.Value) &&
                (!request.RightBound.HasValue || scoreValue <= request.RightBound.Value))
            {
                return new TriggerEvent(Guid.NewGuid(), request.Id, request.SetId, DateTime.UtcNow,
                    request.CompanyId,
                    new TriggerStringLookup(Guid.NewGuid(), request.Id,
                        scoreValue.ToString(CultureInfo.InvariantCulture)))
                {
                    TargetMatchId = record.Id.ToString()
                };
            }
            return null;
        }

    }
}