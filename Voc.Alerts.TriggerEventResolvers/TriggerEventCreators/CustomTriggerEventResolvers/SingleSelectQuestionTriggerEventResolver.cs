﻿using System;
using System.Linq;
using VoC.Alerts.Domain;
using VoC.Alerts.Logic.Triggers;

namespace Voc.Alerts.TriggerEventResolvers.TriggerEventCreators.CustomTriggerEventResolvers
{
    public class SingleSelectQuestionTriggerEventResolver : ITriggerEventResolver
    {
        public string Type => nameof(SingleSelectQuestionTrigger);
        public TriggerEvent? ProcessData(object data, Trigger<TriggerEvents> trigger)
        {
            var request = (SingleSelectQuestionTrigger)trigger;
            var record = (Response)data;

            foreach (var lookup in request.Lookups)
            {
                if (record.TagId.Contains(lookup.Value))
                {
                    return new TriggerEvent(Guid.NewGuid(), request.Id, request.SetId, DateTime.UtcNow,
                        request.CompanyId, new TriggerStringLookup(lookup.Id, lookup.TriggerId, lookup.Value.ToString()))
                    {
                        TargetMatchId = record.Id.ToString()
                    };
                }
            }
            return null;
        }

    }
}