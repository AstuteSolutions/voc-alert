﻿using System;
using VoC.Alerts.Domain;
using VoC.Alerts.Logic.Triggers;

namespace Voc.Alerts.TriggerEventResolvers.TriggerEventCreators.CustomTriggerEventResolvers
{
    public class OpenTextTriggerEventResolver : ITriggerEventResolver
    {
        public string Type => nameof(OpenTextQuestionTrigger);
        public TriggerEvent? ProcessData(object data, Trigger<TriggerEvents> trigger)
        {
            var request = (OpenTextQuestionTrigger)trigger;
            var response = (Response)data;
            foreach (var lookup in request.Lookups)
            {
                if (response.Answer.Contains(lookup.Value, StringComparison.InvariantCultureIgnoreCase) || string.IsNullOrWhiteSpace(lookup.Value) ||
                    (lookup.Value == "*" && !string.IsNullOrEmpty(response.Answer)))
                {
                    return new TriggerEvent(Guid.NewGuid(), request.Id, request.SetId, DateTime.UtcNow, request.CompanyId, lookup)
                    {
                        TargetMatchId = response.Id.ToString()
                    };
                }
            }

            return null;
        }

    }
}
