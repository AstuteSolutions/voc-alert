﻿using System;
using System.Collections.Generic;
using MediatR;
using VoC.Alerts.Domain;

namespace VoC.Alerts.Logic.Triggers
{
    public class Trigger<TResponse> : IRequest<TResponse>
    {
        public int Id { get; }
        public string Type { get; }
        public int CompanyId { get; }
        public Guid SetId { get; set; }

        protected Trigger(int companyId, int id, string type)
        {
            CompanyId = companyId;
            Id = id;
            Type = type;
        }
    }

    public class TriggerEvents
    {
        public List<TriggerEvent> Events { get; set; }

        public TriggerEvents(List<TriggerEvent> events)
        {
            Events = events;
        }
    }

    public class QuestionTriggers : Trigger<TriggerEvents>
    {
        public List<long> RespondentId { get; set; }
        public int QuestionId { get; }
        public int SurveyId { get; }
        public DateTime CutOffTime { get; set; }
        public QuestionTriggers(int companyId, int id, string type, int questionId, int surveyId) : base(companyId, id, type)
        {
            QuestionId = questionId;
            SurveyId = surveyId;
            RespondentId = new List<long>();
        }
    }

    public class QuestionTrigger : Trigger<TriggerEvent>
    {
        public int QuestionId { get; }
        public int SurveyId { get; }
        public DateTime CutOffTime { get; set; }

        protected QuestionTrigger(int companyId, int id, string type, int questionId, int surveyId) : base(companyId, id, type)
        {
            QuestionId = questionId;
            SurveyId = surveyId;
        }
    }
}