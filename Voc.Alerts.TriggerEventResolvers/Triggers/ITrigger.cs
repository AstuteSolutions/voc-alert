﻿using MediatR;
using VoC.Alerts.Domain;

namespace Voc.Alerts.TriggerEventResolvers.Triggers
{
    public interface ITrigger : IRequest<TriggerEvent>
    {
    }
}