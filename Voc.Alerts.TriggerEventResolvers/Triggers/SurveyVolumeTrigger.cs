﻿using System;
using System.Collections.Generic;
using VoC.Alerts.Domain;

namespace VoC.Alerts.Logic.Triggers
{
    public class SurveyVolumeTrigger : Trigger<TriggerEvents>
    {
        public List<long> RespondentId { get; set; }
        public int SurveyId { get; }
        public VolumeBoundedValues LeftBound { get; }
        public VolumeBoundedValues RightBound { get; }
        public int DurationInMinutes { get; }

        public SurveyVolumeTrigger(int companyId, int id, string type, int surveyId, VolumeBoundedValues leftBound, VolumeBoundedValues rightBound, int duration) : base(companyId, id, type)
        {
            SurveyId = surveyId;
            LeftBound = leftBound;
            RightBound = rightBound;
            DurationInMinutes = duration > 0 ? duration : throw new NotSupportedException($"{nameof(duration)} must be greater than zero");
            RespondentId = new List<long>();
        }
    }

    public class VolumeBoundedValues
    {
        public int BoundVolume { get; set; }
        public Guid LookUpId { get; set; }
    }
}