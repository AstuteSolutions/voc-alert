﻿using VoC.Alerts.Domain;

namespace VoC.Alerts.Logic.Triggers
{
    public class SingleSelectScoreQuestionTrigger : QuestionTriggers
    {
        public decimal? LeftBound { get; set; }
        public decimal? RightBound { get; set; }
        public TriggerLookup[] Lookups { get; set; }

        public SingleSelectScoreQuestionTrigger(int companyId, int id, string type, int questionId, int surveyId)
            : base(companyId, id, type, questionId, surveyId)
        {
        }
    }
}