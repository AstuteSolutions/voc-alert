﻿
using VoC.Alerts.Domain;

namespace VoC.Alerts.Logic.Triggers
{
    public class SingleSelectQuestionTrigger : QuestionTriggers
    {
        public SingleSelectQuestionTrigger(int companyId, int id, string type, int questionId, int surveyId)
            : base(companyId, id, type, questionId, surveyId)
        {
        }

        public TriggerNumericLookup[] Lookups { get; set; }
    }
}
