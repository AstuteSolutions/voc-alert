﻿
using VoC.Alerts.Domain;

namespace VoC.Alerts.Logic.Triggers
{
    public class OpenTextQuestionTrigger : QuestionTriggers
    {
        public OpenTextQuestionTrigger(int companyId, int id, string type, int questionId, int surveyId)
            : base(companyId, id, type, questionId, surveyId)
        {
        }

        public TriggerStringLookup[] Lookups { get; set; }
    }
}