﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Voc.Alerts.TriggerEventResolvers;
using Voc.Alerts.TriggerEventResolvers.TriggerEventCreators;
using Voc.Alerts.TriggerEventResolvers.TriggerEventCreators.CustomTriggerEventResolvers;
using VoC.Alerts.Logic;
using VoC.Alerts.Logic.Handlers;
using VoC.Alerts.Manager.Handlers;

namespace VoC.Alerts.DependencyInjection.Manager
{
    public static class AlertsManagerRegistration
    {
        public static IServiceCollection RegisterAlertsManager(this IServiceCollection services)
        {
            return services.AddMediatR(typeof(DefineAlertHandler),
                typeof(DefineAlertCommunicationHandler),
                typeof(AddRecipientsHandler),
                typeof(AddOrUpdateTriggersHandler),
                typeof(ActivateTriggersHandler),
                typeof(UpdateRecipientsHandler),
                typeof(DeleteAlertHandler),
                typeof(SetTriggersThresholdHandler), typeof(AlertSensitivityRequestHandler), 
                typeof(AlertEvaluateCommandHandler),
                typeof(SingleSelectQuestionTriggerHandler), typeof(AlertEvaluateByDateRangeCommandHandler),
                            typeof(OpenTextQuestionTriggerHandler),
                            typeof(SingleSelectScoreQuestionTriggerHandler)).AddTransient<ITriggerCreator, TriggerCreator>()
                        .AddTransient<ITriggerEventResolver, OpenTextTriggerEventResolver>()
                        .AddTransient<ITriggerEventResolver, SingleSelectQuestionTriggerEventResolver>()
                        .AddTransient<ITriggerEventResolver, SingleSelectScoreQuestionTriggerEventResolver>()
                        .AddTransient<ITriggerEventResolver, SurveyVolumeTriggerEventResolver>()
                          .AddTransient<IVocResponseServices, VocResponseServices>();
        }
    }
}
