﻿using Emplifi.VoC.Responses.Repository.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nest;
using VoC.Alerts.Context;
using VoC.Alerts.Data;
using VoC.Alerts.Data.Abstraction;
using Voc.Alerts.Sensitivity.ElasticSearchRepository;
using Voc.Alerts.Sensitivity.ElasticSearchRepository.Services;
using Voc.Alerts.Sensitivity.Logic;
using Voc.Alerts.TriggerEventResolvers;
using Iper.Common.EntityFramework;

namespace VoC.Alerts.DependencyInjection.Data
{
    public static class DataAccessRegistration
    {
        public static IServiceCollection RegisterRepositories(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddTransient<IAlertsRepository, AlertsRepository>()
                .AddTransient<IAlertsRecipientRepository, AlertsRecipientRepository>().AddTransient<IElasticClient>(_ =>
                new ElasticClient(configuration.GetSection("ElasticSearchSettings").Get<ElasticSettings>().GetConnectionSettings()))
                .AddTransient<IAlertsSensitivityElasticSearchRepository, AlertsSensitivityElasticSearchRepository>()
                .AddTransient<IAlertSensitivityManager, AlertSensitivityManager>()
                .AddTransient<IAlertsSensitivityDataSource, ElasticSearchData>()
                .AddTransient<ITriggerCreator, TriggerCreator>();
        }

        public static IServiceCollection RegisterAlertsDataContext(this IServiceCollection services, IConfiguration configuration)
        {
            return services.AddDbContext<AlertsContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("wValidator"));
                if (configuration.GetSection("Environment")?.Value.Equals("DEV") == true)
                {
                    options.EnableSensitiveDataLogging(true);
                    options.EnableDetailedErrors(true);
                }
            }).AddDbContext<WebValidatorContext>(options =>
                    {
                        options.UseSqlServer(configuration.GetConnectionString("wValidator"));
                        if (configuration.GetSection("Environment")?.Value.Equals("DEV") == true)
                        {
                            options.EnableSensitiveDataLogging(true);
                            options.EnableDetailedErrors(true);
                        }
                    });
        }
    }
}
