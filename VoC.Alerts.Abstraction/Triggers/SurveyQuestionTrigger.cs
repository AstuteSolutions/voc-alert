﻿using System;
using VoC.Alerts.Domain;

namespace VoC.Alerts.Abstraction.Triggers
{
    public class SurveyQuestionTrigger : Trigger
    {
        public int QuestionId { get; }
        public TriggerLookup[] Lookups { get; }
        public int CompanyId { get; }
        public int SurveyId { get; }
        public DateTime CutoffDateTime { get; set; }

        public SurveyQuestionTrigger(int id, Guid setId, int companyId, int surveyId, int questionId, TriggerLookup[] lookups) : base(id, "Question")
        {
            CompanyId = companyId;
            SurveyId = surveyId;
            QuestionId = questionId;
            Lookups = lookups;
            SetId = setId;
        }
    }
}
