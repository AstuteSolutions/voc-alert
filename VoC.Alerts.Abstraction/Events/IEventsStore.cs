﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VoC.Alerts.Domain;

namespace VoC.Alerts.Abstraction.Events
{
    public interface IEventsStore<TEvent> where TEvent : TriggerEvent
    {
        Task Store(IEnumerable<TEvent> events);
        IEnumerable<TEvent> GetRelatedEvents(Guid eventId);
        TriggerEvent GetAwaitingEvent(Guid eventId);
    }
}
