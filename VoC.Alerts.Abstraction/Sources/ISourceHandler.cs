﻿using System.Threading.Tasks;

namespace VoC.Alerts.Abstraction.Sources
{
    public interface ISourceHandler<in TSource>
    {
        Task Execute(TSource source);
    }
}
