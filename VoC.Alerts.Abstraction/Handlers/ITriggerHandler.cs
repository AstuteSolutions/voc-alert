﻿using System.Threading;
using System.Threading.Tasks;
using VoC.Alerts.Domain;

namespace VoC.Alerts.Abstraction.Handlers
{
    public interface ITriggerHandler<TTrigger, TResponse>
        where TTrigger: Trigger
        where TResponse : TriggerEvent
    {
        Task<TResponse> HandleAsync(TTrigger trigger, CancellationToken cancellationToken);
    }
}
